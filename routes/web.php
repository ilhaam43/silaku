<?php

use App\Http\Controllers\fungsional;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SetdaController;
use App\Http\Controllers\BKUController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\DataTtdController;
use App\Http\Controllers\DataBkuController;
use App\Http\Controllers\DetailDataBkuController;
use App\Http\Controllers\BenpemController;

use App\Http\Controllers\AdminUserController;
use App\Http\Controllers\RekeningController;
use App\Http\Controllers\Npd_rekapController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\PenjabaranController;
use App\Http\Controllers\Npd_pengajuanController;
use App\Http\Controllers\Kop_suratController;
use App\Http\Controllers\KopSuratController;
use App\Http\Controllers\Spj_panjarController;
use App\Http\Controllers\Spj_panjar_detailController;
use App\Http\Controllers\SpjRincianBelanjaController;
use App\Http\Controllers\PajakController;
use App\Http\Controllers\SpjLsController; 
use App\Http\Controllers\SpjLs_detailController; 
use App\Http\Controllers\Spj_panjar_editController; 
use App\Http\Controllers\PengambilanController; 
use App\Http\Controllers\Ls_PengambilanController; 


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});

 Auth::routes();

Route::get('/login/{role}', [LoginController::class, 'showLoginForm'])->name('login');

// logout
Route::get('/logout',[LoginController::class,'logout'])-> name('logout');

// route admin
Route::group(['middleware' => 'admin'], function () {
    Route::get('/admin', [AdminController::class,'index'])->name('admin');
    Route::get('/admin/rekening', [RekeningController::class,'index'])->name('admin.rekening');
    Route::get('/admin/rekening/list', [RekeningController::class,'list']);
    Route::post('/admin/rekening', [RekeningController::class,'store']);
    Route::get('/admin/rekening/{rekening}', [RekeningController::class,'show']);
    Route::put('/admin/rekening/{rekening}', [RekeningController::class,'update']);
    Route::delete('/admin/rekening/{rekening}', [RekeningController::class,'destroy']);
    Route::get('/admin/penjabaran', [PenjabaranController::class,'index'])->name('admin.penjabaran');
    Route::get('/admin/penjabaran/file/{tahun}', [PenjabaranController::class,'file'])->name('admin.penjabaran.file');
    Route::get('/admin/user', [AdminUserController::class,'index'])->name('admin.user');
    Route::get('/admin/user/list', [AdminUserController::class,'list']);
    Route::post('/admin/user', [AdminUserController::class,'store']);
    Route::get('/admin/user/{user}', [AdminUserController::class,'show']);
    Route::put('/admin/user/{user}', [AdminUserController::class,'update']);
    Route::delete('/admin/user/{user}', [AdminUserController::class,'destroy']);

    Route::get('/admin/ttd', [DataTtdController::class,'index'])->name('ttd.index');
    Route::post('/admin/ttd', [DataTtdController::class,'store'])->name('ttd.store');
    Route::post('/admin/ttd/{id}', [DataTtdController::class,'update'])->name('ttd.update');
    Route::delete('/admin/{id}/ttd', [DataTtdController::class,'destroy'])->name('ttd.destroy');

    Route::get('/cetak/{id}', [AdminController::class,'cetak']);
    Route::get('/fungsional', [fungsional::class,'index']);
    Route::post('/fungsional', [fungsional::class,'inputUrusan']);
    Route::get('/bku', [BKUController::class,'index']);
    Route::get('/cetakbku/{id}', [BKUController::class,'cetak']);
    Route::post('/create', [DataBkuController::class,'create']);

    Route::get('/admin/profile', [AdminController::class,'profile'])->name('admin.profile');
    Route::put('/admin/profile/update', [AdminController::class,'updateProfile'])->name('admin.profile.update');
});


Route::group(['middleware' => 'bentama'], function () {
    Route::get('/bentama', [SetdaController::class,'index'])->name('bentama');
    Route::get('/bentama/persetujuan', [SetdaController::class, 'showPersetujuan'])->name('verifikasiNpd');
    Route::put('/bentama/persetujuan/{kop}', [KopSuratController::class, 'verifikasiKop']);
    Route::get('/bentama/npd/file/{id}', [Npd_pengajuanController::class,'file'])->name('fileNpd');
});

Route::group(['middleware' => 'benpem'], function () {
    Route::get('/benpem',[BenpemController::class,'index'])->name('benpem');


    // route SPJ Panjar
    Route::get('/home',[BenpemController::class,'index']);
    Route::get('/spj/spj_panjar',[Spj_panjarController::class,'index'])->name('spj.spj_panjar');
    Route::delete('/spj/{id}/spj/{id2}',[Spj_panjarController::class,'destroy'])->name('spj.destroy');
    Route::post('/spj/edit/{id}',[Spj_panjarController::class,'update'])->name('spj.update');
    Route::get('/spj/edit/{id}',[Spj_panjarController::class,'edit'])->name('spj.edit');
    Route::get('/spj/create',[Spj_panjarController::class,'create'])->name('spj.create');
    Route::get('/spj/{id}/spj', [Spj_panjarController::class,'input'])->name('spj.input');
    Route::post('/spj/tambah_spj_panjar',[Spj_panjarController::class,'store'])->name('spj.store');

    // Route::get('/spj/{id}/edit', [Spj_panjar_editController::class,'input'])->name('spjedit.input');

    // route SPJ panjar Detail
    Route::get('/spj/tambah_spj_detail_panjar',[Spj_panjar_detailController::class,'index'])->name('spjdetail.index');
    Route::post('/spj/tambah_spj_detail_panjar',[Spj_panjar_detailController::class,'store'])->name('spjdetail.store');
    Route::post('/spj/{id}/update/{id2}',[Spj_panjar_detailController::class,'update'])->name('spjdetail.update');
    Route::get('/spj/{id}/update/{id2}',[Spj_panjar_detailController::class,'edit'])->name('spjdetail.edit');
    Route::get('/spj/{id}/new', [Spj_panjar_detailController::class,'input'])->name('spjdetail.input');
    Route::delete('/spj/{id}/new',[Spj_panjar_detailController::class,'destroy'])->name('spjdetail.destroy');
    Route::get('/spj/file/{id}', [Spj_panjarController::class,'file'])->name('spj.file');
    // Route::post('/spj/spj_panjar',[Spj_panjarController::class,'store']);

    //route Spj Rincian Belanja
    Route::get('/spj/tambah_spj_rincian',[SpjRincianBelanjaController::class,'index'])->name('spjrincian.index');
    Route::get('/spj/{id}/input', [SpjRincianBelanjaController::class,'input'])->name('spjrincian.input');
    Route::post('/spj/tambah_spj_rincian',[SpjRincianBelanjaController::class,'store'])->name('spjrincian.store');
    Route::post('/spj/{id}/edit/{id2}',[SpjRincianBelanjaController::class,'update'])->name('spjrincian.update');
    Route::get('/spj/{id}/edit/{id2}',[SpjRincianBelanjaController::class,'edit'])->name('spjrincian.edit');
    Route::delete('/spj/{id}/input',[SpjRincianBelanjaController::class,'destroy'])->name('spjrincian.destroy');

    //route Spj Pajak
    Route::get('/spj/tambah_pajak',[PajakController::class,'index'])->name('pajak.index');
    Route::get('/spj/{id}/pajak', [PajakController::class,'input'])->name('pajak.input');
    Route::post('/spj/tambah_pajak',[PajakController::class,'store'])->name('pajak.store');
    Route::patch('/spj/{id}/pajak',[PajakController::class,'update'])->name('pajak.update');
    // Route::get('/spj/{id}/edit/{id2}',[SpjRincianBelanjaController::class,'edit'])->name('spjrincian.edit');
    Route::delete('/spj/{id}/pajak',[PajakController::class,'destroy'])->name('pajak.destroy');

    // route SPJ Ls
    // Route::get('/spj-ls/spj_ls',[SpjLsController::class,'index'])->name('spj-ls.index');
    // Route::get('/spj-ls/{id}/baru', [SpjLsController::class,'input'])->name('spj-ls.input');
    // Route::post('/spj-ls/tambah_spj_ls',[SpjLsController::class,'store'])->name('spj-ls.store');
    // Route::post('/spj-ls/edit/{id}',[SpjLsController::class,'update'])->name('spj-ls.update');
    // Route::get('/spj-ls/edit/{id}',[SpjLsController::class,'edit'])->name('spj-ls.edit');
    // Route::delete('/spj-ls/{id}/baru',[SpjLsController::class,'destroy'])->name('spj-ls.destroy');

    // route SPJ Ls Detail
    // Route::get('/spj-ls/tambah_spj_ls_detail',[SpjLs_detailController::class,'index'])->name('spj-ls-detail.index');
    // Route::get('/spj-ls/{id}/new', [SpjLs_detailController::class,'input'])->name('spj-ls-detail.input');
    // Route::post('/spj-ls/tambah_spj_ls_detail',[SpjLs_detailController::class,'store'])->name('spj-ls-detail.store');
    // Route::post('/spj-ls/update/{id}',[SpjLs_detailController::class,'update'])->name('spj-ls-detail.update');
    // Route::get('/spj-ls/update/{id}',[SpjLs_detailController::class,'edit'])->name('spj-ls-detail.edit');
    // Route::delete('/spj-ls/{id}/new',[SpjLs_detailController::class,'destroy'])->name('spj-ls-detail.destroy');

    // Route::post('/spj/{id}/baru',[Spj_panjarController::class,'update'])->name('spj.update');
    // Route::delete('/spj/{id}/baru',[Spj_panjarController::class,'destroy'])->name('spj.destroy');
    // Route::get('/spj/edit/{id}',[Spj_panjarController::class,'edit'])->name('spj.edit');
    // Route::get('/spj/create',[Spj_panjarController::class,'create'])->name('spj.create');


    Route::post('/spj/spj_panjar/',[PengambilanController::class,'store'])->name('pengambilan.store');
    Route::post('/spj/spj_panjar/{id}',[PengambilanController::class,'update'])->name('pengambilan.update');
    // Route::post('/spj-ls/spj_ls/',[Ls_PengambilanController::class,'store'])->name('pengambilan-ls.store');
    // Route::post('/spj-ls/spj_ls/{id}',[Ls_PengambilanController::class,'update'])->name('pengambilan-ls.update');

    //kopsurat
    Route::post('/npd/npd_pengajuan/',[Kop_suratController::class,'store'])->name('kopsurat.store');
    Route::delete('/npd/npd_pengajuan/{id}',[Kop_suratController::class,'destroy'])->name('kopsurat.destroy');
    Route::patch('/npd/npd_pengajuan/{id}',[Kop_suratController::class,'update'])->name('kopsurat.update');
    Route::get('/npd/{id}/baru', [Npd_pengajuanController::class,'input'])->name('npd.input');
    Route::get('/npd/{id}/edit/{id2}', [Npd_pengajuanController::class,'edit'])->name('npd.edit');
    Route::post('/npd/{id}/edit/{id2}',[Npd_pengajuanController::class,'update'])->name('npd.update');
    Route::delete('/npd/{id}/delete/{id2}',[Npd_pengajuanController::class,'destroy'])->name('npd.destroy');
    Route::get('/npd/file/{id}', [Npd_pengajuanController::class,'file'])->name('npd.file');

    Route::get('/npd/npd_pengajuan',[Npd_pengajuanController::class,'index'])->name('npd.npd_pengajuan');

    Route::get('/npd/create',[Npd_pengajuanController::class,'create'])->name('npd.create');
    Route::get('/npd/tambah_npd_pengajuan',[Npd_pengajuanController::class,'index'])->name('npd.index');
    Route::post('/npd/tambah_npd_pengajuan',[Npd_pengajuanController::class,'store'])->name('npd.store');
    Route::get('/npd/list_rekening', [RekeningController::class,'list']);
    
    //ajax route
    Route::post('/ajax-kegiatan', [RekeningController::class,'ajaxKegiatan'])->name('ajax.kegiatan');;
    Route::post('/ajax-pencairan-sebelumnya', [Npd_pengajuanController::class,'ajaxPencairanSebelumnya'])->name('ajax.pencairan-sebelumnya');
});

