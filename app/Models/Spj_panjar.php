<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spj_panjar extends Model
{
    use HasFactory;
        protected $table = 'spj_panjars';
        protected $primaryKey = 'id';
        protected $fillable=[
            'npd_id',
            'rek_id',
            'sub_rek_id',
            'jenis_spj',
            'jml_panjar',
            'spj_panjar',
            'tgl_entry'
        ];

    public function spjdetail(){
        return $this->hasMany(Spj_panjar_detail::class,'panjar_id')->with('spj_detail');
    }

    public function spjedit(){
        return $this->hasMany(Spj_panjar_edit::class,'panjar_id');
    }

    public function rekening(){
        return $this->belongsTo(Rekening::class, 'rek_id')->with('induk');
    }

    public function subrekening(){
        return $this->belongsTo(Rekening::class, 'sub_rek_id');
    }

    public function npd(){
        return $this->belongsTo(Npd_pengajuan::class,'npd_id');
    }


    // public function detail(){
    //     return $this->hasOne('App\Models\Spj_belanja','id');
    // }

    // public function rek_panjar(){
    //     return $this->belongsTo(Rekening::class, 'rek_id');
    // }

    // public function subrekening(){
    //     return $this->belongsTo(Rekening::class, 'rek_sub_id');
    // }

}
