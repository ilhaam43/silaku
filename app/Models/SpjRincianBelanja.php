<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpjRincianBelanja extends Model
{
    use HasFactory;
        protected $table = 'spj_rincian_belanjas';
        protected $primaryKey = 'id';
        protected $fillable=[
            'detail_id',
            'penjelasan_barang_belanja',
            'volume',
            'harga_satuan',
            'total',
            'tgl_belanja'
    ];

    public function spj_detail(){
        return $this->belongsTo(Spj_panjar_detail::class,'detail_id');
    }

}
