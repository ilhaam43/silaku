<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Npd_pengajuan extends Model
{
    use HasFactory;
        protected $table = 'npd_pengajuans';
        protected $primaryKey = 'id';
        protected $fillable=[
            'kop_id',
            'rek_id',
            'sub_rek_id',
            'anggaran_murni',
            'pergeseran',
            'perubahan',
            'pencairan_sebelumnya',
            'pencairan_saat_ini',
            'sisa',
            'pptk'
        ];

    public function kopsurat(){
        return $this->belongsTo(Kop_surat::class, 'kop_id');
    }

    public function npd(){
        return $this->hasMany(Spj_panjar::class,'npd_id');
    }

    public function rekening(){
        return $this->belongsTo(Rekening::class, 'rek_id')->with('induk');
    }

    public function subrekening(){
        return $this->belongsTo(Rekening::class, 'sub_rek_id');
    }

    public function spj(){
        return $this->hasOne(Spj_panjar::class,'npd_id');
    }    

    public function spj_ls(){
        return $this->hasOne(SpjLs::class,'npd_id');
    }


    // public function kopsurat(){
    //     return $this->belongsTo('App\Kop_surat');
    // }

    // public function rekening(){
    //     return $this->belongsTo('App\Rekening');
    // }
}
