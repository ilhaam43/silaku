<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengambilan extends Model
{
    use HasFactory;
        protected $table = 'pengambilans';
        protected $primaryKey = 'id';
        protected $fillable=[
            'kop_id',
            'tgl_pengambilan',
            'pengambilan'
        ];

    public function kopsurat(){
        return $this->belongsTo(Kop_surat::class, 'kop_id');
    }
}
