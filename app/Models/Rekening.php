<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rekening extends Model
{
    use HasFactory;
    protected $table = 'rekenings';
    protected $fillable = [
        'kode_rekening', 'jenis_kegiatan', 'nama_kegiatan', 'parent_id', 'level'
    ];

    public function induk()
    {
        return $this->belongsTo(Rekening::class, 'parent_id')->with('induk');
    }

    public function subs()
    {
        return $this->hasMany(Rekening::class, 'parent_id')->with('subs');
    }

    public function npdpengajuan()
    {
        return $this->hasMany(Npd_pengajuan::class, 'rek_id');
    }

    public function subnpdpengajuan()
    {
        return $this->hasMany(Npd_pengajuan::class, 'sub_rek_id');
    }

    public function spjpanjar()
    {
        return $this->hasMany(Spj_panjar::class, 'rek_id');
    }

    public function subspjpanjar()
    {
        return $this->hasMany(Spj_panjar::class, 'sub_rek_id');
    }

    public function spjedit()
    {
        return $this->hasMany(Spj_panjar_edit::class, 'rek_id');
    }

    public function subspjedit()
    {
        return $this->hasMany(Spj_panjar_edit::class, 'sub_rek_id');
    }

    public function spjdetail()
    {
        return $this->hasMany(Spj_panjar_detail::class, 'rek_id');
    }

    public function subspjpanjardetail()
    {
        return $this->hasMany(Spj_panjar_detail::class, 'sub_rek_id');
    }

    public function spj_ls()
    {
        return $this->hasMany(SpjLs::class, 'rek_id');
    }

    public function subspj_ls()
    {
        return $this->hasMany(SpjLs::class, 'sub_rek_id');
    }

    public function spjdetail_ls()
    {
        return $this->hasMany(Spjls_detail::class, 'rek_id');
    }

    public function spjdetaills()
    {
        return $this->hasMany(Spjls_detail::class, 'sub_rek_id');
    }


}
