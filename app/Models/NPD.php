<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NPD extends Model
{
    use HasFactory;

    public function Rekening(){
        $this->hasMany('App\Models\Rekening');
    }
}
