<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pajak extends Model
{
    use HasFactory;
        protected $table = 'pajaks';
        protected $primaryKey = 'id';
        protected $fillable=[
            'detail_id',
            'terima_pajak',
            'terima_nominal',
            'bayar_pajak',
            'bayar_nominal'
    ];
    public function spj_pajak(){
        return $this->belongsTo(Spj_panjar_detail::class,'detail_id');
    }

}
