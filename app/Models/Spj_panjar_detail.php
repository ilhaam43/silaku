<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spj_panjar_detail extends Model
{
    use HasFactory;
        protected $table = 'spj_panjar_details';
        protected $primaryKey = 'id';
        protected $fillable=[
            'panjar_id',
            'rek_id',
            'sub_rek_id',
            'panjar_jml',
            'panjar_spj',
            'uraian',
            'entry_tgl'
    ];

    public function spjpanjar(){
        return $this->belongsTo(Spj_panjar::class,'panjar_id');
    }

    public function spj_detail(){
        return $this->hasMany(SpjRincianBelanja::class,'detail_id');
    }

    public function rekening(){
        return $this->belongsTo(Rekening::class, 'rek_id')->with('induk');
    }

    public function subrekening(){
        return $this->belongsTo(Rekening::class, 'sub_rek_id');
    }

    public function spj_pajak(){
        return $this->hasMany(Pajak::class,'detail_id');
    }

}
