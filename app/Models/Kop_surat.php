<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kop_surat extends Model
{
    use HasFactory;
    protected $table = 'kop_surats';
    protected $primaryKey = 'id';
    protected $fillable=['nomor_kop', 'nomor_dpa', 'tgl_dpa', 'bidang'];

    public function npdpengajuan(){
        return $this->hasMany(Npd_pengajuan::class,'kop_id');
    }

    public function pengambilan(){
        return $this->hasMany(Pengambilan::class,'kop_id');
    }
}
