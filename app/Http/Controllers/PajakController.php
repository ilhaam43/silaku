<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Spj_panjar_detail;
use App\Models\Pajak;
use RealRashid\SweetAlert\Facades\Alert;

class PajakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pajak = Pajak::all();
        return view('bendahara-pembantu.spj.tambah_pajak',compact('pajak'),['judul' => 'Dashboard']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function pajak($id){
        $spj_detail = Spj_panjar_detail::all();
        return view('bendahara-pembantu.spj.tambah_pajak', compact( 'spj_detail'), ['judul' => 'Tambah Pajak SPJ']);
    }

    public function input($id){
        $spj_detail = Spj_panjar_detail::findOrfail($id);
        // $data = Spj_panjar::findOrfail($id);
        //dd($data);
        // $npd = Npd_pengajuan::find($id);
        $pajak = Pajak::with(['spj_pajak'])->where('detail_id', $id)->get();
        //dd($npd);
        return view('bendahara-pembantu.spj.tambah_pajak', compact( 'spj_detail', 'pajak', ), ['judul' => 'Tambah Pajak SPJ', 'detail_id' => $id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pajak = Pajak::create([
            'detail_id'=>$request->detail_id,
            'terima_pajak'=>$request->terima_pajak,
            'bayar_pajak'=>$request->bayar_pajak,
            'bayar_nominal'=>$request->bayar_nominal,
            'terima_nominal'=>$request->terima_nominal,
        ]);

        return redirect("/spj/{$request->detail_id}/pajak");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Pajak::find($id);
        $data->update($request->all());

        return redirect("/spj/{$request->detail_id}/pajak");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pajak::where('id',$id)->delete();

        return back();
    }
}
