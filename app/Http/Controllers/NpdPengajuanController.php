<?php

namespace App\Http\Controllers;

use App\Models\Npd_pengajuan;
use App\Http\Requests\StoreNpd_pengajuanRequest;
use App\Http\Requests\UpdateNpd_pengajuanRequest;

class NpdPengajuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreNpd_pengajuanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNpd_pengajuanRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Npd_pengajuan  $npd_pengajuan
     * @return \Illuminate\Http\Response
     */
    public function show(Npd_pengajuan $npd_pengajuan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Npd_pengajuan  $npd_pengajuan
     * @return \Illuminate\Http\Response
     */
    public function edit(Npd_pengajuan $npd_pengajuan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateNpd_pengajuanRequest  $request
     * @param  \App\Models\Npd_pengajuan  $npd_pengajuan
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNpd_pengajuanRequest $request, Npd_pengajuan $npd_pengajuan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Npd_pengajuan  $npd_pengajuan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Npd_pengajuan $npd_pengajuan)
    {
        //
    }
}
