<?php

namespace App\Http\Controllers;

use App\Models\SpjRincianBelanja;
use App\Models\Spj_panjar_detail;
use RealRashid\SweetAlert\Facades\Alert;



use Illuminate\Http\Request;

class SpjRincianBelanjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spj_rincian = SpjRincianBelanja::all();
        return view('bendahara-pembantu.spj.tambah_spj_rincian',compact('spj_rincian'),['judul' => 'Dashboard']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function spj_rincian($id){
        $spj_detail = Spj_panjar_detail::all();
        return view('bendahara-pembantu.spj.tambah_spj_rincian', compact( 'spj_detail'), ['judul' => 'Tambah SPJ Rincian Belanja']);
    }

    public function input($id){
        $spj_detail = Spj_panjar_detail::findOrfail($id);
        // $data = Spj_panjar::findOrfail($id);
        //dd($data);
        // $npd = Npd_pengajuan::find($id);
        $spj_belanja = SpjRincianBelanja::with(['spj_detail'])->where('detail_id', $id)->get();
        //dd($npd);
        return view('bendahara-pembantu.spj.tambah_spj_rincian', compact( 'spj_detail', 'spj_belanja', ), ['judul' => 'Tambah SPJ Rincian Belanja', 'detail_id' => $id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $spj_rincian = SpjRincianBelanja::create([
            'detail_id'=>$request->detail_id,
            'penjelasan_barang_belanja'=>$request->penjelasan_barang_belanja,
            'volume'=>$request->volume,
            'harga_satuan'=>$request->harga_satuan,
            'total'=>$request->total,
            'tgl_belanja'=>$request->tgl_belanja
        ]);

        return redirect("/spj/{$request->detail_id}/input");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $id2)
    {
        $data = Spj_panjar_detail::findOrfail($id);
        //dd($data);
        // $npd = Npd_pengajuan::find($id);
        $spj_belanja = SpjRincianBelanja::with(['spj_detail'])->where('detail_id', $id)->get();
        $edit = SpjRincianBelanja::with(['spj_detail'])->where('detail_id', $id)->where('id', $id2)->firstOrFail();
        //dd($npd);
        return view('bendahara-pembantu.spj.edit_belanja', compact('data', 'spj_belanja',  'edit'), ['judul' => 'Ubah Spj Rincian Belanja', 'detail_id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $id2)
    {
        $data = $request->all();
        //dd($data);
        // $npd = Npd_pengajuan::find($id);
        $spj_belanja = SpjRincianBelanja::with(['spj_detail'])->where('detail_id', $id)->where('id', $id2)->firstOrFail();
        if ($spj_belanja->update($data)) {
            Alert::success('Berhasil', 'Berhasi mengubah data pengajuan');
        } else {
            Alert::warning('Gagal', 'Gagal mengubah data pengajuan');
        }
        return redirect()->route('spjrincian.input', ['id' => $id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SpjRincianBelanja::where('id',$id)->delete();

        return back();
    }
}
