<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use App\Models\DataFungsional;
use App\Models\daftarFungsional;

class fungsional extends Controller
{
    //
    public function index()
    {
        $daftarFungsional = daftarFungsional::all();

        foreach ($daftarFungsional as $df) {
            $month = $df->month;
            $dateObj   = DateTime::createFromFormat('!m', $month);
            $monthName = $dateObj->format('F');

            // echo $monthName;
            // echo $df->year;
            // echo $df->subBag;
            // echo "<br>";
        };

        // dd($daftarFungsional);
        return view('admin.daftarFungsional', [
            'daftar' => $daftarFungsional,
            'judul' => 'fungsional',
            'i' => 1,
        ]);
    }

    public function inputUrusan(Request $request)
    {
        // dd($request);
        $dataFungsional = DataFungsional::create([
            'fungsionalId' => $request->idFungsional,
            'kodeUrusan' => $request->kodeUrusan,
            'namaUrusan' => $request->namaUrusan,
            'kodeBidang' => $request->kodeBidang,
            'namaBidang' => $request->namaBidang,
            'kodeUnit' => $request->kodeUnit,
            'namaUnit' => $request->namaUnit,
            'kodeSubUnit' => $request->kodeSubUnit,
            'namaSubUnit' => $request->namaSubUnit,
        ]);

        $fungsional = daftarFungsional::find($request->idFungsional);
        // echo $request->idFungsional;
        // dd($fungsional);
        $fungsional->form = "Y";
        $fungsional->save();


        return back();
    }
}
