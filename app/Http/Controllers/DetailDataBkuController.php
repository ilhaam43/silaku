<?php

namespace App\Http\Controllers;

use App\Models\detailDataBku;
use App\Http\Requests\StoredetailDataBkuRequest;
use App\Http\Requests\UpdatedetailDataBkuRequest;

class DetailDataBkuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoredetailDataBkuRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoredetailDataBkuRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\detailDataBku  $detailDataBku
     * @return \Illuminate\Http\Response
     */
    public function show(detailDataBku $detailDataBku)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\detailDataBku  $detailDataBku
     * @return \Illuminate\Http\Response
     */
    public function edit(detailDataBku $detailDataBku)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatedetailDataBkuRequest  $request
     * @param  \App\Models\detailDataBku  $detailDataBku
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatedetailDataBkuRequest $request, detailDataBku $detailDataBku)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\detailDataBku  $detailDataBku
     * @return \Illuminate\Http\Response
     */
    public function destroy(detailDataBku $detailDataBku)
    {
        //
    }
}
