<?php

namespace App\Http\Controllers;

use App\Models\BKU;
use App\Models\Pajak;
use App\Models\DataBku;
use App\Models\Kop_surat;
use App\Models\Spj_panjar;
use Illuminate\Http\Request;
use App\Models\detailDataBku;
use App\Models\Spj_panjar_detail;
use Illuminate\Support\Facades\DB;
use App\Models\spj_rincian_belanja;
use App\Http\Requests\StoreDataBkuRequest;
use App\Http\Requests\UpdateDataBkuRequest;

class DataBkuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $bku = BKU::find($request->id);


        $year = $bku->year;
        $month = $bku->month;
        $subBag = $bku->subBag;
        $tz = new \DateTimeZone('Asia/Jakarta');




        // echo $penerimaan;

        $kop = Kop_surat::whereYear('tgl_dpa', '=', $year)
            ->whereMonth('tgl_dpa', '=', $month)
            ->where('disetujui', 1)
            ->where('bidang', '=', $subBag)
            ->get();

        // dd($kop);

        foreach ($kop as $data) {
            $pengambilanUang = 0;

            $data->spj = DB::table('npd_pengajuans')
                ->join('spj_panjars', 'npd_pengajuans.id', '=', 'spj_panjars.npd_id')
                ->join('rekenings', 'rekenings.id', '=', 'npd_pengajuans.sub_rek_id')
                ->where('npd_pengajuans.kop_id', '=', $data->id)
                ->select('npd_pengajuans.id', 'spj_panjars.*', 'rekenings.*')
                ->get();

            // dd( $data->spj);

            $j = 0;
            $k = 0;

            $dataBKUs = DataBku::where('kop_id', '=', $data->id)->get();

            foreach ($dataBKUs as $dataBKU) {
                $dataBKU->delete();
            }

            // dd($kop);
            foreach ($data->spj as $spj) {
                $findspj = Spj_panjar::where('npd_id', '=', $spj->npd_id)->first();

                // dd($findspj);
                $spj->id_spj =  $findspj->id;

                $spj->detail = Spj_panjar_detail::where('panjar_id', '=', $spj->id_spj)->get();

                $DataBku = DataBku::create([
                    'kop_id' => $data->id,
                    'tanggal' => $spj->tgl_entry,
                    'rek_id' => $spj->rek_id,
                    'sub_rek_id' => $spj->sub_rek_id,
                    'npd_id' =>  $spj->npd_id,
                    'spj_id' =>  $spj->id_spj,
                    'penerimaan' =>  $spj->jml_panjar,
                    'pengeluaran' =>  $spj->spj_panjar,
                    'jenis_spj' => $spj->jenis_spj,

                ]);

                if ($spj->detail->isNotEmpty()) {
                    // $dataBKU[$j] = DataBku::where('npd_id','=',$spj->npd_id)->where('spj_id','=',$spj->id_spj)->first();
                    foreach ($spj->detail as $detail) {




                        $pajaks = Pajak::where('detail_id', '=', $detail->id)->get();

                        //   echo $pajaks;
                        // $pajak[$k] = $pajaks[0];



                        if ($pajaks->isNotEmpty()) {
                            foreach ($pajaks as $pajak) {
                                $detailDataBKU = detailDataBku::create([
                                    'dataBKU_id' => $DataBku->id,
                                    'tanggal' => $detail->entry_tgl,
                                    'uraian' => $pajak->terima_pajak,
                                    'penerimaan' => $pajak->terima_nominal,
                                    'pengeluaran' => $pajak->bayar_nominal,
                                ]);
                            }
                        }

                        $k++;
                    }
                }




                $j++;
            }



            // dd($pajak);
        }

        return back();
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDataBkuRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDataBkuRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DataBku  $dataBku
     * @return \Illuminate\Http\Response
     */
    public function show(DataBku $dataBku)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DataBku  $dataBku
     * @return \Illuminate\Http\Response
     */
    public function edit(DataBku $dataBku)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDataBkuRequest  $request
     * @param  \App\Models\DataBku  $dataBku
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDataBkuRequest $request, DataBku $dataBku)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DataBku  $dataBku
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataBku $dataBku)
    {
        //
    }
}
