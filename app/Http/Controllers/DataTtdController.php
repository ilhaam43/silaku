<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DataTtd;

class DataTtdController extends Controller
{
    public function index() {
        $data = DataTtd::all();
        $judul = "Daftar TTD";
        return view('admin.ttd', compact(['data', 'judul']));
    }

    public function store(Request $request) {
        DataTtd::create($request->all());
        return back();
    }

    public function update(Request $request, $id) {
        $data = DataTtd::find($id);
        $data->update($request->all());

        return back();
    }

    public function destroy($id) {
        DataTtd::where('id',$id)->delete();

        return back();
    }
}
