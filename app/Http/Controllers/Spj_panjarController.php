<?php

namespace App\Http\Controllers;
use App\Models\Spj_panjar;
use App\Models\Spj_panjar_detail;
use App\Models\Spj_belanja;
use App\Models\Rekening;
use App\Models\Kop_surat;
use App\Models\Npd_pengajuan;
use App\Models\Pajak;
use App\Models\Pengambilan;
use App\Models\DataTtd;
use RealRashid\SweetAlert\Facades\Alert;

use Barryvdh\DomPDF\Facade\Pdf;

use Illuminate\Http\Request;

class Spj_panjarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $daftar_npd = Kop_surat::where('disetujui','=', 1)->where('bidang', auth()->user()->level)->get();
        $rekening = Rekening::all();
        $datas = Spj_panjar::all();
        $pengambilan = Pengambilan::all();
        $spj_detail = Spj_panjar_detail::all();

        return view('bendahara-pembantu.spj.spj_panjar',compact('rekening','datas', 'spj_detail', 'daftar_npd', 'pengambilan'),['judul' => 'SPJ Panjar']);

    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $data = new Spj_panjar;
        // $spj_panjar = Spj_panjar::all();
        // $rekening = Rekening::all();
        // return view('bendahara-pembantu.spj.tambah_spj_panjar',compact('spj_rekening', 'spj_panjar'),['judul' => 'Dashboard']);
    }


    public function spj_panjar($id){
        $spj_panjar = Spj_panjar::all();
        $rekening = Rekening::all();
        return view('bendahara-pembantu.spj.tambah_spj_panjar', compact('spj_panjar','rekening', 'spj_detail'), ['judul' => 'Tambah SPJ']);
    }

    public function input($id){
        $kop = Kop_surat::findOrFail($id);
        $spj = Spj_panjar::all();
        // $data = Spj_panjar::findOrfail($id);
        //dd($data);
        // $npd = Npd_pengajuan::find($id);
        $rekening = Rekening::get();

        $npd_detail = [];
        $data_npd = Npd_pengajuan::select('rekenings.*', 'npd_pengajuans.*')->where('kop_id', $id)->join('rekenings', 'rekenings.id', '=', 'npd_pengajuans.sub_rek_id')->get();
        foreach ($data_npd as $data) {
            $rek_id = $data['rek_id'];
            $rek_key = $rekening->search(function ($item, $key) use ($rek_id) {
                return $item->id == $rek_id;
            });
            $rek = $rekening[$rek_key];
            if (!isset($npd_detail[$rek['nama_kegiatan']])) {
                $npd_detail[$rek['nama_kegiatan']] = [];
            }
            $npd_detail[$rek['nama_kegiatan']][] = $data;
        }


        $npd_rek = Spj_panjar::join('rekenings', 'rekenings.id', '=', 'spj_panjars.sub_rek_id')->get();
        $spj_panjar = Spj_panjar::with(['npd'])->whereHas('npd', function($npd) use ($id) {
            $npd->where('kop_id', $id);
        })->get();
        //dd($npd);
        return view('bendahara-pembantu.spj.tambah_spj_panjar', compact( 'kop', 'npd_rek', 'spj', 'npd_detail', 'spj_panjar', 'rekening'), ['judul' => 'Tambah SPJ', 'npd_id' => $id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $spj_panjar = Spj_panjar::create([
            'npd_id'=>$request->npd_id,
            'rek_id'=>$request->rek_id,
            'sub_rek_id'=>$request->sub_rek_id,
            'jml_panjar'=>$request->jml_panjar,
            'jenis_spj'=>$request->jenis_spj,
            'spj_panjar'=>$request->spj_panjar,
            'tgl_entry'=>$request->tgl_entry
        ]);
        return redirect("/spj/{$spj_panjar->npd->kop_id}/spj")->with('success', 'Berhasil Menambahkan Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $data = Spj_panjar::findOrfail($id);
        // //dd($data);
        // // $npd = Npd_pengajuan::find($id);
        // $spj = Spj_panjar::with(['npd'])->where('npd_id', $id)->get();
        // $edit = Spj_panjar::with(['npd'])->where('npd_id', $id)->where('id', $id1)->firstOrFail();
        // //dd($npd);
        // $rekening = Rekening::get();
        // return view('bendahara-pembantu.spj.edit_panjar', compact('data', 'spj', 'rekening', 'edit'), ['judul' => 'Ubah Spj', 'npd_id' => $id]);

        $spj_panjar = Spj_panjar::findOrfail($id);
        // $data = Spj_panjar::findOrfail($id);
        //dd($data);
        // $npd = Npd_pengajuan::find($id);
        $npd_detail = Npd_pengajuan::join('rekenings', 'rekenings.id', '=', 'npd_pengajuans.sub_rek_id')->get();
        // $spj_edit = Spj_panjar::with(['npd'])->where('npd_id', $id)->get();
        //dd($npd);
        $rekening = Rekening::get();
        return view('bendahara-pembantu.spj.edit_panjar', compact( 'npd_detail', 'spj_panjar', 'rekening'), ['judul' => 'Ubah SPJ', 'npd_id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Spj_panjar::find($id);
        $data->update($request->all());

        return redirect("/spj/{$data->npd->kop_id}/spj")->with('success', 'Berhasil Mengubah Data');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $id2)
    {
        $spj = Spj_panjar::with(['npd'])->where('npd_id', $id)->where('id', $id2)->firstOrFail();
        if ($spj->delete())
            return response()->json(['success' => true]);
            return response()->json(['success' => false, 'message' => 'Terjadi kesalahan']);

    }

    public function file($id)
    {
        $npd_pengajuan = Npd_pengajuan::where('kop_id', $id)->get()->pluck('id');
        $pengambilan = Pengambilan::where('kop_id', $id)->firstOrfail();
        $spjpanjar = Spj_panjar::whereIn('npd_id', $npd_pengajuan)->get();
        $spj_panjar = $spjpanjar->
        groupBy(
            function($data) {
                return $data->rek_id; 
            }
        );
        $pengembalian = 0;
        $total = 0;
        foreach ($spj_panjar as $spjs) {
            foreach ($spjs as $spj) {
                $pengembalian += $spj->jml_panjar;
            }
        }

        
        // $spj_panjar_details = Spj_panjar_detail::whereIn('panjar_id', function($query){
        //     $query->select('id')
        //     ->from(with(new Spj_panjar)->getTable())
        //     ->where('npd_id', $Npd_pengajuan);
        // })->get();

        $spj_panjar_details = Spj_panjar_detail::whereIn('panjar_id', $spjpanjar->pluck('id')->all())->with('spj_pajak')->get();
        foreach ($spj_panjar_details as $detail) {
            $pengembalian -= $detail->panjar_spj; 
            $total += $detail->panjar_spj; 
        }
        $kop = Kop_surat::findOrFail($id);
        $kop->pengembalian = $pengembalian;
        $kop->save();
        // $spj_panjar_detail_pajaks = Pajak::whereIn('detail_id', $spj_panjar_details)
        // ->get();
        $ttd = DataTtd::where('bidang', $kop->bidang)->orderBy('created_at', 'desc')->firstOrFail();

        $repeatHeader = filter_var(request()->get('repeatHeader'), FILTER_VALIDATE_BOOLEAN);
        $pdf = PDF::loadView('bendahara-pembantu.spj.fileSPJ.file', compact('spj_panjar', 'pengambilan', 'npd_pengajuan', 'repeatHeader', 'spj_panjar_details', 'total', 'ttd'));
        return $pdf->setPaper('a3', 'landscape')->stream("SPJ Panjar.pdf");
    }
}