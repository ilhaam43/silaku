<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;


class LoginController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function showLoginForm(string $role = 'admin')
    {
        switch ($role) {
            case 'admin':
                $jabatan = 'Admin';
                break;
            case 'bentama':
                $jabatan = 'Bendahara Setda';
                break;
            case 'benpem':
                $jabatan = 'Bendahara Pembantu';
                break;
            default:
                abort(404);
                break;
        }
        return view('auth.login', compact('jabatan'));
    }
    
    protected $redirectTo;
    public function redirectTo()
    {
        switch(Auth::user()->role){
            case 'Admin':
            $this->redirectTo = '/admin';
            return $this->redirectTo;
                break;
            case 'Bentama':
                $this->redirectTo = '/bentama';
                return $this->redirectTo;
                break;
            case 'Benpem':
                $this->redirectTo = '/benpem';
                return $this->redirectTo;
                break;

            default:
                $this->redirectTo = '/login';
                return $this->redirectTo;
        }
         
        // return $next($request);
    } 
}