<?php

namespace App\Http\Controllers;

use App\Models\Kop_surat;
use Illuminate\Http\Request;

class SetdaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('setda.home',['judul' => 'Dashboard']);
    }

    public function showPersetujuan()
    {
        $kop = Kop_surat::all();
        $judul = "Persetujuan NPD";
        return view('setda.npd_persetujuan', compact('judul', 'kop'));
    }

    public function verifikasiKop(Request $request, Kop_surat $kop)
    {
        $data = $request->json()->all();
        $kop->disetujui = (int) $data['disetujui'];
        if ($kop->save())
            return response()->json(['success' => true, 'data' => $kop]);
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan']);
    }
}
