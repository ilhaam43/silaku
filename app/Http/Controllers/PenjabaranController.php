<?php

namespace App\Http\Controllers;

use App\Models\Kop_surat;
use App\Models\Rekening;
use App\Models\Spj_panjar;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PenjabaranController extends Controller
{
    public function index()
    {
        $judul = 'Penjabaran';
        $penjabaran = Kop_surat::distinct()->get([DB::raw('YEAR(tgl_dpa) as tahun')]);
        return view('admin.penjabaran', compact('judul', 'penjabaran'));
    }

    public function file($tahun)
    {
        $repeatHeader = filter_var(request()->get('repeatHeader'), FILTER_VALIDATE_BOOLEAN);
        $spj = Spj_panjar::whereYear('tgl_entry', $tahun)->with('spjdetail')->get()->groupBy('sub_rek_id');
        $total_anggaran = 0;
        $anggaran = [];
        $total_realisasi = 0;
        $realisasi = [];
        foreach ($spj as $rek_id => $items) {
            foreach ($items as $item) {
                $total_anggaran += $item->npd->perubahan;
                if (isset($anggaran[$rek_id]))
                $anggaran[$rek_id] += $item->npd->perubahan;
                else $anggaran[$rek_id] = $item->npd->perubahan;

                $total_realisasi += $item->spj_panjar;
                if (isset($realisasi[$rek_id]))
                $realisasi[$rek_id] += $item->spj_panjar;
                else $realisasi[$rek_id] = $item->spj_panjar;
            }
        }
        $default = Rekening::where('level', 0)->with('subs')->firstOrFail();
        $pdf = PDF::loadView('admin.filePenjabaran.file', compact('tahun', 'repeatHeader', 'spj', 'default', 'anggaran', 'total_anggaran', 'realisasi', 'total_realisasi'));
        return $pdf->setPaper('a3', 'landscape')->stream("Penjabaran Tahun $tahun.pdf");
    }
}