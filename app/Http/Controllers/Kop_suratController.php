<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kop_surat;
// use RealRashid\SweetAlert\Facades\Alert;


class Kop_suratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Kop_surat::all();
        return view('bendahara-pembantu.npd.npd_pengajuan',compact('datas'),['judul' => 'Nomer Kop Surat']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kop_surat = Kop_surat::create([
            'nomor_kop'=>$request->nomor_kop,
            'nomor_dpa'=>$request->nomor_dpa,
            'tgl_dpa'=>$request->tgl_dpa,
            'bidang'=>auth()->user()->level
        ]);
        return redirect('/npd/npd_pengajuan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        //dd($data);
        // $npd = Npd_pengajuan::find($id);
        $kop = Kop_surat::findOrFail($id);
        if ($kop->update($data)) {
            // Alert::success('Berhasil', 'Berhasi mengubah data pengajuan');
        } else {
            // Alert::warning('Gagal', 'Gagal mengubah data pengajuan');
        }
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kop = Kop_surat::where('id', $id)->findOrFail($id);
        if ($kop->delete())
            return response()->json(['success' => true]);
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan']);
    }
}
