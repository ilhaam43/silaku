<?php

namespace App\Http\Controllers;

use App\Models\spj_panjar;
use App\Http\Requests\Storespj_panjarRequest;
use App\Http\Requests\Updatespj_panjarRequest;

class SpjPanjarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Storespj_panjarRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Storespj_panjarRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\spj_panjar  $spj_panjar
     * @return \Illuminate\Http\Response
     */
    public function show(spj_panjar $spj_panjar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\spj_panjar  $spj_panjar
     * @return \Illuminate\Http\Response
     */
    public function edit(spj_panjar $spj_panjar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Updatespj_panjarRequest  $request
     * @param  \App\Models\spj_panjar  $spj_panjar
     * @return \Illuminate\Http\Response
     */
    public function update(Updatespj_panjarRequest $request, spj_panjar $spj_panjar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\spj_panjar  $spj_panjar
     * @return \Illuminate\Http\Response
     */
    public function destroy(spj_panjar $spj_panjar)
    {
        //
    }
}
