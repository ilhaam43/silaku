<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\BKU;
use App\Models\Pajak;
use App\Models\DataBku;
use App\Models\DataTtd;
use App\Models\Rekening;
use App\Models\Kop_surat;
use App\Models\spj_panjar;
use App\Models\Pengambilan;
use App\Models\detailDataBku;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\Spj_panjar_detail;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreBKURequest;
use App\Http\Requests\UpdateBKURequest;

class BKUController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $BKU = BKU::all();
        $judul = 'Kode Rekening';
        // dd($BKU);
        return view('admin.daftarBKU', [
            'BKU' => $BKU,
            'judul' => $judul,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBKURequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBKURequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BKU  $bKU
     * @return \Illuminate\Http\Response
     */
    public function show(BKU $bKU)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BKU  $bKU
     * @return \Illuminate\Http\Response
     */
    public function edit(BKU $bKU)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBKURequest  $request
     * @param  \App\Models\BKU  $bKU
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBKURequest $request, BKU $bKU)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BKU  $bKU
     * @return \Illuminate\Http\Response
     */
    public function destroy(BKU $bKU)
    {
        //
    }

    public function cetak($id)
    {
        $bku = BKU::find($id);
        $year = $bku->year;
        $month = $bku->month;
        $subBag = $bku->subBag;
        $tz = new \DateTimeZone('Asia/Jakarta');
        $lastmonth = $month - 1;
        $penerimaan = 0;
        $pengeluaran = 0;
        $penerimaanLast = 0;
        $pengeluaranLast = 0;

        $dataTTD = DataTtd::where('bidang', '=', $subBag)->firstOrFail();

        $kop = Kop_surat::whereYear('tgl_dpa', '=', $year)
            ->whereMonth('tgl_dpa', '=', $month)
            ->where('disetujui', 1)
            ->where('bidang', '=', $subBag)
            ->get();

        foreach ($kop as $data) {
            $data->ambilUang = Pengambilan::where('kop_id', '=', $data->id)->first();
            $pengambilanUang = 0;

            $dataBKU = DataBku::where('kop_id', '=', $data->id)
                ->where('rekenings.level', '>', '2')

                ->join('rekenings', 'rekenings.id', '=', 'data_bkus.sub_rek_id')
                ->select('data_bkus.*',  'rekenings.*')->get();
            $data->BKU = $dataBKU;





            $j = 0;
            // dd($data->BKU);
            foreach ($data->BKU as $BKU) {
                if ($BKU->level == 5) {
                    $pengambilanUang = $pengambilanUang + $BKU->penerimaan;
                    // echo $BKU->penerimaan;
                }
                $idBKU =  DataBku::where('kop_id', '=', $BKU->kop_id)
                    ->where('npd_id', '=', $BKU->npd_id)
                    ->where('spj_id', '=', $BKU->spj_id)
                    ->first();


                $BKU->bku_id = $idBKU->id;





                $detailSpj = Spj_panjar_detail::where('panjar_id', '=',  $BKU->spj_id)->get();
                $BKU->detail = $detailSpj;

                foreach ($BKU->detail as $detail) {

                    $rek = Rekening::where('id', '=', $detail->sub_rek_id)->first();
                    $detail->kode_rekening = $rek->kode_rekening;
                    $detail->nama_kegiatan = $rek->nama_kegiatan;
                    $pajaks = Pajak::where('detail_id', '=', $detail->id)->get();
                    $detail->pajaks = $pajaks;
                }


                $data->pengambilanUang = $pengambilanUang;
            }
        }


        $KOPLAST = Kop_surat::whereBetween('tgl_dpa', [Carbon::createFromDate($year, 1, 1, $tz), Carbon::createFromDate($year, $lastmonth, 31, $tz)])
            ->where('disetujui', '=', 1)->where('bidang', '=', $subBag)
            ->get();
        foreach ($KOPLAST as $datanyaKOP) {
            $kopId = $datanyaKOP->id;

            $DataBKULast = DataBku::where('kop_id', $kopId)->get();
            $pengeluaranLast += $datanyaKOP->pengembalian;
            foreach ($DataBKULast as $datanya) {

                $penerimaanLast =  $penerimaanLast + $datanya->penerimaan;
                $pengeluaranLast =  $pengeluaranLast + $datanya->pengeluaran;

                $idBKU2 = $datanya->id;

                // echo $idBKU2;

                $pajak2 = detailDataBku::where('dataBKU_id', '=', $idBKU2)->get();
                foreach ($pajak2 as $dataPajak) {

                    $penerimaanLast += $dataPajak->penerimaan;
                    $pengeluaranLast += $dataPajak->pengeluaran;
                }
            }
        }
        // dd($BKU->detail);

        $repeatHeader = filter_var(request()->get('repeatHeader'), FILTER_VALIDATE_BOOLEAN);
        $pdf = PDF::loadView('admin.bku', [
            'kop' => $kop,
            'bku' => $bku,
            'penerimaanLalu' => $penerimaanLast,
            'pengeluaranLalu' => $pengeluaranLast,
            'ttd' => $dataTTD,

        ], compact('repeatHeader'));
        return $pdf->setPaper('a3', 'landscape')->stream();
    }
}
