<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users'] = User::where('role', '<>', 'Admin')->get();
        $data['judul'] = 'Daftar Pengguna Sistem';
        return view('admin.users', $data);
    }

    public function list(Request $request)
    {
        $res['data'] = User::all();
        $res['success'] = true;
        return response()->json($res);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->json()->all();
        $data['password'] = Hash::make($data['password']);
        if ($data['role'] != 'Benpem') {
            $data['level'] = null;
        }
        $user = User::create($data);
        if ($user->save())
            return response()->json(['success' => true, 'data' => $user]);
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $data['data'] = $user;
        $data['success'] = true;
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $data = $request->json()->all();
        if (isset($data['password'])) $data['password'] = Hash::make($data['password']);
        if ($data['role'] != 'Benpem') {
            $data['level'] = null;
        }
        if ($user->update($data))
            return response()->json(['success' => true, 'data' => $user]);
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->delete())
            return response()->json(['success' => true]);
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan']);
    }
}
