<?php

namespace App\Http\Controllers;

use App\Models\Spj_panjar_detail;
use App\Http\Requests\StoreSpj_panjar_detailRequest;
use App\Http\Requests\UpdateSpj_panjar_detailRequest;

class SpjPanjarDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSpj_panjar_detailRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSpj_panjar_detailRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Spj_panjar_detail  $spj_panjar_detail
     * @return \Illuminate\Http\Response
     */
    public function show(Spj_panjar_detail $spj_panjar_detail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Spj_panjar_detail  $spj_panjar_detail
     * @return \Illuminate\Http\Response
     */
    public function edit(Spj_panjar_detail $spj_panjar_detail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSpj_panjar_detailRequest  $request
     * @param  \App\Models\Spj_panjar_detail  $spj_panjar_detail
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSpj_panjar_detailRequest $request, Spj_panjar_detail $spj_panjar_detail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Spj_panjar_detail  $spj_panjar_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Spj_panjar_detail $spj_panjar_detail)
    {
        //
    }
}
