<?php

namespace App\Http\Controllers;

use App\Models\Rekening;
use Illuminate\Http\Request;

class RekeningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['rekening'] = Rekening::where('parent_id', null)->get();
        $data['judul'] = 'Kode Rekening';
        return view('admin.rekening', $data);
    }

    public function list()
    {
        $res['data'] = Rekening::all();
        $res['success'] = true;
        return response()->json($res);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->json()->all();
        $out = new \Symfony\Component\Console\Output\ConsoleOutput();
        $out->writeln('kode_rekening: ' . $data['kode_rekening']);
        $out->writeln('level: ' . $data['level']);
        $out->writeln('nama_kegiatan: ' . $data['nama_kegiatan']);
        $out->writeln('parent_id: ' . $data['parent_id']);
        $rek = Rekening::create(
            [
                'kode_rekening' => $data['kode_rekening'],
                'level' => $data['level'],
                'nama_kegiatan' => $data['nama_kegiatan'],
                'parent_id' => $data['parent_id'] ?: null
            ]
        );
        if ($rek->save())
            return response()->json(['success' => true, 'data' => $rek]);
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rekening  $rekening
     * @return \Illuminate\Http\Response
     */
    public function show(Rekening $rekening)
    {
        $data['data'] = $rekening;
        $data['success'] = true;
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Rekening  $rekening
     * @return \Illuminate\Http\Response
     */
    public function edit(Rekening $rekening)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Rekening  $rekening
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rekening $rekening)
    {
        $data = $request->json()->all();
        $rekening->kode_rekening = $data['kode_rekening'];
        $rekening->level = $data['level'];
        $rekening->parent_id = $data['parent_id'] ?: null;
        $rekening->nama_kegiatan = $data['nama_kegiatan'];
        if ($rekening->save())
            return response()->json(['success' => true, 'data' => $rekening]);
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rekening  $rekening
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rekening $rekening)
    {
        if ($rekening->delete())
            return response()->json(['success' => true]);
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan']);
    }

    public function ajaxKegiatan(Request $request){
        $rekening = Rekening::where('kode_rekening', $request['rekening'])->first();
        $kegiatan = $rekening->nama_kegiatan;
        return response()->json(['success'=> true, 'kegiatan' => $kegiatan]);
    }
}
