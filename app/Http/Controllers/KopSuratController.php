<?php

namespace App\Http\Controllers;

use App\Models\BKU;
use App\Models\daftarFungsional;
use App\Models\Kop_surat;
use Illuminate\Http\Request;
use App\Http\Requests\StoreKop_suratRequest;
use App\Http\Requests\UpdateKop_suratRequest;
use DateTime;

class KopSuratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreKop_suratRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreKop_suratRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kop_surat  $kop_surat
     * @return \Illuminate\Http\Response
     */
    public function show(Kop_surat $kop_surat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kop_surat  $kop_surat
     * @return \Illuminate\Http\Response
     */
    public function edit(Kop_surat $kop_surat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateKop_suratRequest  $request
     * @param  \App\Models\Kop_surat  $kop_surat
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateKop_suratRequest $request, Kop_surat $kop_surat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kop_surat  $kop_surat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kop_surat $kop_surat)
    {
        //
    }

    public function verifikasiKop(Request $request, Kop_surat $kop)
    {
        $data = $request->json()->all();
        $kop->disetujui = (int) $data['disetujui'];
        
        
        if ($kop->save()){

            $date = DateTime::createFromFormat('Y-m-d', $kop->tgl_dpa);
            $month = date_format($date, "m");
            $year = date_format($date, "Y");

            // buat daftar fungsional
            $cekFungsional = daftarFungsional::where('year', '=', $year)
            ->where('month', '=', $month)
            ->where('subBag', '=' , $kop->bidang)
            ->get();
            
            if($cekFungsional->isEmpty()){
                $fungsional = daftarFungsional::create([
                    'month' => $month,
                    'year' => $year,
                    'subBag' => $kop->bidang,
                ]);
            }

            // buat daftar BKU
            $cekBKU = BKU::where('year', '=', $year)
            ->where('month', '=', $month)
            ->where('subBag', '=' , $kop->bidang)
            ->get();

            if($cekBKU->isEmpty()){
                    $BKU = BKU::create([
                        'month' => $month,
                        'year' => $year,
                        'subBag' => $kop->bidang,
                    ]);
            }

            return response()->json(['success' => true, 'data' => $kop]);
        }
        else{
            return response()->json(['success' => false, 'message' => 'Terjadi kesalahan']);
        }
    }
}
