<?php

namespace App\Http\Controllers;

use DateTimeZone;
use Carbon\Carbon;
use App\Models\DataBku;
use App\Models\DataTtd;
use App\Models\Rekening;
use App\Models\Kop_surat;
use App\Models\Spj_panjar;
use Illuminate\Http\Request;
use App\Models\detailDataBku;
use App\Models\Npd_pengajuan;
use App\Models\DataFungsional;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\daftarFungsional;
use App\Models\Spj_panjar_detail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.home', ['judul' => 'Dashboard']);
    }



    public function cetak($id)
    {
        $rekening = Rekening::with('induk')->get();

        $penerimaanAll = NULL;
        $pengeluaranAll = NULL;
        $all = NULL;
        $fungsional = daftarFungsional::find($id);
        $tz = new \DateTimeZone('Asia/Jakarta');
        $year = $fungsional->year;
        $month = $fungsional->month;
        $subBag = $fungsional->subBag;
        $spjSampaiBulanLalu = NULL;
        $jmlSpjSampaiBulanLalu = NULL;
        $NPD2D = 0;
        $SPJLSUP = 0;
        $dataFungsional = DataFungsional::where('fungsionalId', '=', $id)->first();

        $startDate = Carbon::createFromDate($year, 1)->startOfMonth();
        $endDate = Carbon::createFromDate($year, $month)->endOfMonth();
        // echo $endDate;
        $k = 0;
        $count2 = 0;
        $count3 = 0;
        $count4 = 0;
        $count5 = 0;

        // // dd($year);
        // dd($fungsional);
        $dataTTD = DataTtd::where('bidang', '=', $subBag)->firstOrFail();

        $lastmonth = $month - 1;
        $kop = Kop_surat::whereYear('tgl_dpa', '=', $year)
            ->whereMonth('tgl_dpa', '=', $month)
            ->where('disetujui', 1)
            ->where('bidang', '=', $subBag)
            ->get();

        // dd($kop);
        $kops = [];

        $i = 1;
        if (!empty($kop)) {
            foreach ($kop as $data) {

                $data->lvl2 = Rekening::where('level', '=', 2)->get();

                // dd($lvl2);
                foreach ($data->lvl2 as $rek2) {
                    $rek2->lvl3 = Rekening::where('level', '=', 3)
                        ->where('parent_id', '=', $rek2->id)
                        ->get();

                    $rek3totalMurni = 0;

                    foreach ($rek2->lvl3 as $rek3) {

                        $rek4penerimaan = 0;
                        $rek4totalMurni = 0;
                        $rek4totalPergeseran = 0;
                        $rek4totalPerubahan = 0;
                        $rek4totalBulanLalu = 0;
                        $rek4totalBulanIni = 0;
                        // $rek4totalSPJ = 0;
                        $rek4totaljmlSpjSampaiBulanLalu = 0;

                        $rek3->lvl4 = Rekening::where('level', '=', 4)
                            ->where('parent_id', '=', $rek3->id)
                            ->get();


                        foreach ($rek3->lvl4 as $rek4) {
                            $rek4->NPDBKU = DB::table('data_bkus')
                                ->join('npd_pengajuans', 'npd_pengajuans.id', '=', 'data_bkus.npd_id')
                                ->join('rekenings', 'rekenings.id', '=', 'data_bkus.sub_rek_id')
                                ->where('npd_pengajuans.kop_id', '=', $data->id)
                                ->where('npd_pengajuans.rek_id', '=', $rek4->id)
                                ->select('npd_pengajuans.*', 'data_bkus.*', 'rekenings.*')
                                ->get();

                            if (empty($rek4->NPDBKU)) {
                                unset($rek3->lvl4[$count4]);
                            }

                            $count4++;


                            $data->kegiatan = collect();

                            if (!empty($rek4->NPDBKU)) {
                                $j = 0;
                                $spjSampaiBulanLalu = 0;
                                $jmlSpjSampaiBulanLalu = 0;

                                $NPDpenerimaan = 0;
                                $NPDtotalMurni = 0;
                                $NPDtotalPergeseran = 0;
                                $NPDtotalPerubahan = 0;
                                $NPDtotalBulanLalu = 0;
                                $NPDtotalBulanIni = 0;
                                // $NPDtotalSPJ = 0;
                                $NPDtotaljmlSpjSampaiBulanLalu = 0;

                                foreach ($rek4->NPDBKU as $NPDBKU) {
                                    $rek_id = $NPDBKU->rek_id;
                                    $sub_rek_id = $NPDBKU->sub_rek_id;

                                    $rek = $rekening->search(function ($item, $key) use ($rek_id) {
                                        return $item->id == $rek_id;
                                    });
                                    $keg = $rekening->get($rek)->induk;
                                    // dd($keg);

                                    if (!$data->kegiatan->has($keg->id)) {
                                        $data->kegiatan->put($keg->id, $keg);
                                        $kegiatan = $data->kegiatan->get($keg->id);
                                        $kegiatan->total_anggaran = 0;
                                        $kegiatan->total_pergeseran = 0;
                                        $kegiatan->total_perubahan = 0;
                                    }



                                    if (isset($data->kegiatan->get($keg->id)->subkegiatan))
                                        $subs = $data->kegiatan->get($keg->id)->subkegiatan;
                                    else
                                        $subs = $data->kegiatan->get($keg->id)->subkegiatan = collect();

                                    if (!$subs->has($rek_id)) {
                                        $subs->put($rek_id, $rekening->get($rek));
                                        $sub = $subs->get($rek_id);
                                        $sub->total_anggaran = 0;
                                        $sub->total_pergeseran = 0;
                                        $sub->total_perubahan = 0;
                                    }




                                    if (isset($subs->get($rek_id)->belanjas))
                                        $belanjas = $subs->get($rek_id)->belanjas;
                                    else
                                        $belanjas = $subs->get($rek_id)->belanjas = collect();

                                    $belanjas->push($NPDBKU);

                                    $idBKU = DataBku::where('kop_id', '=', $NPDBKU->kop_id)
                                        ->where('npd_id', '=', $NPDBKU->npd_id)
                                        ->where('spj_id', '=', $NPDBKU->spj_id)
                                        ->first();

                                    $NPDBKU->idBKU = $idBKU->id;

                                    $BKULAST[$j] = DB::table('data_bkus')
                                        ->whereYear('tanggal', '=', $year)
                                        ->whereBetween('tanggal', [Carbon::createFromDate($year, 1, 1)->startOfDay(), Carbon::createFromDate($year, $lastmonth, 31)->endOfDay()])
                                        ->where('sub_rek_id', '=', $NPDBKU->sub_rek_id)
                                        ->get();

                                    foreach ($BKULAST[$j] as $bkuLast) {
                                        $NPDLAST = Npd_pengajuan::find($bkuLast->npd_id);
                                        $bkuLast->npd_anggaranMurni = $NPDLAST->anggaran_murni;
                                        $bkuLast->npd_anggaranPergeseran = $NPDLAST->pergeseran;
                                        $bkuLast->npd_anggaranPerubahan = $NPDLAST->perubahan;

                                        $spjSampaiBulanLalu =  $spjSampaiBulanLalu + $bkuLast->pengeluaran;
                                        $jmlSpjSampaiBulanLalu =  $jmlSpjSampaiBulanLalu + $bkuLast->penerimaan;
                                    }


                                    $NPDBKU->SPJdetail = Spj_panjar_detail::where('panjar_id', '=', $NPDBKU->spj_id)->get();


                                    foreach ($NPDBKU->SPJdetail as $SPJdetail) {
                                        $SPJdetailLasts = Spj_panjar_detail::where('sub_rek_id', '=', $SPJdetail->sub_rek_id)
                                            ->whereYear('entry_tgl', '=', $year)
                                            ->whereBetween('entry_tgl', [Carbon::createFromDate($year, 1, 1)->startOfDay(), Carbon::createFromDate($year, $lastmonth, 31)->endOfDay()])
                                            ->get();
                                        $detailPenerimaanLalu = 0;
                                        $detailPengeluaranLalu = 0;

                                        foreach ($SPJdetailLasts as $SPJdetailLast) {
                                            $detailPenerimaanLalu += $SPJdetailLast->panjar_jml;
                                            $detailPengeluaranLalu += $SPJdetailLast->panjar_spj;
                                        }
                                        $SPJdetail->panjar_jmlLalu = $detailPenerimaanLalu;
                                        $SPJdetail->panjar_spjLalu = $detailPengeluaranLalu;

                                        $rekDetail = Rekening::where('id', '=', $SPJdetail->sub_rek_id)->first();
                                        // dd($rekDetail);
                                        $SPJdetail->kode_rekening = $rekDetail->kode_rekening;
                                        $SPJdetail->nama_kegiatan = $rekDetail->nama_kegiatan;
                                    }

                                    $NPDBKU->spjSampaiBulanLalu =  $spjSampaiBulanLalu;
                                    $NPDBKU->jmlSpjSampaiBulanLalu =  $jmlSpjSampaiBulanLalu;

                                    $j++;
                                    $k++;

                                    $NPDpenerimaan += $NPDBKU->penerimaan;
                                    $NPDtotalMurni += $NPDBKU->anggaran_murni;
                                    $NPDtotalPergeseran += $NPDBKU->pergeseran;
                                    $NPDtotalPerubahan += $NPDBKU->perubahan;
                                    $NPDtotalBulanLalu += $NPDBKU->spjSampaiBulanLalu;
                                    $NPDtotalBulanIni += $NPDBKU->pengeluaran;
                                    // $NPDtotalSPJ += $NPDBKU->spjPanjar;
                                    $NPDtotaljmlSpjSampaiBulanLalu +=   $NPDBKU->jmlSpjSampaiBulanLalu;
                                }
                            }
                            $rek4->penerimaan =  $NPDpenerimaan;
                            $rek4->anggaran_murni =  $NPDtotalMurni;
                            $rek4->pergeseran = $NPDtotalPergeseran;
                            $rek4->perubahan = $NPDtotalPerubahan;
                            $rek4->SpjSampaiBulanLalu = $NPDtotalBulanLalu;
                            $rek4->pengeluaran = $NPDtotalBulanIni;
                            // $rek4->spjPanjar = $NPDtotalSPJ;
                            $rek4->jmlSpjSampaiBulanLalu = $NPDtotaljmlSpjSampaiBulanLalu;


                            $rek4penerimaan += $rek4->penerimaan;
                            $rek4totalMurni += $rek4->anggaran_murni;
                            $rek4totalPergeseran += $rek4->pergeseran;
                            $rek4totalPerubahan += $rek4->perubahan;
                            $rek4totalBulanLalu += $rek4->SpjSampaiBulanLalu;
                            $rek4totalBulanIni += $rek4->pengeluaran;
                            // $rek4totalSPJ +=$rek4->spjPanjar ;
                            $rek4totaljmlSpjSampaiBulanLalu += $rek4->jmlSpjSampaiBulanLalu;
                        }

                        if ($rek3->lvl4->count()) {
                        } else {
                            unset($rek2->lvl3[$count3]);
                        }
                        $count3++;

                        $rek3->penerimaan = $rek4penerimaan;
                        $rek3->anggaran_murni = $rek4totalMurni;
                        $rek3->pergeseran = $rek4totalPergeseran;
                        $rek3->perubahan =  $rek4totalPerubahan;
                        $rek3->spjSampaiBulanLalu = $rek4totalBulanLalu;
                        $rek3->pengeluaran =  $rek4totalBulanIni;
                        // $rek3->jumlahSPK = $rek4totalSPJ;

                        $rek3->jmlSpjSampaiBulanLalu = $rek4totaljmlSpjSampaiBulanLalu;

                        $rek3totalMurni += $rek3->anggaran_murni;
                    }

                    if ($rek2->lvl3->count()) {
                    } else {
                        unset($data->lvl2[$count2]);
                    }
                    $count2++;
                    $rek2->anggaran_murni = $rek3totalMurni;
                }

                $kops[] = $data->kegiatan;
                // };
            }




            $kopSekarang =   Kop_surat::whereYear('tgl_dpa', '=', $year)
                ->where('tgl_dpa', '<', $endDate)
                ->where('disetujui', 1)
                ->where('bidang', '=', $subBag)
                ->get();

            // dd($kopSekarang);

            $pajakMasuk = 0;
            $pajakKeluar = 0;
            $pencairan =  0;

            foreach ($kopSekarang as $kopS) {
                $npdall = Npd_pengajuan::join('rekenings', 'rekenings.id', '=', 'npd_pengajuans.sub_rek_id')

                    ->where('kop_id', '=', $kopS->id)
                    ->select('npd_pengajuans.*',  'rekenings.level')->get();



                $bkuall = DataBku::join('rekenings', 'rekenings.id', '=', 'data_bkus.sub_rek_id')

                    ->where('kop_id', '=', $kopS->id)
                    ->select('data_bkus.*',  'rekenings.level')->get();


                foreach ($npdall as $npd) {
                    if ($npd->level == 5) {
                        $pencairan = $pencairan + $npd->pencairan_saat_ini;
                    }
                }

                foreach ($bkuall as $bkuS) {
                    $pajaks = detailDataBku::where('dataBKU_id', '=', $bkuS->id)->get();
                    foreach ($pajaks as $pajak) {
                        $pajakMasuk = $pajakMasuk + $pajak->penerimaan;
                        $pajakKeluar = $pajakKeluar + $pajak->pengeluaran;
                    }
                }
            }


            // dd($pajakMasuk);
            $KOPLAST = Kop_surat::whereBetween('tgl_dpa', [Carbon::createFromDate($year, 1, 1, $tz), Carbon::createFromDate($year, $month, 31, $tz)])
                ->where('disetujui', '=', 1)->where('bidang', '=', $subBag)
                ->get();

            foreach ($KOPLAST as $datanyaKOP) {
                $kopId = $datanyaKOP->id;

                $DataNPDLast = Npd_pengajuan::where('kop_id', $kopId)->get();
                foreach ($DataNPDLast as $datanyaNPD) {
                    $NPD2D += $datanyaNPD['pencairan_saat_ini'];
                }

                $DataBKULast = DataBku::where('kop_id', $kopId)->get();

                foreach ($DataBKULast as $datanyaBKU) {

                    $SPJLSUP += $datanyaBKU['pengeluaran'];
                }
            }




            $penerimaanAll['pajak'] = $pajakMasuk;
            $penerimaanAll['npd'] = $pencairan;
            $pengeluaranAll['pajak'] = $pajakKeluar;


            // dd($kop);
            $repeatHeader = filter_var(request()->get('repeatHeader'), FILTER_VALIDATE_BOOLEAN);
            $pdf = PDF::loadView('admin.fungsional', [
                'kop' => $kop,
                'dataFungsional' => $dataFungsional,
                'all' => $all,
                'penerimaanAll' => $penerimaanAll,
                'pengeluaranAll' => $pengeluaranAll,
                'ttd' => $dataTTD,
                'NPD2D' => $NPD2D,
                'SPJLSUP' => $SPJLSUP,

            ], compact('repeatHeader'));
            return $pdf->setPaper('a3', 'landscape')->stream();
        }
    }

    public function profile()
    {
        $user = auth()->user();
        return view('admin.profile', ['judul' => 'Profil', 'user' => $user]);
    }

    public function updateProfile()
    {
        $user = auth()->user();
        $data = request()->all();
        if ($data['password']) {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }
        if ($user->update($data)) {
            return redirect()->route('admin.profile')->with('alert', 'Data berhasil diupdate!')->with('alert_type', 'success');
        }
        return redirect('/admin/profile')->with('alert', 'Data gagal diupdate!')->with('alert_type', 'danger');
    }
}
