<?php

namespace App\Http\Controllers;

use App\Models\BKU;
use Illuminate\Http\Request;
use App\Models\Rekening;
use App\Models\Npd_pengajuan;
use App\Models\Kop_surat;
use App\Models\DataTtd;
use App\Models\User;
use RealRashid\SweetAlert\Facades\Alert;
use Barryvdh\DomPDF\Facade\Pdf;
use DateTime;

class Npd_pengajuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $npd = Npd_pengajuan::all();
        $kop_surat = Kop_surat::where('bidang', auth()->user()->level)->get();
        return view('bendahara-pembantu.npd.npd_pengajuan',compact('kop_surat', 'npd'),['judul' => 'Dashboard']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $rekening = Rekening::all();
        $npd = Npd_pengajuan::all();
        return view('bendahara-pembantu.npd.tambah_npd_pengajuan', compact('rekening', 'npd'), ['judul' => 'Tambah NPD']);
    }

    public function npd($id){
        $kop_surat = Kop_surat::all();
        $rekening = Rekening::all();
        return view('bendahara-pembantu.npd.tambah_npd_pengajuan', compact('kop_surat','rekening', 'npd'), ['judul' => 'Tambah NPD']);
    }

    public function input($id){
        $data = Kop_surat::findOrfail($id);
        //dd($data);
        // $npd = Npd_pengajuan::find($id);
        $npd = Npd_pengajuan::with(['kopsurat'])->where('kop_id', $id)->get();
        //dd($npd);
        $rekening = Rekening::get();
        return view('bendahara-pembantu.npd.tambah_npd_pengajuan', compact('data', 'npd', 'rekening'), ['judul' => 'Tambah NPD', 'kop_id' => $id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['bidang'] = auth()->user()->level;

        $cek = Npd_pengajuan::create($data);
        if ($cek == true) {
            Alert::success('Berhasil', 'Berhasi menambahkan data pengajuan');
        } else {
            Alert::warning('Gagal', 'Gagal menambahkan data pengajuan');
        }
        return back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $id2)
    {
        $data = Kop_surat::findOrfail($id);
        //dd($data);
        // $npd = Npd_pengajuan::find($id);
        $npd = Npd_pengajuan::with(['kopsurat'])->where('kop_id', $id)->get();
        $edit = Npd_pengajuan::with(['kopsurat'])->where('kop_id', $id)->where('id', $id2)->firstOrFail();
        //dd($npd);
        $rekening = Rekening::get();
        return view('bendahara-pembantu.npd.edit_npd_pengajuan', compact('data', 'npd', 'rekening', 'edit'), ['judul' => 'Tambah NPD', 'kop_id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $id2)
    {
        $data = $request->all();
        //dd($data);
        // $npd = Npd_pengajuan::find($id);
        $npd = Npd_pengajuan::with(['kopsurat'])->where('kop_id', $id)->where('id', $id2)->firstOrFail();
        if ($npd->update($data)) {
            Alert::success('Berhasil', 'Berhasi mengubah data pengajuan');
        } else {
            Alert::warning('Gagal', 'Gagal mengubah data pengajuan');
        }
        return redirect()->route('npd.input', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $id2)
    {
        $npd = Npd_pengajuan::with(['kopsurat'])->where('kop_id', $id)->where('id', $id2)->firstOrFail();
        
        // $date = DateTime::createFromFormat('Y-m-d', $npd->kopsurat->tgl_dpa);
        // $month = date_format($date, "m");
        // $year = date_format($date, "Y");

        // $bku = BKU::where('year' ,'=',$year)->where('month','=', $month)->where('subBag','=',$npd->kopsurat->bidang)->first();
        // $bku->generate = 'N';
        // $bku->save();

        if ($npd->delete())
            return response()->json(['success' => true]);
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan']);
    }

    public function file($id)
    {
        $data = Kop_surat::findOrfail($id);
        // $user = User::all();
        //dd($data);
        // $npd = Npd_pengajuan::find($id);
        $file = Npd_pengajuan::with(['rekening', 'subrekening'])->where('kop_id', $id)->get();
        //dd($file);
        $total = [];
        $subtotal = [];
        $cols = ['anggaran_murni', 'pergeseran', 'perubahan', 'pencairan_sebelumnya', 'pencairan_saat_ini', 'sisa'];
        foreach ($cols as $col) {
            $total[$col] = 0;
            $subtotal[$col] = [];
            foreach ($file as $pengajuan) {
                $tambah = $pengajuan->$col;
                if (array_search($col, ['pergeseran', 'perubahan']) !== false) {
                    if (!$tambah) $tambah = $pengajuan->anggaran_murni;
                }
                $total[$col] += $tambah;
                $lv4 = $pengajuan->rekening->id;
                $lv3 = $pengajuan->rekening->induk->id;
                $lv2 = $pengajuan->rekening->induk->induk->id;
                $lv1 = $pengajuan->rekening->induk->induk->induk->id;
                $lv0 = $pengajuan->rekening->induk->induk->induk->induk->id;
                if(isset($subtotal[$col][$lv0])) $subtotal[$col][$lv0] += $tambah; else $subtotal[$col][$lv0] = $tambah;
                if(isset($subtotal[$col][$lv1])) $subtotal[$col][$lv1] += $tambah; else $subtotal[$col][$lv1] = $tambah;
                if(isset($subtotal[$col][$lv2])) $subtotal[$col][$lv2] += $tambah; else $subtotal[$col][$lv2] = $tambah;
                if(isset($subtotal[$col][$lv3])) $subtotal[$col][$lv3] += $tambah; else $subtotal[$col][$lv3] = $tambah;
                if(isset($subtotal[$col][$lv4])) $subtotal[$col][$lv4] += $tambah; else $subtotal[$col][$lv4] = $tambah;
            }
        }
        $ttd = DataTtd::where('bidang', $data->bidang)->orderBy('created_at', 'desc')->firstOrFail();

        //dd($rekening);
        $repeatHeader = filter_var(request()->get('repeatHeader'), FILTER_VALIDATE_BOOLEAN);
        $pdf = PDF::loadView('bendahara-pembantu.npd.fileNpd.file', compact('data', 'file', 'total', 'subtotal', 'repeatHeader', 'ttd'));
        $tgl = \Carbon\Carbon::parse($data->tgl_dpa)->translatedFormat('j F Y');
        return $pdf->setPaper('a3', 'landscape')->stream("NPD {$data->nomor_kop} $tgl.pdf");
    }

    public function ajaxPencairanSebelumnya(Request $request){
        $kop = Kop_surat::where('bidang', $request['bidang'])->get()->pluck('id');
        $npdPengajuan = Npd_pengajuan::whereIn('kop_id', $kop)->where('sub_rek_id', $request['subrekening'])->where('rek_id', $request['rekening'])->sum('pencairan_saat_ini');
        $anggaran = Npd_pengajuan::whereIn('kop_id', $kop)->where('sub_rek_id', $request['subrekening'])->where('rek_id', $request['rekening'])->latest()->first();

        $anggaranMurni = $anggaran->anggaran_murni ?? 0;
        
        return response()->json(['success'=> true, 'pencairan_sebelumnya' => $npdPengajuan, 'anggaran_murni' => $anggaranMurni]);
    }
}
