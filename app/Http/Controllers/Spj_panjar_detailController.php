<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Npd_pengajuan;
use App\Models\Spj_panjar_detail;
use App\Models\Spj_panjar;
use App\Models\Spj_belanja;
use App\Models\Rekening;
use App\Models\Kop_surat;
use RealRashid\SweetAlert\Facades\Alert;


class Spj_panjar_detailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spj_detail = Spj_panjar_detail::all();
        $spj_panjar = Spj_panjar::all();
        return view('bendahara-pembantu.spj.tambah_spj_panjar',compact('spj_detail', 'spj_panjar'),['judul' => 'Dashboard']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rekening = Rekening::all();
        $spj_detail = Spj_panjar_detail::all();
        return view('bendahara-pembantu.spj.tambah_spj_panjar', compact('rek', 'detail_spj'), ['judul' => 'Tambah SPJ Detail']);
    }

    public function spj_detail($id){
        $spj_panjar = Spj_panjar::all();
        $rekening = Rekening::all();
        return view('bendahara-pembantu.spj.tambah_spj_panjar', compact('panjar_spj','rekening', 'spj_detail'), ['judul' => 'Tambah SPJ Detail']);
    }

    public function input($id){
        $spj = Spj_panjar::findOrfail($id);
        $spj_panjar = Spj_panjar::all();
        // $data = Spj_panjar::findOrfail($id);
        //dd($data);
        // $npd = Npd_pengajuan::find($id);
        $npd_detail = Npd_pengajuan::join('rekenings', 'rekenings.id', '=', 'npd_pengajuans.sub_rek_id')->get();
        $spj_detail = Spj_panjar_detail::with(['spjpanjar'])->where('panjar_id', $id)->get();
        //dd($npd);
        $rekening = Rekening::get();
        return view('bendahara-pembantu.spj.tambah_spj_detail', compact( 'spj_detail', 'spj_panjar', 'spj', 'npd_detail', 'rekening'), ['judul' => 'Tambah SPJ Detail', 'panjar_id' => $id]);
    }

    public function store(Request $request)
    {   
        $spj_detail = Spj_panjar_detail::create([
            'panjar_id'=>$request->panjar_id,
            'rek_id'=>$request->rek_id,
            'sub_rek_id'=>$request->sub_rek_id,
            'panjar_jml'=>$request->panjar_jml,
            'panjar_spj'=>$request->panjar_spj,
            'uraian'=>$request->uraian,
            'entry_tgl'=>$request->entry_tgl
        ]);

        return redirect("/spj/{$request->panjar_id}/new");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $id2)
    {
        $data = Spj_panjar::findOrfail($id);
        //dd($data);
        // $npd = Npd_pengajuan::find($id);
        $npd_detail = Npd_pengajuan::join('rekenings', 'rekenings.id', '=', 'npd_pengajuans.sub_rek_id')->get();

        $spj_detail = Spj_panjar_detail::with(['spjpanjar'])->where('panjar_id', $id)->get();
        $edit = Spj_panjar_detail::with(['spjpanjar'])->where('panjar_id', $id)->where('id', $id2)->firstOrFail();
        //dd($npd);
        $rekening = Rekening::get();
        return view('bendahara-pembantu.spj.edit_detail', compact('data', 'spj_detail', 'rekening', 'edit', 'npd_detail'), ['judul' => 'Ubah SPJ Detail', 'panjar_id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $id2)
    {
        $data = $request->all();
        //dd($data);
        // $npd = Npd_pengajuan::find($id);
        $spj_detail = Spj_panjar_detail::with(['spjpanjar'])->where('panjar_id', $id)->where('id', $id2)->firstOrFail();
        if ($spj_detail->update($data)) {
            Alert::success('Berhasil', 'Berhasi mengubah data pengajuan');
        } else {
            Alert::warning('Gagal', 'Gagal mengubah data pengajuan');
        }
        return redirect()->route('spjdetail.input', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Spj_panjar_detail::where('id',$id)->delete();

        return back();
    }
}
