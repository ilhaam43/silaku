<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    public function handle($request, Closure $next, $guard = null)
    {
        if ($guard == "admin" && Auth::guard($guard)->check()) {
            return redirect('/admin');
        }
        else if ($guard == "setda" && Auth::guard($guard)->check()) {
            return redirect('/setda');
        }
        else if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }

        return $next($request);
    }
}