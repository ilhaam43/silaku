<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKopSuratsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kop_surats', function (Blueprint $table) {
            $table->id();
            $table->string('nomor_kop');
            $table->string('nomor_dpa');
            $table->date('tgl_dpa');
            $table->string('bidang');
            $table->boolean('disetujui')->default(false);
            $table->string('pengembalian')->default("0");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kop_surats');
    }
}
