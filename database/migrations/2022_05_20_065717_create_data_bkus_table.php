<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataBkusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_bkus', function (Blueprint $table) {
            $table->id();
            $table->date('tanggal');
            $table->string('NoBukti')->nullable();
            $table->foreignId('kop_id')->references('id')->on('kop_surats')->onDelete('cascade');
            $table->foreignId('rek_id')->references('id')->on('rekenings')->onDelete('cascade');
            $table->foreignId('sub_rek_id')->references('id')->on('rekenings')->onDelete('cascade');
            $table->foreignId('npd_id')->references('id')->on('npd_pengajuans')->onDelete('cascade');
            $table->foreignId('spj_id')->references('id')->on('spj_panjars')->onDelete('cascade');
            $table->string('jenis_spj');
            $table->string('penerimaan');
            $table->string('pengeluaran');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_bkus');
    }
}
