<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataFungsionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_fungsionals', function (Blueprint $table) {
            $table->id();
            $table->integer('fungsionalId');
            $table->string('kodeUrusan');
            $table->string('namaUrusan');
            $table->string('kodeBidang');
            $table->string('namaBidang');
            $table->string('kodeUnit');
            $table->string('namaUnit');
            $table->string('kodeSubUnit');
            $table->string('namaSubUnit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_fungsionals');
    }
}
