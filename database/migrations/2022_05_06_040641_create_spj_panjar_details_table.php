<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpjPanjarDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spj_panjar_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('panjar_id')->constrained('spj_panjars')->onDelete('cascade');
            $table->foreignId('rek_id')->references('id')->on('rekenings')->onDelete('cascade');
            $table->foreignId('sub_rek_id')->references('id')->on('rekenings')->onDelete('cascade');
            $table->string('panjar_jml');
            $table->string('panjar_spj');
            $table->string('uraian');
            $table->date('entry_tgl');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spj_panjar_details');
    }
}
