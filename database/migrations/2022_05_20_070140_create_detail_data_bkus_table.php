<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailDataBkusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_data_bkus', function (Blueprint $table) {
            $table->id();
            $table->foreignId('dataBKU_id')->references('id')->on('data_bkus')->onDelete('cascade');
            $table->date('tanggal');
            $table->string('NoBukti')->nullable();
            $table->string('uraian')->nullable();
            $table->string('penerimaan')->nullable();
            $table->string('pengeluaran')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_data_bkus');
    }
}
