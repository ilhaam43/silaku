<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNpdPengajuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('npd_pengajuans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('kop_id')->references('id')->on('kop_surats')->onDelete('cascade');
            $table->foreignId('rek_id')->references('id')->on('rekenings')->onDelete('cascade');
            $table->foreignId('sub_rek_id')->references('id')->on('rekenings')->onDelete('cascade');
            $table->string('anggaran_murni')->nullable();
            $table->string('pergeseran')->nullable();
            $table->string('perubahan')->nullable();
            $table->string('pencairan_sebelumnya')->nullable();
            $table->string('pencairan_saat_ini')->nullable();
            $table->string('sisa')->nullable();
            $table->string('pptk')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('npd_pengajuans');
    }
}
