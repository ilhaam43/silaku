<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpjRincianBelanjasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spj_rincian_belanjas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('detail_id')->constrained('spj_panjar_details')->onDelete('cascade');
            $table->string('penjelasan_barang_belanja');
            $table->string('volume');
            $table->string('harga_satuan');
            $table->string('total');
            $table->string('tgl_belanja');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spj_rincian_belanjas');
    }
}
