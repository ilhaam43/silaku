<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpjPanjarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spj_panjars', function (Blueprint $table) {
            $table->id();
            $table->foreignId('npd_id')->constrained('npd_pengajuans')->onDelete('cascade');
            $table->foreignId('rek_id')->references('id')->on('rekenings')->onDelete('cascade');
            $table->foreignId('sub_rek_id')->references('id')->on('rekenings')->onDelete('cascade');
            $table->string('jenis_spj');
            $table->string('jml_panjar');
            $table->string('spj_panjar');
            $table->date('tgl_entry');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spj_panjars');
    }
}
