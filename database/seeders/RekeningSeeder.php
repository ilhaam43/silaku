<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class RekeningSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $r[1] = \App\Models\Rekening::create([
            'kode_rekening' => '4',
            'level' => 0,
            'nama_kegiatan' => 'Unsur Pendukung Urusan Pemerintahan '
        ]);
        $r[2] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01',
            'level' => 1,
            'nama_kegiatan' => 'Sekretariat Daerah ',
            'parent_id' => $r[1]->id
        ]);
        $r[3] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01',
            'level' => 2,
            'nama_kegiatan' => 'PROGRAM PENUNJANG URUSAN PEMERINTAHAN DAERAH KABUPATEN/ KOTA ',
            'parent_id' => $r[2]->id
        ]);
        $r[4] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.01',
            'level' => 3,
            'nama_kegiatan' => 'Perencanaan, Penganggaran, dan Evaluasi Kinerja Perangkat Daerah ',
            'parent_id' => $r[3]->id
        ]);
        $r[5] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.02',
            'level' => 3,
            'nama_kegiatan' => 'Administrasi Keuangan Perangkat Daerah ',
            'parent_id' => $r[3]->id
        ]);
        $r[6] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.05',
            'level' => 3,
            'nama_kegiatan' => 'Administrasi Kepegawaian Perangkat Daerah ',
            'parent_id' => $r[3]->id
        ]);
        $r[7] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.06',
            'level' => 3,
            'nama_kegiatan' => 'Administrasi Umum Perangkat Daerah ',
            'parent_id' => $r[3]->id
        ]);
        $r[8] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.07',
            'level' => 3,
            'nama_kegiatan' => 'Pengadaan Barang Milik Daerah Penunjang Urusan Pemerintah Daerah ',
            'parent_id' => $r[3]->id
        ]);
        $r[9] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.08',
            'level' => 3,
            'nama_kegiatan' => 'Penyediaan Jasa Penunjang Urusan Pemerintahan Daerah ',
            'parent_id' => $r[3]->id
        ]);
        $r[10] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.09',
            'level' => 3,
            'nama_kegiatan' => 'Pemeliharaan Barang Milik Daerah Penunjang Urusan Pemerintahan Daerah ',
            'parent_id' => $r[3]->id
        ]);
        $r[11] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.11',
            'level' => 3,
            'nama_kegiatan' => 'Administrasi Keuangan dan Operasional Kepala Daerah dan Wakil Kepala Daerah ',
            'parent_id' => $r[3]->id
        ]);
        $r[12] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.12',
            'level' => 3,
            'nama_kegiatan' => 'Fasilitasi Kerumahtanggaan Sekretariat Daerah ',
            'parent_id' => $r[3]->id
        ]);
        $r[13] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.01.01',
            'level' => 4,
            'nama_kegiatan' => 'Penyusunan Dokumen Perencanaan Perangkat Daerah ',
            'parent_id' => $r[4]->id
        ]);
        $r[14] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.01.02',
            'level' => 4,
            'nama_kegiatan' => 'Koordinasi dan Penyusunan Dokumen RKA-SKPD ',
            'parent_id' => $r[4]->id
        ]);
        $r[15] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.01.03',
            'level' => 4,
            'nama_kegiatan' => 'Koordinasi dan Penyusunan Dokumen Perubahan RKA-SKPD ',
            'parent_id' => $r[4]->id
        ]);
        $r[16] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.01.06',
            'level' => 4,
            'nama_kegiatan' => 'Koordinasi dan Penyusunan Laporan Capaian Kinerja dan Ikhtisar Realisasi Kinerja SKPD ',
            'parent_id' => $r[4]->id
        ]);
        $r[17] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.01.07',
            'level' => 4,
            'nama_kegiatan' => 'Evaluasi Kinerja Perangkat Daerah ',
            'parent_id' => $r[4]->id
        ]);
        $r[18] = \App\Models\Rekening::create([
            'kode_rekening' => '4.01.01.2.02.02',
            'level' => 4,
            'nama_kegiatan' => 'Penyediaan Administrasi Pelaksanaan Tugas ASN ',
            'parent_id' => $r[5]->id
        ]);
        $r[19] = \App\Models\Rekening::create([
            'kode_rekening' => '5.1.02.01.01.0024',
            'level' => 5,
            'nama_kegiatan' => 'Belanja Alat/Bahan untuk Kegiatan Kantor- Alat Tulis Kantor ',
        ]);
        $r[20] = \App\Models\Rekening::create([
            'kode_rekening' => '5.1.02.01.01.0026 ',
            'level' => 5,
            'nama_kegiatan' => 'Belanja Alat/Bahan untuk Kegiatan Kantor- Bahan Cetak ',
        ]);
        $r[21] = \App\Models\Rekening::create([
            'kode_rekening' => '5.1.02.01.01.0027 ',
            'level' => 5,
            'nama_kegiatan' => 'Belanja Alat/Bahan untuk Kegiatan Kantor- Benda Pos ',
        ]);
        $r[22] = \App\Models\Rekening::create([
            'kode_rekening' => '5.1.02.01.01.0052 ',
            'level' => 5,
            'nama_kegiatan' => 'Belanja Makanan dan Minuman Rapat ',
        ]);
        $r[23] = \App\Models\Rekening::create([
            'kode_rekening' => '5.1.02.02.01.0026 ',
            'level' => 5,
            'nama_kegiatan' => 'Belanja Jasa Tenaga Administrasi ',
        ]);
        $r[24] = \App\Models\Rekening::create([
            'kode_rekening' => '5.1.02.02.02.0006 ',
            'level' => 5,
            'nama_kegiatan' => 'Belanja Iuran Jaminan Kecelakaan Kerja bagi Non ASN ',
        ]);
        $r[25] = \App\Models\Rekening::create([
            'kode_rekening' => '5.1.02.02.02.0007 ',
            'level' => 5,
            'nama_kegiatan' => 'Belanja Iuran Jaminan Kematian bagi Non ASN ',
        ]);
        foreach ($r as $rr) {
            $rr->save();
        }
    }
}