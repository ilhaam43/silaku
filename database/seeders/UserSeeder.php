<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'Admin',  
            'email' => 'admin@silaku.test', 
            'password' => Hash::make('password') 
        ])->save();
        \App\Models\User::create([
            'name' => 'Setda',  
            'role' => 'Bentama', 
            'email' => 'setda@silaku.test', 
            'password' => Hash::make('password') 
        ])->save();
        \App\Models\User::create([
            'name' => 'Benpem',  
            'role' => 'Benpem', 
            'level' => 'Umum', 
            'email' => 'benpem@silaku.test', 
            'password' => Hash::make('password') 
        ])->save();
    }
}
