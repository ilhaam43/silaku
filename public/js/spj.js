const deleteBtn = document.querySelectorAll(".user__delete-btn")

for (const btn of deleteBtn) {
    btn.addEventListener('click', e => {
        const id = e.target.dataset.id
        Swal.fire({
            title: "Yakin menghapus data?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4e73df",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yakin",
            cancelButtonText: "Tidak",
            preConfirm: () => {
                return fetch(e.target.dataset.url_hapus, {
                    method: "DELETE",
                    headers: {
                        "X-Requested-With": "XMLHttpRequest",
                        "X-CSRF-Token": document.querySelector(
                            "[name='csrf-token']"
                        ).content,
                    },
                    credentials: "include",
                })
                    .then((response) => {
                        if (!response.ok) {
                            throw new Error(`${response.status}: ${response.statusText}`);
                        }
                        return response.json();
                    })
                    .catch((error) => {
                        Swal.showValidationMessage(`Request failed: ${error}`);
                    });
            },
            allowOutsideClick: () => !Swal.isLoading(),
        }).then((result) => {
            if (result.isConfirmed) {
                if (result.value.success) {
                    Swal.fire({
                        title: "Berhasil menghapus",
                        icon: "success",
                        confirmButtonColor: "#4e73df",
                        confirmButtonText: "OK",
                    }).then(() => {
                        window.location.reload();
                    });
                } else {
                    Swal.fire({
                        title: "Gagal menghapus",
                        text: result.value.message,
                        icon: "error",
                        confirmButtonColor: "#4e73df",
                        confirmButtonText: "OK",
                    }).then(() => {
                        window.location.reload();
                    });
                }
            }
        });
    })
}