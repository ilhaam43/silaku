const addBtn = document.querySelector(".user__add-btn");
const deleteBtn = document.querySelectorAll(".user__delete-btn");
const editBtn = document.querySelectorAll(".user__edit-btn");

let rekening = []
fetch(window.location.href + '/list')
.then(res => res.json())
.then(res => {
    res.data.forEach(rek => {
        rekening.push(rek);
    });
})

addBtn.addEventListener("click", (e) => {
    Swal.fire({
        title: "Tambah Kode Rekening",
        html: `
    <div class="form-group text-left">
      <label for="tambah__kd-rekening">Kode Rekening</label>
      <input
        type="text"
        id="tambah__kd-rekening"
        class="form-control"
        value=""
      />
    </div>
    <div class="form-group text-left">
      <label for="tambah__jenis-rekening">Jenis Rekening</label>
      <select id="tambah__jenis-rekening" class="form-control">
        <option value="" disabled selected>Pilih Jenis Rekening</option>
        <option value="0">Default 0</option>
        <option value="1">Default 1</option>
        <option value="2">Program</option>
        <option value="3">Kegiatan</option>
        <option value="4">Sub Kegiatan</option>
        <option value="5">Belanja</option>
      </select>
    </div>
    <div class="form-group text-left">
      <label for="tambah__rekening-induk">Rekening Induk</label>
      <select id="tambah__rekening-induk" class="form-control">
        <option value="" disabled selected>Pilih Rekening Induk</option>
      </select>
    </div>
    <div class="form-group text-left">
      <label for="tambah__uraian-rekening">Uraian Kegiatan</label>
      <textarea class="form-control" id="tambah__uraian-rekening" rows="3"></textarea>
    </div>
    `,
        showCancelButton: true,
        confirmButtonColor: "#4e73df",
        cancelButtonColor: "#d33",
        confirmButtonText: "Tambah",
        cancelButtonText: "Batal",
        focusConfirm: false,
        preConfirm: () => {
            const kode = Swal.getPopup().querySelector(
                "#tambah__kd-rekening"
            ).value;
            const jenis = Swal.getPopup().querySelector(
                "#tambah__jenis-rekening"
            ).value;
            const induk = Swal.getPopup().querySelector(
                "#tambah__rekening-induk"
            ).value;
            const uraian = Swal.getPopup().querySelector(
                "#tambah__uraian-rekening"
            ).value;
            if (!kode || !jenis || !uraian) {
                return Swal.showValidationMessage(`Harap lengkapi data`);
            }
            if (jenis != '0' && jenis != '5' && !induk) {
                return Swal.showValidationMessage(`Harap pilih rekening induk`);
            }
            console.log(JSON.stringify({
                kode_rekening: kode,
                parent_id: induk ?? null,
                level: jenis,
                nama_kegiatan: uraian,
            }));
            return fetch(`${window.location.href}`, {
                method: "POST",
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-Token": document.querySelector(
                        "[name='csrf-token']"
                    ).content,
                },
                body: JSON.stringify({
                    kode_rekening: kode,
                    parent_id: induk ?? null,
                    level: jenis,
                    nama_kegiatan: uraian,
                }),
                credentials: "include",
            })
                .then((response) => {
                    if (!response.ok) {
                        throw new Error(response.statusText);
                    }
                    return response.json();
                })
                .catch((error) => {
                    Swal.showValidationMessage(`Request failed: ${error}`);
                });
        },
    }).then((result) => {
        if (result.isConfirmed) {
            if (result.value.success) {
                Swal.fire({
                    title: "Berhasil menambahkan",
                    icon: "success",
                    confirmButtonColor: "#4e73df",
                    confirmButtonText: "OK",
                }).then(() => {
                    window.location.reload();
                });
            } else {
                Swal.fire({
                    title: "Gagal menambahkan",
                    text: result.value.message,
                    icon: "error",
                    confirmButtonColor: "#4e73df",
                    confirmButtonText: "OK",
                }).then(() => {
                    window.location.reload();
                });
            }
        }
    });
    document.querySelector("#tambah__jenis-rekening").addEventListener('change', e => {
        let level = e.target.value;
        const selectParent = document.querySelector("#tambah__rekening-induk");
        if (level == '0' || level == '5') {
            selectParent.disabled = true;
        } else {
            selectParent.disabled = false;
            while (selectParent.childElementCount > 1) {
                selectParent.removeChild(selectParent.lastElementChild)
            }
            level = (parseInt(level) - 1);
            rekening.filter(rek => rek.level == level).forEach(rek => {
                const opt = document.createElement('option');
                opt.value = rek.id;
                opt.innerHTML = rek.nama_kegiatan;
                selectParent.appendChild(opt)
            })
        }
    })
});

const destroy = (id) => {
    Swal.fire({
        title: "Yakin menghapus data?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4e73df",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yakin",
        cancelButtonText: "Tidak",
        preConfirm: () => {
            console.log(`${window.location.href}/${id}`);
            return fetch(`${window.location.href}/${id}`, {
                method: "DELETE",
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-Token": document.querySelector(
                        "[name='csrf-token']"
                    ).content,
                },
                credentials: "include",
            })
                .then((response) => {
                    if (!response.ok) {
                        throw new Error(`${response.status}: ${response.statusText}`);
                    }
                    return response.json();
                })
                .catch((error) => {
                    Swal.showValidationMessage(`Request failed: ${error}`);
                });
        },
        allowOutsideClick: () => !Swal.isLoading(),
    }).then((result) => {
        if (result.isConfirmed) {
            if (result.value.success) {
                Swal.fire({
                    title: "Berhasil menghapus",
                    icon: "success",
                    confirmButtonColor: "#4e73df",
                    confirmButtonText: "OK",
                }).then(() => {
                    window.location.reload();
                });
            } else {
                Swal.fire({
                    title: "Gagal menghapus",
                    text: result.value.message,
                    icon: "error",
                    confirmButtonColor: "#4e73df",
                    confirmButtonText: "OK",
                }).then(() => {
                    window.location.reload();
                });
            }
        }
    });
};

const update = (id) => {
    let parentId = 0;
    fetch(window.location.href + "/" + id)
        .then(response => {
            return response.json();
        })
        .then(data => {
            parentId = data.data.parent_id;
            let html = `
            <div class="form-group text-left">
              <label for="edit__kd-rekening">Kode Rekening</label>
              <input
                type="text"
                id="edit__kd-rekening"
                class="form-control"
                value="${data.data.kode_rekening}"
              />
            </div>
            <div class="form-group text-left">
                <label for="edit__jenis-rekening">Jenis Rekening</label>
                <select id="edit__jenis-rekening" class="form-control">
                    <option value="" disabled>Pilih Jenis Rekening</option>
                    <option value="0" ${data.data.level == 0 ? 'selected' : ''}>Default 0</option>
                    <option value="1" ${data.data.level == 1 ? 'selected' : ''}>Default 1</option>
                    <option value="2" ${data.data.level == 2 ? 'selected' : ''}>Program</option>
                    <option value="3" ${data.data.level == 3 ? 'selected' : ''}>Kegiatan</option>
                    <option value="4" ${data.data.level == 4 ? 'selected' : ''}>Sub Kegiatan</option>
                    <option value="5" ${data.data.level == 5 ? 'selected' : ''}>Belanja</option>
                </select>
            </div>
            <div class="form-group text-left">
                <label for="edit__rekening-induk">Rekening Induk</label>
                <select id="edit__rekening-induk" class="form-control" ${data.data.level == 0 ? 'disabled' : ''}>
                    <option value="" disabled selected>Pilih Rekening Induk</option>`;
                if (data.data.level != 0) {
                    rekening.filter(rek => rek.level == data.data.level-1).forEach(rek => {
                        html += `<option value="${rek.id}" ${data.data.parent_id == rek.id ? 'selected' : ''}>${rek.nama_kegiatan}</option>`;
                    })
                }
                html += `</select>
                </div>
                <div class="form-group text-left">
                  <label for="edit__uraian-rekening">Uraian Kegiatan</label>
                  <textarea class="form-control" id="edit__uraian-rekening" rows="3">${data.data.nama_kegiatan}</textarea>
                </div>
                `;
            Swal.fire({
                title: "Ubah Kode Rekening",
                html: html,
                showCancelButton: true,
                confirmButtonColor: "#4e73df",
                cancelButtonColor: "#d33",
                cancelButtonText: "Batal",
                confirmButtonText: "Ubah",
                focusConfirm: false,
                preConfirm: () => {
                    const kode = Swal.getPopup().querySelector(
                        "#edit__kd-rekening"
                    ).value;
                    const jenis = Swal.getPopup().querySelector(
                        "#edit__jenis-rekening"
                    ).value;
                    const induk = Swal.getPopup().querySelector(
                        "#edit__rekening-induk"
                    ).value;
                    const uraian = Swal.getPopup().querySelector(
                        "#edit__uraian-rekening"
                    ).value;
                    if (!kode || !jenis || !uraian) {
                        return Swal.showValidationMessage(`Harap lengkapi data`);
                    }
                    if (jenis != '0' && jenis != '5' && !induk) {
                        return Swal.showValidationMessage(`Harap pilih rekening induk`);
                    }
                    console.log(JSON.stringify({
                        kode_rekening: kode,
                        parent_id: induk ?? null,
                        level: jenis,
                        nama_kegiatan: uraian,
                    }));
                    console.log(`${window.location.href}/${id}`);
                    return fetch(`${window.location.href}/${id}`, {
                        method: "PUT",
                        headers: {
                            "X-Requested-With": "XMLHttpRequest",
                            "X-CSRF-Token": document.querySelector(
                                "[name='csrf-token']"
                            ).content,
                        },
                        body: JSON.stringify({
                            kode_rekening: kode,
                            parent_id: induk ?? null,
                            level: jenis,
                            nama_kegiatan: uraian,
                        }),
                        credentials: "include",
                    }).then((response) => {
                        if (!response.ok) {
                            throw new Error(`${response.status}: ${response.statusText}`);
                        }
                        return response.json();
                    })
                    .catch((error) => {
                        Swal.showValidationMessage(`Request failed: ${error}`);
                    });
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    if (result.value.success) {
                        Swal.fire({
                            title: "Data berhasil diubah",
                            icon: "success",
                            confirmButtonColor: "#4e73df",
                            confirmButtonText: "OK",
                        }).then(() => {
                            window.location.reload();
                        });
                    } else {
                        Swal.fire({
                            title: "Gagal mengubah data",
                            text: result.value.message,
                            icon: "error",
                            confirmButtonColor: "#4e73df",
                            confirmButtonText: "OK",
                        }).then(() => {
                            window.location.reload();
                        });
                    }
                }
            });
            document.querySelector("#edit__jenis-rekening").addEventListener('change', e => {
                let level = e.target.value;
                const selectParent = document.querySelector("#edit__rekening-induk");
                if (level == '0' || level == '5') {
                    selectParent.disabled = true;
                } else {
                    selectParent.disabled = false;
                    while (selectParent.childElementCount > 1) {
                        selectParent.removeChild(selectParent.lastElementChild)
                    }
                    level = (parseInt(level) - 1);
                    rekening.filter(rek => rek.level == level).forEach(rek => {
                        const opt = document.createElement('option');
                        opt.value = rek.id;
                        opt.innerHTML = rek.nama_kegiatan;
                        opt.selected = rek.parent_id == parentId;
                        selectParent.appendChild(opt)
                    })
                }
            })
        });
};

// Call the dataTables jQuery plugin
$(document).ready(function () {
    $("#dataTable").DataTable({
        "order": []
    });
});
