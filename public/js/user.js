const userAddBtn = document.querySelector(".user__add-btn");
const userDeleteBtn = document.querySelectorAll(".user__delete-btn");
const userEditBtn = document.querySelectorAll(".user__edit-btn");

userAddBtn.addEventListener("click", (e) => {
  Swal.fire({
    title: "Tambah User",
    html: `
      <div class="form-group">
        <input
          type="text"
          id="tambah__nama"
          class="form-control"
          placeholder="Nama"
          value=""
        />
      </div>
      <div class="form-group">
        <input
          type="email"
          id="tambah__email"
          class="form-control"
          placeholder="Email"
          value=""
        />
      </div>
      <div class="form-group">
        <select id="tambah__jabatan" class="form-control">
          <option value="" disabled selected>Pilih Jabatan</option>
          <option value="Bentama">Bendahara Setda</option>
          <option value="Benpem">Bendahara Pembantu</option>
        </select>
      </div>
      <div class="form-group">
        <select id="tambah__level" class="form-control" style="display: none">
          <option value="" disabled selected>Pilih Bidang</option>
          <option value="Umum">Bidang Umum</option>
          <option value="Organisasi">Bidang Organisasi</option>
          <option value="Prokopim">Bidang Prokopim</option>
        </select>
      </div>
      <div class="form-group">
        <input
          type="password"
          id="tambah__password"
          class="form-control"
          placeholder="Password"
          value=""
        />
      </div>
      <div class="form-group">
        <input
          type="password"
          id="tambah__password-confirm"
          class="form-control"
          placeholder="Konfirmasi password"
          value=""
        />
      </div>
      `,
    showCancelButton: true,
    confirmButtonColor: "#4e73df",
    cancelButtonColor: "#d33",
    confirmButtonText: "Tambah",
    cancelButtonText: "Batal",
    focusConfirm: false,
    preConfirm: () => {
      const nama = Swal.getPopup().querySelector("#tambah__nama").value;
      const email = Swal.getPopup().querySelector("#tambah__email").value;
      const jabatan = Swal.getPopup().querySelector("#tambah__jabatan").value;
      const level = Swal.getPopup().querySelector("#tambah__level").value;
      const password = Swal.getPopup().querySelector("#tambah__password").value;
      const passwordConfirm = Swal.getPopup().querySelector(
        "#tambah__password-confirm"
      ).value;
      if (!nama || !email || !jabatan || !password || !passwordConfirm) {
        return Swal.showValidationMessage(`Harap lengkapi data`);
      }
      if (jabatan == 'Benpem' && !level) {
        return Swal.showValidationMessage(`Harap lengkapi data`);
      }
      if (password != passwordConfirm) {
        return Swal.showValidationMessage('Konfirmasi password tidak cocok');
      }
      return fetch(`${window.location.href}`, {
        method: "POST",
        headers: {
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-Token": document.querySelector(
                "[name='csrf-token']"
            ).content,
        },
        body: JSON.stringify({
            name: nama,
            email: email,
            role: jabatan,
            level: level ?? null,
            password: password,
        }),
        credentials: "include",
    }).then(response => {
      if (!response.ok) {
        throw new Error(response.statusText)
      }
      return response.json()
    })
    .catch(error => {
      Swal.showValidationMessage(
        `Request failed: ${error}`
      )
    })
    },
  }).then((result) => {
    if (result.isConfirmed) {
      if (result.value.success) {
        Swal.fire({
          title: "Berhasil menambahkan",
          icon: "success",
          confirmButtonColor: "#4e73df",
          confirmButtonText: "OK",
        }).then(() => {window.location.reload()});
      } else {
        Swal.fire({
          title: "Gagal menambahkan",
          text: result.value.message,
          icon: "error",
          confirmButtonColor: "#4e73df",
          confirmButtonText: "OK",
        }).then(() => {window.location.reload()});
      }
    }
  });
  document.querySelector("#tambah__jabatan").addEventListener('change', e => {
    const select = document.querySelector("#tambah__level")
    select.style.display = e.target.value == 'Benpem' ? 'initial' : 'none'
  })
});

for (const btn of userDeleteBtn) {
  btn.addEventListener("click", (e) => {
    const id = e.target.dataset.id;
    console.log(e.target.dataset);
    Swal.fire({
      title: "Yakin menghapus data?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#4e73df",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yakin",
      cancelButtonText: "Tidak",
      preConfirm: () => {
        return fetch(`${window.location.href}/${id}`, {
          method: 'DELETE',
          headers: {
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-Token": document.querySelector("[name='csrf-token']").content
          },
          credentials: "include"
        })
          .then(response => {
            if (!response.ok) {
              throw new Error(response.statusText)
            }
            return response.json()
          })
          .catch(error => {
            Swal.showValidationMessage(
              `Request failed: ${error}`
            )
          })
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if (result.isConfirmed) {
        if (result.value.success){
        Swal.fire({
          title: "Berhasil menghapus",
          icon: "success",
          confirmButtonColor: "#4e73df",
          confirmButtonText: "OK",
        }).then(() => {window.location.reload()});} else {
          Swal.fire({
            title: "Gagal menghapus",
            text: result.value.message,
            icon: "error",
            confirmButtonColor: "#4e73df",
            confirmButtonText: "OK",
          }).then(() => {window.location.reload()});
        }
      }
    })
  });
}

for (const btn of userEditBtn) {
  btn.addEventListener("click", (e) => {
    const id = e.target.dataset.id;
    console.log(e.target.dataset)
    fetch(document.location.href + '/' + id).then(
      response => {
        return response.json()
      }
    ).then(data => {
      Swal.fire({
        title: "Ubah User",
        html: `
        <div class="form-group">
          <input
            type="text"
            id="edit__nama"
            class="form-control"
            placeholder="Nama"
            value="${data.data.name}"
          />
        </div>
        <div class="form-group">
          <input
            type="text"
            id="edit__email"
            class="form-control"
            placeholder="Email"
            value="${data.data.email}"
          />
        </div>
        <div class="form-group">
          <select id="edit__jabatan" class="form-control">
            <option value="" disabled>Pilih Jabatan</option>
            <option value="Bentama" ${data.data.role == 'Bentama' ? 'selected' : ''}>Bendahara Setda</option>
            <option value="Benpem" ${data.data.role == 'Benpem' ? 'selected' : ''}>Bendahara Pembantu</option>
          </select>
        </div>
        <div class="form-group">
          <select id="edit__level" class="form-control" style="display: ${data.data.role == 'Benpem' ? 'initial' : 'none'}">
          <option value="" disabled ${data.data.role == 'Benpem' ? '' : 'selected'}>Pilih Bidang</option>
          <option value="Umum" ${(data.data.role == 'Benpem' && data.data.level == 'Umum') ? 'selected' : ''}>Bidang Umum</option>
            <option value="Organisasi" ${(data.data.role == 'Benpem' && data.data.level == 'Organisasi') ? 'selected' : ''}>Bidang Organisasi</option>
            <option value="Prokopim" ${(data.data.role == 'Benpem' && data.data.level == 'Prokopim') ? 'selected' : ''}>Bidang Prokopim</option>
          </select>
        </div>
        <div class="form-group">
          <input
            type="password"
            id="edit__password"
            class="form-control"
            placeholder="Password"
            value=""
          />
        </div>
        <div class="form-group">
          <input
            type="password"
            id="edit__password-confirm"
            class="form-control"
            placeholder="Konfirmasi password"
            value=""
          />
        </div>
        `,
        showCancelButton: true,
        confirmButtonColor: "#4e73df",
        cancelButtonColor: "#d33",
        cancelButtonText: "Batal",
        confirmButtonText: "Ubah",
        focusConfirm: false,
        preConfirm: () => {
          const nama = Swal.getPopup().querySelector("#edit__nama").value;
          const nip = Swal.getPopup().querySelector("#edit__nip").value;
          const email = Swal.getPopup().querySelector("#edit__email").value;
          const jabatan = Swal.getPopup().querySelector("#edit__jabatan").value;
          const level = Swal.getPopup().querySelector("#edit__level").value;
          const password = Swal.getPopup().querySelector("#edit__password").value;
          const passwordConfirm = Swal.getPopup().querySelector(
            "#edit__password-confirm"
          ).value;
          if (!nama || !email || !jabatan) {
            return Swal.showValidationMessage(`Harap lengkapi data`);
          }
          if (jabatan == 'Benpem' && !level) {
            return Swal.showValidationMessage(`Harap lengkapi data`);
          }
          if (password) {
            if (!passwordConfirm) return Swal.showValidationMessage('Harap isi konfirmasi password');
            if (password != passwordConfirm) return Swal.showValidationMessage('Konfirmasi password tidak sesuai');
            return fetch(`${window.location.href}/${id}`, {
              method: "PUT",
              headers: {
                  "X-Requested-With": "XMLHttpRequest",
                  "X-CSRF-Token": document.querySelector(
                      "[name='csrf-token']"
                  ).content,
              },
              body: JSON.stringify({
                  name: nama,
                  email: email,
                  role: jabatan,
                  level: level ?? null,
                  password: password,
              }),
              credentials: "include",
          }).then(response => {
            if (!response.ok) {
              throw new Error(response.statusText)
            }
            return response.json()
          })
          .catch(error => {
            Swal.showValidationMessage(
              `Request failed: ${error}`
            )
          })
          }
          return fetch(`${window.location.href}/${id}`, {
            method: "PUT",
            headers: {
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRF-Token": document.querySelector(
                    "[name='csrf-token']"
                ).content,
            },
            body: JSON.stringify({
              name: nama,
              email: email,
              role: jabatan,
              level: level ?? null,
            }),
            credentials: "include",
        }).then(response => {
          if (!response.ok) {
            throw new Error(response.statusText)
          }
          return response.json()
        })
        .catch(error => {
          Swal.showValidationMessage(
            `Request failed: ${error}`
          )
        })
        },
      }).then((result) => {
        if (result.isConfirmed) {
          if (result.value.success){
          Swal.fire({
            title: "Data berhasil diubah",
            icon: "success",
            confirmButtonColor: "#4e73df",
            confirmButtonText: "OK",
          }).then(() => {window.location.reload()});} else {
            Swal.fire({
              title: "Gagal mengubah data",
              text: result.value.message,
              icon: "error",
              confirmButtonColor: "#4e73df",
              confirmButtonText: "OK",
            }).then(() => {window.location.reload()});
          }
        }
      });
      document.querySelector("#edit__jabatan").addEventListener('change', e => {
        const select = document.querySelector("#edit__level")
        select.style.display = e.target.value == 'Benpem' ? 'initial' : 'none'
      })
    })
    
  });
}

// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTable').DataTable();
});
