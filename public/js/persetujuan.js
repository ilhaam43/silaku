const editBtn = document.querySelectorAll(".bendahara-npd__edit-btn");

for (const btn of editBtn) {
  btn.addEventListener("click", (e) => {
    const btn = e.target
    const row = btn.parentElement.parentElement
    const td = row.getElementsByTagName('td')
    Swal.fire({
      title: "Verifikasi",
      html: `
      <div class="form-group text-left">
        <label for="edit__no-npd">No NPD</label>
        <input
          type="text"
          id="edit__no-npd"
          class="form-control"
          placeholder="No NPD"
          value="${td[1].innerHTML}"
          disabled
        />
      </div>
      <div class="form-group text-left">
        <label for="edit__status">Status</label>
        <select id="edit__status" class="form-control">
          <option value=1 ${e.target.dataset.disetujui == 1 ? 'selected' : ''}>Disetujui</option>
          <option value=0 ${e.target.dataset.disetujui == 0 ? 'selected' : ''}>Belum disetujui</option>
        </select>
      </div>
      `,
      showCancelButton: true,
      confirmButtonColor: "#4e73df",
      cancelButtonColor: "#d33",
      cancelButtonText: "Batal",
      confirmButtonText: "Simpan",
      focusConfirm: false,
      preConfirm: () => {
        const status = Swal.getPopup().querySelector("#edit__status").value;
        return fetch(`${window.location.href}/${e.target.dataset.id}`, {
          method: "PUT",
          headers: {
              "X-Requested-With": "XMLHttpRequest",
              "X-CSRF-Token": document.querySelector(
                  "[name='csrf-token']"
              ).content,
          },
          body: JSON.stringify({
            disetujui: status
          }),
          credentials: "include",
      }).then((response) => {
          if (!response.ok) {
              throw new Error(`${response.status}: ${response.statusText}`);
          }
          return response.json();
      })
      .catch((error) => {
          return Swal.showValidationMessage(`Request failed: ${error}`);
      });
      },
    }).then((result) => {
      if (result.isConfirmed) {
        if (result.value.success) {
          Swal.fire({
              title: "Verifikasi berhasil",
              icon: "success",
              confirmButtonColor: "#4e73df",
              confirmButtonText: "OK",
          }).then(() => {
              window.location.reload();
          });
      } else {
          Swal.fire({
              title: "Gagal verifikasi",
              text: result.value.message,
              icon: "error",
              confirmButtonColor: "#4e73df",
              confirmButtonText: "OK",
          }).then(() => {
              window.location.reload();
          });
      }
      }
    });
  });
}
