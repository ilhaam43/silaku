@extends('layouts.setda')

@section('setda')
<style>
    #content {
  padding-top: 2rem;
}

.sidebar .sidebar-brand-bendahara {
  height: 6rem;
}

.bendahara-npd__badge {
  line-height: 28px;
}

</style>

<div class="container-fluid">
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-center mb-4">
    <h1 class="h3 mb-0 text-gray-800">Verifikasi & Pengecekan</h1>
  </div>

  <!-- Tables-->
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <div class="row">
        <div class="col-sm-3 col-md-2">
          <input
            type="date"
            name="date-from"
            id="date-from"
            class="form-control"
          />
        </div>
        <span style="padding-top: 0.4rem">-</span>
        <div class="col-sm-3 col-md-2">
          <input
            type="date"
            name="date-from"
            id="date-from"
            class="form-control"
          />
        </div>
        <button
          type="button"
          class="btn btn-primary bendahara-npd__terapkan-btn"
        >
          Terapkan
        </button>
      </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table
          class="table table-bordered table-striped text-center"
          id="dataTable"
          width="100%"
          cellspacing="0"
        >
          <thead>
            <tr>
              <th>No</th>
              <th>Uraian Kegiatan</th>
              <th>Tanggal</th>
              <th> Bidang </th>
              <th>Data Pengajuan</th>
              <th>Aksi</th>
              <th>Status</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>No</th>
              <th>Uraian Kegiatan</th>
              <th>Tanggal</th>
              <th> Bidang </th>
              <th>Data Pengajuan</th>
              <th>Aksi</th>
              <th>Status</th>
            </tr>
          </tfoot>
          <tbody>
            @foreach($kop as $k)
            <tr>
              <td>{{$loop->iteration}}</td>
              <td>{{$k->nomor_kop}}</td>
              <td>{{ \Carbon\Carbon::parse($k->tgl_dpa)->translatedFormat('d/m/Y') }}</td>
              <td>{{$k->bidang}}</td>
              <td>
                <a
                  href="{{route('fileNpd', ['id' => $k->id])}}" target="_blank"
                  class="btn btn-outline-secondary bendahara-npd__delete-btn"
                >
                  <i class="fas fa-fw fa-download"></i>
                </a>
              </td>
              <td>
                <button
                  type="button"
                  class="btn btn-outline-primary bendahara-npd__edit-btn"
                  data-nomor_npd="{{$k->nomor_kop}}"
                  data-disetujui="{{$k->disetujui}}"
                  data-id="{{$k->id}}"
                >
                  <i class="fas fa-fw fa-edit"></i>
                </button>
              </td>
              <td>
                @if($k->disetujui)
                <div class="badge badge-success bendahara-npd__badge">
                  Disetujui
                </div>
                @else
                <div class="badge badge-secondary bendahara-npd__badge">
                  Belum disetujui
                </div>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script src="{{ asset('js/persetujuan.js') }}"></script>
@endsection