<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Berkas Nota Pengajuan Dana</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Custom fonts for this template-->
    {{-- <link href={{ asset('/vendor/fontawesome-free/css/all.min.css') }} rel="stylesheet" type="text/css">
    <link
        href='https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'
        rel="stylesheet"> --}}

    <!-- Custom styles for this template-->
    <link href={{ asset('/css/sb-admin-2.min.css') }} rel="stylesheet">

    <style>

        * {
            font-family: 'Roboto', Times, serif;
            font-size: 12px;
            color: black;
        }
    </style>

    <!-- Custom styles for this page -->
    <link
      href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}"
      rel="stylesheet"
    />


</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col mt-4">
                <h1 class="text-center"><b>Surat Pertanggung Jawaban (SPJ)</b><br>
                </h1>
                <p class="text-center">
                Bagian: Umum
                </p>
                <p class="text-center">
               Jumlah SPJ Panjar : {{ $data->nomor_kop }}
                </p>

            </div>
        </div>
        {{-- <hr color="#000000" style="margin-top: -10px">
        <hr color="#000000" style="height: 5px; margin-top: -10px;"> --}}

        <div class="mt-3">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead class="text-center" >
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">TANGGAL </th>
                      <th scope="col">URAIAN</th>
                      <th scope="col">NO REKENING</th>
                      <th scope="col">JUMLAH PANJAR</th>
                      <th scope="col">SPJ PANJAR</th>
                      <th scope="col">Sisa</th>
                    </tr>
                  </thead>
                <tbody>
                    @php ($np = 1)
                    @php ($lv3 = 0)
                    @foreach ($file as $item)
                    {{-- LEVEL 3 --}}
                    @if ($item->id != $lv3)
                    @php ($lv3 = $item->id)
                    <tr>
                        <td><strong>{{ $np++ }}</strong></td>
                        <td><strong>{{ $item->tgl_entry }}</strong></td>
                        <td><strong>{{ $item->uraian }}</strong></td>
                        <td><strong>{{ $item->rek_kegiatan }}</strong></td>
                        <td style="text-align: right"><strong>{{ $item->jml_panjar }}</strong></td>
                        <td style="text-align: right"><strong>{{ $item->spj_panjar }}</strong></td>
                        <td style="text-align: right"><strong>{{ $item->sisa }}</strong></td>
                    </tr>
                    @endif
                    {{-- <option value="{{ $item-> id }}">{{ $item-> nama_kegiatan }}</option> --}}
                    <tr>
                        <td>{{ $np++ }}</td>
                        @foreach ($spj as $spj)                        
                        <td></td>
                        <td>{{ $spj->uraian_rek }}</td>
                        <td>{{ $spj->rek }}</td>
                        <td style="text-align: right">@uang($spj->panjar_jml)</td>
                        <td style="text-align: right">@uang($spj->panjar_spj)</td>
                        <td style="text-align: right">@uang($spj->sisa_panjar)</td>
                    </tr>
                    @endforeach
                    @endforeach
    </div>

        <!-- Bootstrap core JavaScript-->
    <script src={{ asset('/vendor/jquery/jquery.min.js') }}></script>
    <script src={{ asset('/vendor/bootstrap/js/bootstrap.bundle.min.js') }}></script>

    <!-- Core plugin JavaScript-->
    <script src={{ asset('/vendor/jquery-easing/jquery.easing.min.js') }}></script>

    <!-- Custom scripts for all pages-->
    <script src={{ asset('/js/sb-admin-2.min.js') }}></script>

    <!-- Page level plugins -->
    <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('js/demo/datatables-demo.js') }}"></script>

    </body>

</html>
