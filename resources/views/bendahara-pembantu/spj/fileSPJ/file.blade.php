<style>
    html {
        margin: 40px;
    }

    * {
        box-sizing: border-box;
        margin: 0;
        padding: 0;
        font-family: calibri, Helvetica, sans-serif;
    }

    .table__header {
        padding: 15px;
        padding-bottom: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        position: relative;
    }

    .table__title {
        text-align: center;
    }

    .table__title * {
        margin-bottom: 5px;
    }

    .table__content {
        width: 100%;
    }

    .table__content tr,
    .table__content th,
    .table__content td {
        border: 1px solid black;
    }

    .table__content thead {
        border: 2px solid black;
    }

    .table__content tbody {
        border: 2px solid black;
    }

    .table__content th,
    .table__content td {
        padding: 5px;
    }

    .no {
        width: 2%;
    }

    .tanggal,
    .no-rekening,
    .jml-panjar,
    .spj-panjar,
    .sisa {
        width: 11%;
    }

    .no,
    .tanggal {
        text-align: center;
    }

    .jml-panjar,
    .spj-panjar,
    .sisa {
        text-align: right;
    }

    .table__content tfoot tr,
    .table__content tfoot td {
        border: none;
    }

    .table__content .jumlah {
        border: 1px solid black;
        text-align: right;
    }

    .table__footer {
        padding-top: 30px;
        width: 100%;
    }

    .table__tanggal {
        width: 100%;
        text-align: center;
    }

    .table__sign-container {
        text-align: center;
        clear: right;
        margin-top: 30px;
    }

    .table__sign-container h4 {
        font-weight: normal;
    }

    .table__sign-left {
        width: 50%;
    }

    .table__sign-right {
        width: 50%;
    }

    .table__sign {
        height: 80px;
    }
</style>
<div class="table__container">
    <header class="table__header">
        <div class="table__title">
            <h4>BAGIAN : UMUM</h4>
            <p>Jumlah SPJ Panjar: <span>@uang($total)</span></p>
        </div>
    </header>
    <!-- <p>13</p> -->
    <table class="table__content" style="border-collapse: collapse">
        <!-- @if($repeatHeader) -->
        <thead>
            <!-- @endif -->
            <tr>
                <th>NO</th>
                <th>TANGGAL</th>
                <th>URAIAN</th>
                <th>NO REKENING</th>
                <th>JUMLAH PANJAR</th>
                <th>SPJ PANJAR</th>
                <th>SISA</th>
            </tr>
            <!-- @if($repeatHeader) -->
        </thead>
        <!-- @endif -->
        <tbody>
        @php
            $jml_panjar = 0;
            $bulan_ini = 0;
            $sisa = 0;
            $nomer = 1;
        @endphp
        @foreach ($spj_panjar as $rek_id=>$items)
        
            <tr>
                <td class="no">{{ $nomer++ }}</td>
                <td class="tanggal">{{ Carbon\Carbon::parse($items[0]->tgl_entry)->translatedFormat('d F Y') }}</td>
                <td class="uraian">
                    {{ $items[0]->rekening->nama_kegiatan }}
                </td>
                <td class="no-rekening">{{ $items[0]->rekening->kode_rekening }}</td>
                <td class="jml-panjar"></td>
                <td class="spj-panjar"></td>
                <td class="sisa"></td>
            </tr>
            @foreach ($items as $item)
            @php
                $bulan_ini = $bulan_ini + $item->jml_panjar;
                $sisa = $sisa + $item->jml_panjar;
            @endphp
            <tr>
                <td class="no"></td>
                <td class="tanggal"></td>
                <td class="uraian">
                    {{ $item->subrekening->nama_kegiatan }}
                </td>
                <td class="no-rekening">{{ $item->subrekening->kode_rekening }}</td>
                <td class="jml-panjar"> @uang($item->jml_panjar) </td>
                <td class="spj-panjar"> @uang($item->spj_panjar) </td>
                <td class="sisa">@uang($sisa)</td>
            </tr>
            @endforeach
        @endforeach
            <tr>
                <td class="no">{{ $nomer++ }}</td>
                <td class="tanggal">{{ Carbon\Carbon::parse($pengambilan->tgl_pengambilan)->translatedFormat('d F Y') }}</td>
                <td class="uraian">
                    Pengambilan Uang Melalui {{ $pengambilan->pengambilan }}
                </td>
                <td class="no-rekening"></td>
                <td class="jml-panjar">@uang($bulan_ini)</td>
                <td class="spj-panjar"></td>
                <td class="sisa">@uang($bulan_ini)</td>
            </tr>
        
        @foreach ($spj_panjar_details as $item)
            @php
                $jml_panjar = $jml_panjar + $item->panjar_spj;
                $sisa = $sisa - $item->panjar_spj;

                $arr  = explode(".", $item->rekening->kode_rekening , -2);
                $atas = implode(".", $arr);
            @endphp
            <tr>
                <td class="no">{{ $nomer++ }}</td>
                <td class="tanggal">{{ Carbon\Carbon::parse($item->entry_tgl)->translatedFormat('d F Y') }}</td>
                <td class="uraian" rowspan="2">
                   {{ $item->rekening->nama_kegiatan}} - {{ $item->uraian }}
                </td>
                <td class="no-rekening">{{ $atas }}</td>
                <td class="jml-panjar"></td>
                <td class="spj-panjar"></td>
                <td class="sisa"></td>
            </tr>
            <tr>
                <td class="no"></td>
                <td class="tanggal"></td>
                <td class="no-rekening"> {{ $item->rekening->kode_rekening }}</td>
                <td class="jml-panjar"></td>
                <td class="spj-panjar">@uang($item->panjar_spj)</td>
                <td class="sisa">@uang($sisa)</td>
            </tr>

            @if($item->spj_pajak)
            @foreach ($item->spj_pajak as $pajak)
                <tr>
                        <td class="no"></td>
                        <td class="tanggal"></td>
                        <td class="uraian">
                        terima {{ $pajak->terima_pajak }}
                        </td>
                        <td class="no-rekening"></td>
                        <td class="jml-panjar">@uang($pajak->terima_nominal)</td>
                        <td class="spj-panjar"></td>
                        <td class="sisa"></td>
                </tr>
            @endforeach
            @foreach ($item->spj_pajak as $pajak)
                <tr>
                        <td class="no"></td>
                        <td class="tanggal"></td>
                        <td class="uraian">
                        bayar {{ $pajak->bayar_pajak }}
                        </td>
                        <td class="no-rekening"></td>
                        <td class="jml-panjar"></td>
                        <td class="spj-panjar">@uang($pajak->bayar_nominal)</td>
                        <td class="sisa"></td>
                </tr>
            @endforeach
            @endif
        @endforeach
                <tr>
                        <td class="no"></td>
                        <td class="tanggal"></td>
                        <td class="uraian">
                        Pengembalian sisa panjar ke Bendahara Setda
                        </td>
                        <td class="no-rekening"></td>
                        <td class="jml-panjar"></td>
                        <td class="spj-panjar">@uang($sisa)</td>
                        <td class="sisa">-</td>
                </tr>

            <!-- <tr>
                <td class="no">2</td>
                <td class="tanggal">7 Maret 2022</td>
                <td class="uraian">
                    Koordinasi dan Penyusunan Laporan Capaian Kinerja dan
                    Ikhtisar Realisasi Kinerja SKPD
                </td>
                <td class="no-rekening">4.01.01.2.01.06</td>
                <td class="jml-panjar"></td>
                <td class="spj-panjar"></td>
                <td class="sisa"></td>
            </tr> -->
        </tbody>
        <tfoot>
            <tr>
                <td class=""></td>
                <td class=""></td>
                <td class="">Jumlah bulan ini</td>
                <td class=""></td>
                <td class="jumlah">@uang($bulan_ini)</td>
                <td class="jumlah">@uang($jml_panjar)</td>
                <td class=""></td>
            </tr>
            <tr>
                <td class=""></td>
                <td class=""></td>
                <td class="">Jumlah s/d bulan lalu</td>
                <td class=""></td>
                <td class="jumlah">0</td>
                <td class="jumlah">0</td>
                <td class=""></td>
            </tr>
            <tr>
                <td class=""></td>
                <td class=""></td>
                <td class="">Jumlah semua</td>
                <td class=""></td>
                <td class="jumlah">@uang($bulan_ini)</td>
                <td class="jumlah">@uang($jml_panjar)</td>
                <td class=""></td>
            </tr>
            <tr>
                <td class=""></td>
                <td class=""></td>
                <td class="">Sisa</td>
                <td class=""></td>
                <td class=""></td>
                <td class="jumlah">0</td>
                <td class=""></td>
            </tr>
        </tfoot>
    </table>
    <table class="table__footer">
        <tr class="table__tanggal">
            <td></td>
            <td>Purworejo, {{ Carbon\Carbon::now()->translatedFormat('d F Y') }}</td>
        </tr>
        <tr class="table__sign-container clearfix">
            <td class="table__sign-left">
                <h4 class="">KEPALA BAGIAN UMUM</h4>
                <h4 class="">Selaku Kuasa Pengguna Anggaran</h4>
                <div class="table__sign"></div>
                <p class="">{{$ttd->nama_kepala}}</p>
                <p class="">NIP. {{$ttd->nip_kepala}}</p>
            </td>
            <td class="table__sign-right">
                <h4 class="">BENDAHARA PENGELUARAN PEMBANTU</h4>
                <h4 class="">Bagian Umum Setda Purworejo</h4>
                <div class="table__sign"></div>
                <p class="">{{$ttd->nama_bendahara}}</p>
                <p class="">NIP. {{$ttd->nip_bendahara}}</p>
            </td>
        </tr>
    </table>
</div>

