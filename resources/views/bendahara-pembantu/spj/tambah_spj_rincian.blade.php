@extends('layouts.bendahara_pembantu')

@section('bendahara-pembantu')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-center mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tambah SPJ Rincian Belanja</h1>
        </div>

        <!-- Modal tambah SPJ detail -->
        <div class="modal fade" id="tambahDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="{{ route('spjrincian.store') }}" method="post">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah SPJ Rincian Belanja</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">

                            <input type="hidden" id="detail_id" name="detail_id" value="{{ $spj_detail->id }}">

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="entry_id" class="col-form-label">Tanggal</label>
                                </div>
                                <div class="col-sm-8">
                                    <input class="form-control" type="date" name="tgl_belanja" id="tgl_belanja" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="penjelasan_barang_belanja" class="col-form-label">Nama Barang Belanja</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="penjelasan_barang_belanja" name="penjelasan_barang_belanja" class="form-control" required>
                                </div>
                            </div>  

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="volume" class="col-form-label">Volume</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="volume" name="volume" class="form-control" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="harga_satuan" class="col-form-label">Harga Satuan</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="harga_satuan" name="harga_satuan" class="form-control" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="total" class="col-form-label">Total</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="total" name="total" class="form-control" required readonly>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        
        <!-- Tables-->
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <button class="btn btn-primary" type="button"data-toggle="modal" data-target="#tambahDetailModal"><i class="fas fa-plus-circle"></i> Tambah</button>
                {{-- <a href="{{ route('spj.create') }}" class="btn btn-primary"><i class="fas fa-plus-circle"></i>Tambah</a><br> --}}
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table
                    class="table table-bordered table-striped text-center"
                    id="dataTable"
                    width="100%"
                    cellspacing="0"
                    >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Nama Barang</th>
                                <th>Volume</th>
                                <th>Harga Satuan</th>
                                <th>Total</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($spj_belanja as $item)
                            <tr>
                                <td>{{ $loop-> iteration }}</td>
                                <td>{{ Carbon\Carbon::parse($item->tgl_belanja)->translatedFormat('d F Y') }}</td>
                                <td>{{ $item-> penjelasan_barang_belanja }}</td>
                                <td>{{ $item-> volume }}</td>
                                <td>{{ $item-> harga_satuan }}</td>
                                <td>{{ $item-> total }}</td>
                                <td>
                                    <div class="mb-2">
                                        <form action="{{ route('spjrincian.destroy',$item->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                            <button type="sumbit" onclick="return confirm('Apakah Yakin Ingin Menghapus Data?')" href="{{ url('spj/tambah_spj_rincian/'.$item->id) }}" class="btn btn-outline-danger"><i class="fas fa-trash-alt"></i></i></button>
                                        </form>
                                    </div>

                                    <div class="mb-2">
                                        <a href="edit/{{$item->id}}" class="btn btn-outline-info"><i class="fas fa-fw fa-edit"></i></a>
                                    </div>
                                        
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endsection
        @push('js')
        <script>
            $('#harga_satuan').change(function(){
    console.log($('#harga_satuan').val());
    var harga = $('#volume').val() * $('#harga_satuan').val();
    $('#total').val(harga);
});
</script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#sub_rek_id").change(function() {
        var selected = $('#sub_rek_id').find(":selected");
        var rekening = selected.text();
        var rek_id = selected.data("rek-id");
        $("#rek_id").val(rek_id);

        $.ajax({
                type:'POST',
                url:"{{ route('ajax.kegiatan') }}",
                data:{rekening:rekening},
                success:function(data){
                    var uraian = $("#uraian_rek").val(data.kegiatan);
            }
        });
    });

    $("#sub_rek").change(function() {
        var subrekening = $('#sub_rek').find(":selected").text();

        $.ajax({
                type:'POST',
                url:"{{ route('ajax.kegiatan') }}",
                data:{rekening:subrekening},
                success:function(data){
                    var suburaian = $("#uraian_sub").val(data.kegiatan);
            }
        });
    });
    </script>
@endpush