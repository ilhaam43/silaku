@extends('layouts.bendahara_pembantu')

@section('bendahara-pembantu')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-center mb-4">
            <h1 class="h3 mb-0 text-gray-800">Ubah data SPJ</h1>
        </div>

                <form action="{{ route('spjrincian.update', ['id' => $data->id, 'id2' => $edit->id]) }}" method="post">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah SPJ Rincian Belanja</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">

                            <input type="hidden" id="detail_id" name="detail_id" value={{ $detail_id }}>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="entry_id" class="col-form-label">Tanggal</label>
                                </div>
                                <div class="col-sm-8">
                                    <input class="form-control" type="date" name="tgl_belanja" id="tgl_belanja" value="{{ $edit->tgl_belanja }}" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="penjelasan_barang_belanja" class="col-form-label">Nama Barang Belanja</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="penjelasan_barang_belanja" name="penjelasan_barang_belanja" class="form-control" required value="{{ $edit->penjelasan_barang_belanja }}">
                                </div>
                            </div>  

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="volume" class="col-form-label">Volume</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="volume_ubah" name="volume" class="form-control" required value="">
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="harga_satuan" class="col-form-label">Harga Satuan</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="harga_satuan_ubah" name="harga_satuan" class="form-control" required value="">
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="total" class="col-form-label">Total</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="total_ubah" name="total" class="form-control" required>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
        @endsection
        @push('js')
        <script>
    $('#harga_satuan_ubah').change(function(){
    console.log($('#harga_satuan_ubah').val());
    var harga = $('#volume_ubah').val() * $('#harga_satuan_ubah').val();
    $('#total_ubah').val(harga);
});
</script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#sub_rek_id").change(function() {
        var selected = $('#sub_rek_id').find(":selected");
        var rekening = selected.text();
        var rek_id = selected.data("rek-id");
        $("#rek_id").val(rek_id);

        $.ajax({
                type:'POST',
                url:"{{ route('ajax.kegiatan') }}",
                data:{rekening:rekening},
                success:function(data){
                    var uraian = $("#uraian").val(data.kegiatan);
            }
        });
    });

    // $("#sub_rek").change(function() {
    //     var subrekening = $('#sub_rek').find(":selected").text();

    //     $.ajax({
    //             type:'POST',
    //             url:"{{ route('ajax.kegiatan') }}",
    //             data:{rekening:subrekening},
    //             success:function(data){
    //                 var suburaian = $("#uraian_sub").val(data.kegiatan);
    //         }
    //     });
    </script>
@endpush