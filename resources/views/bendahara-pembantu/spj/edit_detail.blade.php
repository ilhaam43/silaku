@extends('layouts.bendahara_pembantu')

@section('bendahara-pembantu')
    <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-center mb-4">
            <h1 class="h3 mb-0 text-gray-800">Ubah data SPJ</h1>
        </div>
        <form action="{{ route('spjdetail.update', ['id' => $data->id, 'id2' => $edit->id]) }}" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Detail SPJ</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="npd_id" name="panjar_id" value={{ $panjar_id }}>
                    <div class="row mb-3">
                        <div class="col-sm-4">
                            <label for="entry_id" class="col-form-label">Tanggal</label>
                        </div>
                        <div class="col-sm-8">
                            <input class="form-control" type="date" name="entry_tgl" id="entry_tgl" value="{{ $edit->entry_tgl }}" required>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-sm-4">
                            <label for="rek" class="col-form-label">No. Rekening</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="hidden" id="rek_id" name="rek_id" value="{{ $edit->rek_id }}" />
                            <select type="text" class="form-control" name="sub_rek_id" id="sub_rek_id" readonly>
                                <option value="">- Pilih nomor rekening -</option>
                                    @foreach ($npd_detail as $item)  
                                        <option data-rek-id="{{$item->rek_id}}" value="{{ $item-> sub_rek_id }}"
                                            {{ $item->rek_id == $edit->rekening->id ? 'selected' : '' }}>{{ $item-> kode_rekening }} - {{ $item-> nama_kegiatan }}  </option>
                                    @endforeach
                            </select>
                        </div>
                    </div>

                    <!-- <div class="row mb-3">
                        <div class="col-sm-4">
                            <label for="uraian_rek" class="col-form-label">Uraian</label>
                        </div>
                        <div class="col-sm-8">
                            <textarea name="uraian_rek" id="uraian_rek" cols="20" rows="5" class="form-control" ></textarea>
                        </div>
                    </div> -->

                    <div class="row mb-3">
                        <div class="col-sm-4">
                            <label for="panjar_jml" class="col-form-label">Jumlah Panjar</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="text" id="panjar_jml" name="panjar_jml" class="form-control" value="{{ $edit->panjar_jml }}" required>
                        </div>
                    </div>  

                    <div class="row mb-3">
                        <div class="col-sm-4">
                            <label for="spj_panjar" class="col-form-label">SPJ Panjar</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="text" id="spj_panjar" name="panjar_spj" class="form-control" value="{{ $edit->panjar_spj }}" required>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-sm-4">
                            <label for="sisa_panjar" class="col-form-label">Uraian</label>
                        </div>
                        <div class="col-sm-8">
                            <textarea name="uraian" id="uraian" cols="30" rows="10" value="{{ $edit->uraian}}" required>{{ $edit->uraian }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>
@endsection