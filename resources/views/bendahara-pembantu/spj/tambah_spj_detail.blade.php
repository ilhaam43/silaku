@extends('layouts.bendahara_pembantu')

@section('bendahara-pembantu')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-center mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tambah detail SPJ</h1>
        </div>

        <!-- Modal tambah SPJ detail -->
        <div class="modal fade" id="tambahDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="{{ route('spjdetail.store') }}" method="post">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah Detail SPJ</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">

                            <input type="hidden" id="npd_id" name="panjar_id" value="{{ $spj->id }}">
                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="entry_id" class="col-form-label">Tanggal</label>
                                </div>
                                <div class="col-sm-8">
                                    <input class="form-control" type="date" name="entry_tgl" id="entry_tgl" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="rek" class="col-form-label">No. Rekening</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="hidden" id="rek_id" name="rek_id" value="{{ $spj->rek_id }}" />
                                    <select type="text" class="form-control" name="sub_rek_id" id="sub_rek_id" readonly>
                                        <option value="">- Pilih nomor rekening -</option>
                                            @foreach ($npd_detail as $item)  
                                                <option data-rek-id="{{$item->rek_id}}" value="{{ $item-> sub_rek_id }}"
                                                    {{ $item->rek_id == $spj->rekening->id ? 'selected' : '' }}>{{ $item-> kode_rekening }} - {{ $item-> nama_kegiatan }}  </option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>

                            <!-- <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="uraian_rek" class="col-form-label">Uraian</label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="uraian_rek" id="uraian_rek" cols="20" rows="5" class="form-control" ></textarea>
                                </div>
                            </div> -->

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="panjar_jml" class="col-form-label">Jumlah Panjar</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="panjar_jml" name="panjar_jml" class="form-control" required>
                                </div>
                            </div>  

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="spj_panjar" class="col-form-label">SPJ Panjar</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="spj_panjar" name="panjar_spj" class="form-control" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="sisa_panjar" class="col-form-label">Uraian</label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="uraian" id="uraian" cols="30" rows="10" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        
        <!-- Tables-->
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <button class="btn btn-primary" type="button"data-toggle="modal" data-target="#tambahDetailModal"><i class="fas fa-plus-circle"></i> Tambah</button>
                {{-- <a href="{{ route('spj.create') }}" class="btn btn-primary"><i class="fas fa-plus-circle"></i>Tambah</a><br> --}}
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table
                    class="table table-bordered table-striped text-center"
                    id="dataTable"
                    width="100%"
                    cellspacing="0"
                    >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>No Rekening</th>
                                <th>Belanja</th>
                                <th>Jumlah Panjar</th>
                                <th>Spj Panjar</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($spj_detail as $item)
                            <tr>
                                <td>{{ $loop-> iteration }}</td>
                                <td>{{ Carbon\Carbon::parse($item->entry_tgl)->translatedFormat('d F Y') }}</td>
                                <td>{{ $item-> rekening->nama_kegiatan }}</td>
                                <td>{{ $item-> subrekening->nama_kegiatan }}</td>
                                <td>{{ $item-> panjar_jml }}</td>
                                <td>{{ $item-> panjar_spj }}</td>
                                <td>
                                    <div class="mb-2">
                                        <form action="{{ route('spjdetail.destroy',$item->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                            <button type="sumbit" onclick="return confirm('Apakah Yakin Ingin Menghapus Data?')" href="{{ url('spj/tambah_spj_detail/'.$item->id) }}" class="btn btn-outline-danger"><i class="fas fa-trash-alt"></i></i></button>
                                        </form>
                                    </div>

                                    <div class="mb-2">
                                        <a class="btn btn-outline-primary" type="button" href="{{ route('spjrincian.input',$item->id) }}"><i class="fas fa-plus-circle"></i></a>
                                        {{-- <a href="{{ route('spj.create') }}" class="btn btn-outline-info"><i class="fas fa-plus-circle"></i></a>--}}
                                    </div>

                                    <div class="mb-2">
                                        <a class="btn btn-outline-success" type="button" href="{{ route('pajak.input',$item->id) }}"><i class="fas fa-comment-dollar"></i></a>
                                    </div>

                                    <div class="mb-2">
                                        <a href="update/{{$item->id}}" class="btn btn-outline-info user__edit-btn"><i class="fas fa-fw fa-edit"></i></a>
                                    </div>
                                        
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endsection
        @push('js')
        <script>
            $('#harga_satuan').change(function(){
    console.log($('#harga_satuan').val());
    var harga = $('#volume').val() * $('#harga_satuan').val();
    $('#total').val(harga);
});
</script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#sub_rek_id").change(function() {
        var selected = $('#sub_rek_id').find(":selected");
        var rekening = selected.text();
        var rek_id = selected.data("rek-id");
        $("#rek_id").val(rek_id);

        $.ajax({
                type:'POST',
                url:"{{ route('ajax.kegiatan') }}",
                data:{rekening:rekening},
                success:function(data){
                    var uraian = $("#uraian_rek").val(data.kegiatan);
            }
        });
    });

    $("#sub_rek").change(function() {
        var subrekening = $('#sub_rek').find(":selected").text();

        $.ajax({
                type:'POST',
                url:"{{ route('ajax.kegiatan') }}",
                data:{rekening:subrekening},
                success:function(data){
                    var suburaian = $("#uraian_sub").val(data.kegiatan);
            }
        });
    });
    </script>
@endpush