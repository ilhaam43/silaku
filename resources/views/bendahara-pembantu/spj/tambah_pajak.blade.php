@extends('layouts.bendahara_pembantu')

@section('bendahara-pembantu')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-center mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tambah Pajak SPJ</h1>
        </div>

        <!-- Modal tambah SPJ detail -->
        <div class="modal fade" id="tambahDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="{{ route('pajak.store') }}" method="post">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah Pajak SPJ</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">

                            <input type="hidden" id="detail_id" name="detail_id" value="{{ $spj_detail->id }}">


                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="rek" class="col-form-label">Terima Pajak</label>
                                </div>
                                <div class="col-sm-8">
                                    <select type="text" class="form-control" name="terima_pajak" id="terima_pajak">
                                        <option value="">- Pilih -</option>
                                        <option value="PPN">PPN</option>
                                        <option value="PPH 21">PPH 21</option>
                                        <option value="PPH 22">PPH 22</option>
                                        <option value="PPH 23">PPH 23</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="terima_nominal" class="col-form-label">Nominal Terima Pajak</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="terima_nominal" name="terima_nominal" class="form-control" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="rek" class="col-form-label">Bayar Pajak</label>
                                </div>
                                <div class="col-sm-8">
                                    <select type="text" class="form-control" name="bayar_pajak" id="bayar_pajak">
                                        <option value="">- Pilih -</option>
                                        <option value="PPN">PPN</option>
                                        <option value="PPH 21">PPH 21</option>
                                        <option value="PPH 22">PPH 22</option>
                                        <option value="PPH 23">PPH 23</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="bayar_nominal" class="col-form-label">Nominal Bayar Pajak</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="bayar_nominal" name="bayar_nominal" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        
        <!-- Tables-->
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <button class="btn btn-primary" type="button"data-toggle="modal" data-target="#tambahDetailModal"><i class="fas fa-plus-circle"></i> Tambah</button>
                {{-- <a href="{{ route('spj.create') }}" class="btn btn-primary"><i class="fas fa-plus-circle"></i>Tambah</a><br> --}}
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table
                    class="table table-bordered table-striped text-center"
                    id="dataTable"
                    width="100%"
                    cellspacing="0"
                    >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Terima Pajak</th>
                                <th>Nominal Terima Pajak</th>
                                <th>Bayar Pajak</th>
                                <th>Nominal Bayar Pajak</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pajak as $item)
                            <tr>
                                <td>{{ $loop-> iteration }}</td>
                                <td>{{ Carbon\Carbon::parse($item->tgl_belanja)->translatedFormat('d F Y') }}</td>
                                <td>{{ $item-> terima_pajak }}</td>
                                <td>{{ $item-> terima_nominal }}</td>
                                <td>{{ $item-> bayar_pajak }}</td>
                                <td>{{ $item-> bayar_nominal }}</td>
                                <td>
                                    <div class="mb-2">
                                        <form action="{{ route('pajak.destroy',$item->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                            <button type="sumbit" onclick="return confirm('Apakah Yakin Ingin Menghapus Data?')" href="{{ url('spj/tambah_pajak/'.$item->id) }}" class="btn btn-outline-danger"><i class="fas fa-trash-alt"></i></i></button>
                                        </form>
                                    </div>

                                    <div class="mb-2">
                                        <button class="btn btn-outline-info" type="button"data-toggle="modal" data-target="#UbahModal{{ $item->id }}"><i class="fas fa-fw fa-edit"></i></button>
                                    </div>
                                        
                                </td>
                            </tr>
                            <!-- Modal Ubah Pajak -->
                            <div class="modal fade" id="UbahModal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <form action="{{ route('pajak.update', $item->id) }}" method="post">
                                        @csrf
                                        @method('PATCH')
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Tambah Pajak SPJ</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                            </div>
                                            <div class="modal-body">

                                                <input type="hidden" id="detail_id" name="detail_id" value="{{ $spj_detail->id }}">


                                                <div class="row mb-3">
                                                    <div class="col-sm-4">
                                                        <label for="rek" class="col-form-label">Terima Pajak</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <select type="text" class="form-control" name="terima_pajak" id="terima_pajak">
                                                            <option value="{{ $item->terima_pajak }}">{{ $item->terima_pajak }}</option>
                                                            <option value="PPN">PPN</option>
                                                            <option value="PPH 21">PPH 21</option>
                                                            <option value="PPH 22">PPH 22</option>
                                                            <option value="PPH 23">PPH 23</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row mb-3">
                                                    <div class="col-sm-4">
                                                        <label for="terima_nominal" class="col-form-label">Nominal Terima Pajak</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input type="text" id="terima_nominal" name="terima_nominal" class="form-control" required value="{{ $item->terima_nominal }}">
                                                    </div>
                                                </div>

                                                <div class="row mb-3">
                                                    <div class="col-sm-4">
                                                        <label for="rek" class="col-form-label">Bayar Pajak</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <select type="text" class="form-control" name="bayar_pajak" id="bayar_pajak">
                                                            <option value="{{ $item->bayar_pajak }}">{{ $item->bayar_pajak }}</option>
                                                            <option value="PPN">PPN</option>
                                                            <option value="PPH 21">PPH 21</option>
                                                            <option value="PPH 22">PPH 22</option>
                                                            <option value="PPH 23">PPH 23</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row mb-3">
                                                    <div class="col-sm-4">
                                                        <label for="bayar_nominal" class="col-form-label">Nominal Bayar Pajak</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input type="text" id="bayar_nominal" name="bayar_nominal" class="form-control" required value="{{ $item->bayar_nominal }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endsection
        @push('js')
        <script>
            $('#harga_satuan').change(function(){
    console.log($('#harga_satuan').val());
    var harga = $('#volume').val() * $('#harga_satuan').val();
    $('#total').val(harga);
});
</script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#sub_rek_id").change(function() {
        var selected = $('#sub_rek_id').find(":selected");
        var rekening = selected.text();
        var rek_id = selected.data("rek-id");
        $("#rek_id").val(rek_id);

        $.ajax({
                type:'POST',
                url:"{{ route('ajax.kegiatan') }}",
                data:{rekening:rekening},
                success:function(data){
                    var uraian = $("#uraian_rek").val(data.kegiatan);
            }
        });
    });

    $("#sub_rek").change(function() {
        var subrekening = $('#sub_rek').find(":selected").text();

        $.ajax({
                type:'POST',
                url:"{{ route('ajax.kegiatan') }}",
                data:{rekening:subrekening},
                success:function(data){
                    var suburaian = $("#uraian_sub").val(data.kegiatan);
            }
        });
    });
    </script>
@endpush