@extends('layouts.bendahara_pembantu')

@section('bendahara-pembantu')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-center mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tambah data SPJ Kegiatan</h1>
        </div>

        <!-- Modal tambah SPJ -->
        <div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="{{ route('spj.store') }}" method="post">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah SPJ</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">

                            <input type="hidden" id="npd_id" name="npd_id">

                            <!-- <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="rek" class="col-form-label">Jenis rekening</label>
                                </div>
                                <div class="col-sm-8">
                                    <select type="text" class="form-control" name="rek_id" id="rek_id">
                                        <option value="">- Pilih jenis rekening -</option>
                                        <option value="0">Kegiatan </option>
                                        <option value="1">Sub Kegiatan </option>
                                    </select>
                                </div>
                            </div> -->

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="entry_id" class="col-form-label">Tanggal</label>
                                </div>
                                <div class="col-sm-8">
                                    <input class="form-control" type="date" name="tgl_entry" id="tgl_entry" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="rek" class="col-form-label">No. Rekening</label>
                                </div>
                                <div class="col-sm-8">
                                <input type="hidden" id="rek_id" name="rek_id"/>
                                    <select type="text" class="form-control" name="sub_rek_id" id="sub_rek_id" required>
                                        <option value="">- Pilih nomor rekening -</option>
                                            @foreach ($npd_detail as $rek_id => $items)
                                            <optgroup label="{{ $rek_id }}">
                                                @foreach ($items as $item)
                                                    
                                                <option value="{{ $item-> sub_rek_id }}" data-rek-id="{{$item->rek_id}}" data-pencairan="{{$item->pencairan_saat_ini}}" data-npd-id="{{$item->id}}">{{ $item-> kode_rekening }} - {{ $item-> nama_kegiatan }}</option>
                                                @endforeach
                                            </optgroup>
                                            @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="rek" class="col-form-label">Jenis Spj</label>
                                </div>
                                <div class="col-sm-8">
                                    <select type="text" class="form-control" name="jenis_spj" id="jenis_spj" required>
                                        <option value="">- Pilih Jenis Spj -</option>
                                        <option value="Spj Panjar">Spj Panjar</option>
                                        <option value="Spj Ls">Spj Ls</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="panjar_jml" class="col-form-label">Jumlah Panjar/LS</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="jml_panjar" name="jml_panjar" class="form-control" readonly>
                                </div>
                            </div>  

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="spj_panjar" class="col-form-label">SPJ Panjar/LS</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="spj_panjar" name="spj_panjar" class="form-control" required>
                                </div>
                            </div>
                                
                            <!-- <table border="0" align="center" id="dynamicTable">
                                <tr>
                                    <td><label for="penjelasan_barang_belanja">Penjelasan <br> Barang <br> Belanja</label></td><td>:</td><td><br><br></td><td><input type="text" id="penjelasan_barang_belanja" name="penjelasan_barang_belanja" placeholder="Nama Barang"><br><br><input type="text" name="volume" placeholder="Volume"><br><br><input type="text" id="harga_satuan" name="harga_satuan" placeholder="Harga Satuan"><br><br><input type="text" id="total" name="total" placeholder="Total Harga"><br></td>                            
                                </tr>
                                <tr>
                                    <td><label for="">No. Rek</label></td><td>:</td><td><br><br></td><td>
                                        <select type="text" class="" name="sub_rek" id="sub_rek">
                                            <option value="">-Pilih-</option>
                                                @foreach ($rekening as $item)    
                                                    @if($item->level == 5)
                                                        <option value="{{ $item-> kode_rekening }}">{{ $item-> kode_rekening }}</option>
                                                    @endif
                                                @endforeach
                                        </select>
                                    </td>                            
                                </tr>
                                <tr>
                                    <td><label for="uraian_sub">Uraian Sub <br> Kegiatan No. Rek</label></td><td>:</td><td><br><br></td><td><textarea name="uraian_sub" id="uraian_sub" cols="20" rows="5"></textarea></td>                            
                                </tr>
                            </table> -->
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        
        <!-- Tables-->
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#tambahModal"><i class="fas fa-plus-circle"></i> Tambah</button>
                {{-- <a href="{{ route('spj.create') }}" class="btn btn-primary"><i class="fas fa-plus-circle"></i>Tambah</a><br> --}}
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table
                    class="table table-bordered table-striped text-center"
                    id="dataTable"
                    width="100%"
                    cellspacing="0"
                    >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Uraian</th>
                                <th>Belanja</th>
                                <th>Jenis Spj</th>
                                <th>Jumlah Panjar</th>
                                <th>Spj Panjar</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($spj_panjar as $item)
                            <tr>
                                <input type="hidden" class="serdelete_val" value="$item->id">
                                <td>{{ $loop-> iteration }}</td>
                                <td>{{ Carbon\Carbon::parse($item->tgl_entry)->translatedFormat('d F Y') }}</td>
                                <td>{{ $item-> rekening->nama_kegiatan }}</td>
                                <td>{{ $item-> subrekening->nama_kegiatan }}</td>
                                <td>{{ $item-> jenis_spj }}</td>
                                <td>Rp.{{ number_format($item-> jml_panjar) }}</td>
                                <td>Rp.{{ number_format($item-> spj_panjar) }}</td>
                                <td>
                                <div class="mb-2">
                                        <button class="btn btn-outline-danger user__delete-btn" data-url_hapus="{{ route('spj.destroy', ['id' => $item->npd->id, 'id2' => $item->id]) }}"><i class="fas fa-fw fa-trash"></i></button>
                                    </div>
                                    <div class="mb-2">
                                        <a class="btn btn-outline-primary" type="button" href="{{ route('spjdetail.input',$item->id) }}"><i class="fas fa-plus-circle"></i></a>
                                        {{-- <a href="{{ route('spj.create') }}" class="btn btn-outline-info"><i class="fas fa-plus-circle"></i></a>--}}
                                    </div>
                                    </div>
                                    <a class="btn btn-outline-info" type="button" href="{{ route('spj.edit',$item->id) }}"><i class="fas fa-fw fa-edit"></i></a>
                                    <div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endsection
        @push('js')

    <script src="{{ asset('js/spj.js') }}"></script>


        <script>
            $('#harga_satuan').change(function(){
    console.log($('#harga_satuan').val());
    var harga = $('#volume').val() * $('#harga_satuan').val();
    $('#total').val(harga);
});
</script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#sub_rek_id").change(function() {
        var selected = $('#sub_rek_id').find(":selected");
        var rekening = selected.text();
        var rek_id = selected.data("rek-id");
        var npd_id = selected.data("npd-id");
        var jml_panjar = selected.data("pencairan");
        $("#rek_id").val(rek_id);
        $("#npd_id").val(npd_id);
        $("#jml_panjar").val(jml_panjar);
        
        $.ajax({
                type:'POST',
                url:"{{ route('ajax.kegiatan') }}",
                data:{rekening:rekening},
                success:function(data){
                    var uraian = $("#uraian_rek").val(data.kegiatan);
            }
        });
    });

    $("#sub_rek").change(function() {
        var subrekening = $('#sub_rek').find(":selected").text();

        $.ajax({
                type:'POST',
                url:"{{ route('ajax.kegiatan') }}",
                data:{rekening:subrekening},
                success:function(data){
                    var suburaian = $("#uraian_sub").val(data.kegiatan);
            }
        });
    });
    </script>
@endpush