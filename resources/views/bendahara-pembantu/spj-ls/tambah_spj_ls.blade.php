@extends('layouts.bendahara_pembantu')

@section('bendahara-pembantu')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-center mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tambah data SPJ LS</h1>
        </div>

        <!-- Modal tambah SPJ -->
        <div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="{{ route('spj-ls.store') }}" method="post">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah SPJ</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">

                            <input type="hidden" id="npd_id" name="npd_id" value="{{ $npd->id }}" >

                            <!-- <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="rek" class="col-form-label">Jenis rekening</label>
                                </div>
                                <div class="col-sm-8">
                                    <select type="text" class="form-control" name="rek_id" id="rek_id">
                                        <option value="">- Pilih jenis rekening -</option>
                                        <option value="0">Kegiatan </option>
                                        <option value="1">Sub Kegiatan </option>
                                    </select>
                                </div>
                            </div> -->

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="entry_id" class="col-form-label">Tanggal</label>
                                </div>
                                <div class="col-sm-8">
                                    <input class="form-control" type="date" name="tgl_entry_ls" id="tgl_entry_ls" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="rek" class="col-form-label">No. Rekening</label>
                                </div>
                                <div class="col-sm-8">
                                <input type="hidden" id="rek_id" name="rek_id"/>
                                    <select type="text" class="form-control" name="sub_rek_id" id="sub_rek_id" required>
                                        <option value="">- Pilih nomor rekening -</option>
                                            @foreach ($npd_detail as $item)  
                                                <option value="{{ $item-> sub_rek_id }}" data-rek-id="{{$item->rek_id}}">{{ $item-> kode_rekening }} - {{ $item-> nama_kegiatan }}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="panjar_jml" class="col-form-label">Jumlah Panjar</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="jml_panjar_ls" name="jml_panjar_ls" class="form-control" value="{{ $npd->pencairan_saat_ini }}" readonly>
                                </div>
                            </div>  

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="spj_panjar" class="col-form-label">SPJ Panjar</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="spj_panjar_ls" name="spj_panjar_ls" class="form-control" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="spj_panjar" class="col-form-label">Sisa</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="sisa_ls" name="sisa_ls" class="form-control" required>
                                </div>
                            </div>
                                
                            <!-- <table border="0" align="center" id="dynamicTable">
                                <tr>
                                    <td><label for="penjelasan_barang_belanja">Penjelasan <br> Barang <br> Belanja</label></td><td>:</td><td><br><br></td><td><input type="text" id="penjelasan_barang_belanja" name="penjelasan_barang_belanja" placeholder="Nama Barang"><br><br><input type="text" name="volume" placeholder="Volume"><br><br><input type="text" id="harga_satuan" name="harga_satuan" placeholder="Harga Satuan"><br><br><input type="text" id="total" name="total" placeholder="Total Harga"><br></td>                            
                                </tr>
                                <tr>
                                    <td><label for="">No. Rek</label></td><td>:</td><td><br><br></td><td>
                                        <select type="text" class="" name="sub_rek" id="sub_rek">
                                            <option value="">-Pilih-</option>
                                                @foreach ($rekening as $item)    
                                                    @if($item->level == 5)
                                                        <option value="{{ $item-> kode_rekening }}">{{ $item-> kode_rekening }}</option>
                                                    @endif
                                                @endforeach
                                        </select>
                                    </td>                            
                                </tr>
                                <tr>
                                    <td><label for="uraian_sub">Uraian Sub <br> Kegiatan No. Rek</label></td><td>:</td><td><br><br></td><td><textarea name="uraian_sub" id="uraian_sub" cols="20" rows="5"></textarea></td>                            
                                </tr>
                            </table> -->
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        
        <!-- Tables-->
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#tambahModal"><i class="fas fa-plus-circle"></i> Tambah</button>
                {{-- <a href="{{ route('spj.create') }}" class="btn btn-primary"><i class="fas fa-plus-circle"></i>Tambah</a><br> --}}
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table
                    class="table table-bordered table-striped text-center"
                    id="dataTable"
                    width="100%"
                    cellspacing="0"
                    >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal Entry</th>
                                <th>Uraian</th>
                                <th>Sub Kegiatan</th>
                                <th>Jumlah Panjar</th>
                                <th>Spj Panjar</th>
                                <th>Sisa</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($spjls as $item)
                            <tr>
                                <td>{{ $loop-> iteration }}</td>
                                <td>{{ Carbon\Carbon::parse($item->tgl_entry)->translatedFormat('d F Y') }}</td>
                                <td>{{ $item-> rekening->nama_kegiatan }}</td>
                                <td>{{ $item-> subrekening->nama_kegiatan }}</td>
                                <td>{{ $item-> jml_panjar_ls }}</td>
                                <td>{{ $item-> spj_panjar_ls }}</td>
                                <td>{{ $item-> sisa_ls }}</td>
                                <td>
                                <div class="mb-2">
                                    <form action="{{ route('spj.destroy',$item->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                        <button type="sumbit" onclick="return confirm('Apakah Yakin Ingin Menghapus Data?')" href="{{ url('spj/tambah_spj_panjar/'.$item->id) }}" class="btn btn-outline-danger"><i class="fas fa-trash-alt"></i></i></button>
                                    </form>
                                    </div>
                                    <div class="mb-2">
                                        <a class="btn btn-outline-primary" type="button" href="{{ route('spj-ls-detail.input',$item->id) }}"><i class="fas fa-plus-circle"></i></a>
                                        {{-- <a href="{{ route('spj.create') }}" class="btn btn-outline-info"><i class="fas fa-plus-circle"></i></a>--}}
                                    </div>
                                    </div>
                                    <a class="btn btn-outline-info" type="button" href="{{ route('spj-ls.edit',$item->id) }}"><i class="fas fa-fw fa-edit"></i></a>
                                    <div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endsection
        @push('js')
        <script>
            $('#harga_satuan').change(function(){
    console.log($('#harga_satuan').val());
    var harga = $('#volume').val() * $('#harga_satuan').val();
    $('#total').val(harga);
});
</script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#sub_rek_id").change(function() {
        var selected = $('#sub_rek_id').find(":selected");
        var rekening = selected.text();
        var rek_id = selected.data("rek-id");
        $("#rek_id").val(rek_id);

        $.ajax({
                type:'POST',
                url:"{{ route('ajax.kegiatan') }}",
                data:{rekening:rekening},
                success:function(data){
                    var uraian = $("#uraian").val(data.kegiatan);
            }
        });
    });

    // $("#sub_rek").change(function() {
    //     var subrekening = $('#sub_rek').find(":selected").text();

    //     $.ajax({
    //             type:'POST',
    //             url:"{{ route('ajax.kegiatan') }}",
    //             data:{rekening:subrekening},
    //             success:function(data){
    //                 var suburaian = $("#uraian_sub").val(data.kegiatan);
    //         }
    //     });
    </script>
@endpush