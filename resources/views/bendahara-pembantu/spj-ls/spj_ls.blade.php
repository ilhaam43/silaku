@extends('layouts.bendahara_pembantu')
@push('head')
    <!-- Custom styles for this page -->
    <link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}"rel="stylesheet"/>
@endpush
@section('bendahara-pembantu')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-center mb-4">
            <h1 class="h3 mb-0 text-gray-800">Daftar data SPJ LS</h1>
        </div>

        <!-- Modal tambah SPJ -->
        <div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="" method="post" class="form-horizontal">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah SPJ</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">
                            <label for="rek_kegiatan">Kode Rekening :</label>
                            <select name="rek_kegiatan" id="rek_kegiatan">
                                <option value="">-Pilih-</option>
                                @foreach ($rekening as $item)
                                    @if($item->level == 4)
                                        <option value="{{ $item-> kode_rekening }}">{{ $item-> kode_rekening }}</option>
                                    @endif
                                @endforeach
                            </select><br>
                            <label for="uraian">Uraian :</label>
                            <input type="text" name="uraian" id="uraian"><br>
                            <label for="sub_rek_kegiatan">Kode Rekening :</label>
                            <select name="sub_rek_kegiatan" id="sub_rek_kegiatan">
                                <option value="">-Pilih-</option>
                                @foreach ($rekening as $item)
                                    @if($item->level == 5)
                                        <option value="{{ $item-> kode_rekening }}">{{ $item-> kode_rekening }}</option>
                                    @endif
                                @endforeach
                            </select><br>
                            <label for="uraian"> Sub Uraian :</label>
                            <input type="text" name="sub_uraian" id="sub_uraian"><br>
                            <label for="jml_panjar">Jumlah :</label>
                            <input type="text" name="jml_panjar" id=""><br>
                            <label for="spj_panjar">Spj Panjar :</label>
                            <input type="text" name="spj_panjar" id=""><br>
                            <label for="sisa">Sisa :</label>
                            <input type="text" name="sisa" id=""><br>
                            <label for="tgl_entry">Tanggal Cetak :</label>
                            <input type="date" name="tgl_entry" id="tgl_entry">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>



        <!-- Modal cetak -->
        <div class="modal fade" id="cetakModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Pilih Rentang Bulan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
                    <div class="modal-body">
                        <input type="date" name="" id=""> S.D <input type="date" name="" id="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary">Cetak</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Tables-->
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-2">
                        <div class="form-group text-left">
                            <input class="form-control" type="date" name="tgl_dpa" id="">
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group text-left">
                            <input class="form-control" type="date" name="tgl_dpa" id="">
                        </div>
                    </div>
                    <div class="col-2">
                        <button class="btn btn-success" type="button" data-toggle="modal" data-target="#cetakModal">Cetak</button>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <table class="table table-bordered table-striped text-center" id="dataTable" width="100%"cellspacing="0" >
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>No NPD</th>
                            <th>Nomer DPA</th>
                            <th>Tanggal DPA</th>
                            <th>Aksi</th>
                            <th>Cetak</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($daftar_npd as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ Carbon\Carbon::parse($item->created_at)->translatedFormat('d F Y') }}</td>
                            <td>{{ $item->nomor_kop }}</td>
                            <td>{{ $item->nomor_dpa }}</td>
                            <td>{{ Carbon\Carbon::parse($item->tgl_dpa)->translatedFormat('d F Y') }}</td>

                            <td>
                                <!-- <a class="btn btn-primary" type="button" href="{{ route('spj.edit',$item->id) }}"><i class="fas fa-pencil-alt"></i></a> | -->
                                <div class="mb-2">
                                    <a class="btn btn-primary" type="button" href="{{ route('spj-ls.input',$item->id) }}"><i class="fas fa-list"></i></a>
                                </div>
                                <div class="mb-2">
                                    <button class="btn btn-warning" type="button" data-toggle="modal" data-target="#PengambilanModal{{ $item->id }}" ><i class="fas fa-hand-holding-usd"></i></button>
                                </div>
                                <div class="mb-2">
                                    <button class="btn btn-success" type="button" data-toggle="modal" data-target="#UbahModal{{ $item->id }}" ><i class="fas fa-hand-holding-usd"></i></button>
                                </div>

        <!-- Modal tambah Pengambilan -->
        <div class="modal fade" id="PengambilanModal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="{{ route('pengambilan-ls.store') }}" method="post">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Pengambilan</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">

                            <input type="text" id="npd_id" name="kop_id" value="{{ $item->id }}" >

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="entry_id" class="col-form-label">Tanggal</label>
                                </div>
                                <div class="col-sm-8">
                                    <input class="form-control" type="date" name="tgl_pengambilan_ls" id="tgl_pengambilan_ls" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="rek" class="col-form-label">Pengambilan</label>
                                </div>
                                <div class="col-sm-8">
                                    <select type="text" class="form-control" name="pengambilan_ls" id="pengambilan_ls" required>
                                        <option value="#">- Pilih Pengambilan -</option>  
                                        <option value="Tunai">Tunai</option>
                                        <option value="CMS">Melalui CMS</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

@foreach ($pengambilan as $r)
    
<!-- Modal Ubah Pengambilan Pengambilan -->
        <div class="modal fade" id="UbahModal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="{{ route('pengambilan-ls.update', ['id' => $r->id])}}" method="post">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Pengambilan</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">

                            <input type="text" id="npd_id" name="kop_id" value="{{ $item->id }}" >

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="entry_id" class="col-form-label">Tanggal</label>
                                </div>
                                <div class="col-sm-8">
                                    <input class="form-control" type="date" name="tgl_pengambilan_ls" id="tgl_pengambilan_ls" value="{{ $r->tgl_pengambilan_ls }}" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="rek" class="col-form-label">Pengambilan</label>
                                </div>
                                <div class="col-sm-8">
                                    <select type="text" class="form-control" name="pengambilan_ls" id="pengambilan_ls" required>
                                        <option value="{{ $r->pengambilan_ls }}">{{ $r->pengambilan_ls }}</option>  
                                        <option value="Tunai">Tunai</option>
                                        <option value="CMS">Melalui CMS</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endforeach

                            <!-- <form action="/spj/spj_panjar/{{ $item->id }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="sumbit" onclick="return confirm('Apakah Yakin Ingin Menghapus Data?')" href="{{ url('spj/spj_panjar/'.$item->id) }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></i></button>
                            </form> -->
                            </td>
                            <td>
                                <a class="btn btn-info" type="button" target="_blank" href="{{ route('spj.file',$item->id) }}"><i class="fas fa-file"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#rek_kegiatan").change(function() {
        var rekening = $('#rek_kegiatan').find(":selected").text();

        $.ajax({
                type:'POST',
                url:"{{ route('ajax.kegiatan') }}",
                data:{rekening:rekening},
                success:function(data){
                    var uraian = $("#uraian").val(data.kegiatan);
            }
        });
    });

    $("#sub_rek_kegiatan").change(function() {
        var subrekening = $('#sub_rek_kegiatan').find(":selected").text();

        $.ajax({
                type:'POST',
                url:"{{ route('ajax.kegiatan') }}",
                data:{rekening:subrekening},
                success:function(data){
                    var suburaian = $("#sub_uraian").val(data.kegiatan);
            }
        });
    });
    </script>
@endpush