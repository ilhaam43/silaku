@extends('layouts.bendahara_pembantu')

@section('bendahara-pembantu')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-center mb-4">
            <h1 class="h3 mb-0 text-gray-800">Ubah data SPJ LS</h1>
        </div>

                <form action="{{ route('spj-ls.update', ['id' => $spj_panjar->id]) }}" method="post">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-body">
                            <input type="hidden" id="npd_id" name="npd_id" value="{{ $spj_panjar->npd_id }}" >

                            <!-- <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="rek" class="col-form-label">Jenis rekening</label>
                                </div>
                                <div class="col-sm-8">
                                    <select type="text" class="form-control" name="rek_id" id="rek_id">
                                        <option value="">- Pilih jenis rekening -</option>
                                        <option value="0">Kegiatan </option>
                                        <option value="1">Sub Kegiatan </option>
                                    </select>
                                </div>
                            </div> -->

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="entry_id" class="col-form-label">Tanggal</label>
                                </div>
                                <div class="col-sm-8">
                                    <input class="form-control" type="date" name="tgl_entry_ls" id="tgl_entry_ls" value="{{ $spj_panjar->tgl_entry_ls }}">
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="rek" class="col-form-label">No. Rekening</label>
                                </div>
                                <div class="col-sm-8">
                                <input type="hidden" id="rek_id" name="rek_id" value="{{ $spj_panjar->rekening->id }}"/>
                                    <select type="text" class="form-control" name="sub_rek_id" id="sub_rek_id">
                                        <option value="">- Pilih nomor rekening -</option>
                                            @foreach ($npd_detail as $item)  
                                                <option value="{{ $item-> sub_rek_id }}" data-rek-id="{{$item->rek_id}}"
                                                    {{ $item->rek_id == $spj_panjar->rekening->id ? 'selected' : '' }}
                                                    >{{ $item-> kode_rekening }} - {{ $item-> nama_kegiatan }}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="panjar_jml" class="col-form-label">Jumlah LS</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="jml_panjar" name="jml_panjar_ls" class="form-control" value="{{ $spj_panjar->jml_panjar_ls }}">
                                </div>
                            </div>  

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="spj_panjar" class="col-form-label">SPJ LS</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="spj_panjar" name="spj_panjar_ls" class="form-control" value="{{ $spj_panjar->spj_panjar_ls }}">
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-4">
                                    <label for="spj_panjar" class="col-form-label">Sisa</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="sisa" name="sisa_ls" class="form-control" value="{{ $spj_panjar->sisa_ls }}">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
        @endsection
        @push('js')
        <script>
            $('#harga_satuan').change(function(){
    console.log($('#harga_satuan').val());
    var harga = $('#volume').val() * $('#harga_satuan').val();
    $('#total').val(harga);
});
</script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#sub_rek_id").change(function() {
        var selected = $('#sub_rek_id').find(":selected");
        var rekening = selected.text();
        var rek_id = selected.data("rek-id");
        $("#rek_id").val(rek_id);

        $.ajax({
                type:'POST',
                url:"{{ route('ajax.kegiatan') }}",
                data:{rekening:rekening},
                success:function(data){
                    var uraian = $("#uraian").val(data.kegiatan);
            }
        });
    });

    // $("#sub_rek").change(function() {
    //     var subrekening = $('#sub_rek').find(":selected").text();

    //     $.ajax({
    //             type:'POST',
    //             url:"{{ route('ajax.kegiatan') }}",
    //             data:{rekening:subrekening},
    //             success:function(data){
    //                 var suburaian = $("#uraian_sub").val(data.kegiatan);
    //         }
    //     });
    </script>
@endpush