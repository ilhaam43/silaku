@extends('layouts.bendahara_pembantu')

@section('bendahara-pembantu')
<div class="container-fluid">
    <div class="card-header">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-center mb-4">
            <h1 class="h4 mb-0 text-gray-800">Daftar Nomor Surat Pengajuan Dana</h1>
        </div>

        <!-- Modal tambah Kop Surat -->
        <div class="modal fade" id="tambahModal" tabindex="4" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="{{ route('kopsurat.store') }}" method="post" class="form-horizontal">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header align-items-center justify-content-center">
                            <h4 class="modal-title" id="exampleModalLabel">Tambah Nomor Surat</h4>
                        </div>
                        <div class="form-group text-left mx-4 px-4">
                            <label for="nomor_kop">Masukan nomor Kop Surat</label>
                            <input class="form-control" type="text" name="nomor_kop" id="" required>
                        </div>
                        <div class="form-group text-left mx-4 px-4">
                            <label for="nomor_dpa">Masukan nomor DPA</label>
                            <input class="form-control" type="text" name="nomor_dpa" id="" required>
                        </div>
                        <div class="form-group text-left mx-4 px-4">
                            <label for="tgl_dpa">Masukan Tanggal DPA</label>
                            <input class="form-control" type="date" name="tgl_dpa" id="" required>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


            <!-- Tables Kop Surat-->
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <button type="button" class="btn btn-primary user__add-btn" data-toggle="modal" data-target="#tambahModal">
                        <i class="fas fa-plus"></i> Tambah Nomor Surat
                    </button>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" >
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <!-- <th>Tanggal</th> -->
                                    <th>Nomor Surat</th>
                                    <th>No DPA</th>
                                    <th>Tanggal DPA</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($kop_surat as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <!-- <td>{{ Carbon\Carbon::parse($item->created_at)->translatedFormat('d F Y') }}</td> -->
                                <td>{{ $item->nomor_kop }}</td>
                                <td>{{ $item->nomor_dpa }}</td>
                                <td>{{ $item->tgl_dpa }}</td>
                                <td>{{ $item->disetujui ? 'Disetujui' : 'Belum disetujui' }}</td>
                                <td>
                                    <div class="mb-2">
                                    <a href="{{ route('npd.input',$item->id) }}" class="btn btn-outline-primary">
                                        <i class="fas fa-fw fa-plus"></i>
                                    </a>
                                    </div>
                                    <div class="mb-2">
                                        <button class="btn btn-outline-danger user__delete-btn" data-url_delete="{{ route('kopsurat.destroy', $item->id) }}"><i class="fas fa-fw fa-trash"></i></button>
                                    </div>
                                    <div class="mb-2">
                                    <a href="{{ route('npd.file', $item->id) }}" target="_blank" class="btn btn-outline-info">
                                        <i class="fas fa-fw fa-file-alt"></i>
                                    </a>
                                    </div>
                                    <div>
                                        <button class="btn btn-outline-primary user__edit-btn" data-toggle="modal" data-target="#UbahModal{{ $item->id }}"><i class="fas fa-fw fa-edit"></i></button>
                                    </div>
                                </td>
                            </tr>

        <!-- Modal ubah Kop Surat -->
        <div class="modal fade" id="UbahModal{{ $item->id }}" tabindex="4" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="{{ route('kopsurat.update', ['id' => $item->id]) }}" method="post" class="form-horizontal">
                    @csrf
                @method('PATCH')
                    <div class="modal-content">
                        <div class="modal-header align-items-center justify-content-center">
                            <h4 class="modal-title" id="exampleModalLabel">Ubah Nomor Surat</h4>
                        </div>
                        <div class="form-group text-left mx-4 px-4">
                            <label for="nomor_kop">Masukan nomor Kop Surat</label>
                            <input class="form-control" type="text" name="nomor_kop" id="" value="{{ $item->nomor_kop }}">
                        </div>
                        <div class="form-group text-left mx-4 px-4">
                            <label for="nomor_dpa">Masukan nomor DPA</label>
                            <input class="form-control" type="text" name="nomor_dpa" id="" value="{{ $item->nomor_dpa }}">
                        </div>
                        <div class="form-group text-left mx-4 px-4">
                            <label for="tgl_dpa">Masukan Tanggal DPA</label>
                            <input class="form-control" type="date" name="tgl_dpa" id="" value="{{ $item->tgl_dpa }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


    </div>
</div>
@endsection
@push('js')
    <script src="{{ asset('js/pengajuan.js') }}"></script>
@endpush
