@extends('layouts.bendahara_pembantu')

@section('bendahara-pembantu')
    <div class="container-fluid">
        <div class="card-header">
        </div>
    </div>

    <div class="container-fluid">
    <div class="card-header">
        <button type="button" data-toggle="modal" data-target="#exampleModal">Tambah Rekening</button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="npd_rekening" method="post">
                    @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Tambah Rekening</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                            </div>
                                <div class="modal-body">
                                    <label for="nomor_kop">Masukan nomor rekening kegiatan</label>
                                    <input type="text" name="no_rekening_kegiatan" id=""><br>
                                    <label for="nomor_kop">Masukan nomor rekening sub kegiatan</label><br>
                                    <input type="text" name="no_rekening_sub_kegiatan" id="">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                    </form>
            </div>
        </div>
            <table class="table table-bordered" id="dataTable" >
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nomor Rekening Kegiatan</th>
                        <th>Nomor Rekening Sub Kegiatan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($datas as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->no_rekening_kegiatan }}</td>
                    <td>{{ $item->no_rekening_sub_kegiatan }}</td>
                    <td>
                        <button type="button" class="btn btn-primary"><i class="fas fa-download"></i></button>
                        <button type="button" class="btn btn-info"><i class="fas fa-file-alt"></i></button>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection 