<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Berkas Nota Pengajuan Dana</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Custom fonts for this template-->
    {{-- <link href={{ asset('/vendor/fontawesome-free/css/all.min.css') }} rel="stylesheet" type="text/css">
    <link
        href='https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'
        rel="stylesheet"> --}}

    <!-- Custom styles for this template-->
    <link href={{ asset('/css/sb-admin-2.min.css') }} rel="stylesheet">

    <style>

        * {
            font-family: 'Roboto', Times, serif;
            font-size: 12px;
            color: black;
        }
    </style>

    <!-- Custom styles for this page -->
    <link
      href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}"
      rel="stylesheet"
    />


</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col mt-4">
                <h1 class="text-center"><b>NOTA PENCAIRAN DANA (NPD)</b><br>
                </h1>
                <p class="text-center">
                No. {{ $data->nomor_kop }}
                </p>

            </div>
        </div>
        {{-- <hr color="#000000" style="margin-top: -10px">
        <hr color="#000000" style="height: 5px; margin-top: -10px;"> --}}

        <div class="mt-3">
            <table class="table table-bordered"  id="dataTable" width="100%" cellspacing="0">
                <tbody>
                    <tr>
                        <td> BENDAHARA PENGELURAN SETDA </td>
                    </tr>
                    <tr>
                        <td> SKPD: SEKRETARIAT DAERAH KAB. PURWOREJO </td>
                    </tr>
                    <tr>
                        <td> Supaya mencairkan dana kepada: </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="mt-3">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <tbody>
                    @php ($no = 1)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td> Bendahara Pengeluaran Pembantu Bagian Umum </td>
                    </tr>
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td> Nomor DPA {{ $data->nomor_dpa }} Tanggal {{ \Carbon\Carbon::parse($data->tgl_dpa)->translatedFormat('j F Y') }} </td>
                    </tr>
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td> Tahun Anggaran 2022 </td>
                    </tr>
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td> Jumlah dana yang diminta </td>
                    </tr>

                </tbody>
            </table>
        </div>

        <div class="mt-3">
            <h1 class="text-center"><b>Pembebanan pada kode rekening</b><br>
            </h1>
        </div>

        <div class="mt-3">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead class="text-center" >
                    <tr>
                      <th rowspan="2" scope="col">No</th>
                      <th rowspan="2" scope="col">Kode Rek </th>
                      <th scope="col">Nama Kegiatan dan Nama Rincian Obyek</th>
                      <th rowspan="2" scope="col">Anggaran Murni</th>
                      <th rowspan="2" scope="col">Akumulasi pencairan Sebelumnya</th>
                      <th rowspan="2" scope="col">Pencairan<br>Pencairan saat ini</th>
                      <th rowspan="2" scope="col">Sisa</th>
                      <th rowspan="2" scope="col">Nama PPTK</th>
                      <th rowspan="2" scope="col"></th>
                    </tr>
                    <tr>
                        <th scope="col">Belanja</th>
                    </tr>
                  </thead>
                <tbody>
                    @php ($np = 1)
                    @php ($lv0 = 0)
                    @php ($lv1 = 0)
                    @php ($lv2 = 0)
                    @php ($lv3 = 0)
                    @php ($lv4 = 0)
                    @foreach ($file as $item)
                    {{-- LEVEL 0 --}}
                    @if ($item->rekening->induk->induk->induk->induk->id != $lv0)
                    @php ($lv0 = $item->rekening->induk->induk->induk->induk->id)
                    <tr>
                        <td><strong>{{ $np++ }}</strong></td>
                        <td><strong>{{ $item->rekening->induk->induk->induk->induk->kode_rekening }}</strong></td>
                        <td><strong>{{ $item->rekening->induk->induk->induk->induk->nama_kegiatan }}</strong></td>
                        <td style="text-align: right"><strong>@uang($subtotal[$lv0])</strong></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endif
                    {{-- LEVEL 1 --}}
                    @if ($item->rekening->induk->induk->induk->id != $lv1)
                    @php ($lv1 = $item->rekening->induk->induk->induk->id)
                    <tr>
                        <td><strong>{{ $np++ }}</strong></td>
                        <td><strong>{{ $item->rekening->induk->induk->induk->kode_rekening }}</strong></td>
                        <td><strong>{{ $item->rekening->induk->induk->induk->nama_kegiatan }}</strong></td>
                        <td style="text-align: right"><strong>@uang($subtotal[$lv1])</strong></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endif
                    {{-- LEVEL 2 --}}
                    @if ($item->rekening->induk->induk->id != $lv2)
                    @php ($lv2 = $item->rekening->induk->induk->id)
                    <tr>
                        <td><strong>{{ $np++ }}</strong></td>
                        <td><strong>{{ $item->rekening->induk->induk->kode_rekening }}</strong></td>
                        <td><strong>{{ $item->rekening->induk->induk->nama_kegiatan }}</strong></td>
                        <td class="text-right"><strong>@uang($subtotal[$lv2])</strong></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endif
                    {{-- LEVEL 3 --}}
                    @if ($item->rekening->induk->id != $lv3)
                    @php ($lv3 = $item->rekening->induk->id)
                    <tr>
                        <td><strong>{{ $np++ }}</strong></td>
                        <td><strong>{{ $item->rekening->induk->kode_rekening }}</strong></td>
                        <td><strong>{{ $item->rekening->induk->nama_kegiatan }}</strong></td>
                        <td style="text-align: right"><strong>@uang($subtotal[$lv3])</strong></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endif
                    {{-- LEVEL 4 --}}
                    @if ($item->rekening->id != $lv4)
                    @php ($lv4 = $item->rekening->id)
                    <tr>
                        <td><strong>{{ $np++ }}</strong></td>
                        <td><strong>{{ $item->rekening->kode_rekening }}</strong></td>
                        <td><strong>{{ $item->rekening->nama_kegiatan }}</strong></td>
                        <td style="text-align: right"><strong>@uang($subtotal[$lv4])</strong></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endif
                    {{-- <option value="{{ $item-> id }}">{{ $item-> nama_kegiatan }}</option> --}}
                    <tr>
                        <td>{{ $np++ }}</td>
                        <td>{{ $item->subrekening->kode_rekening }}</td>
                        <td>{{ $item->subrekening->nama_kegiatan }}</td>
                        <td style="text-align: right">@uang($item->anggaran_murni)</td>
                        <td style="text-align: right">@uang($item->pencairan_sebelumnya)</td>
                        <td style="text-align: right">@uang($item->pencairan_saat_ini)</td>
                        <td style="text-align: right">@uang($item->sisa)</td>
                        <td>{{ $item->pptk }}</td>
                        <td></td>
                    </tr>
                    @endforeach
    </div>

        <!-- Bootstrap core JavaScript-->
    <script src={{ asset('/vendor/jquery/jquery.min.js') }}></script>
    <script src={{ asset('/vendor/bootstrap/js/bootstrap.bundle.min.js') }}></script>

    <!-- Core plugin JavaScript-->
    <script src={{ asset('/vendor/jquery-easing/jquery.easing.min.js') }}></script>

    <!-- Custom scripts for all pages-->
    <script src={{ asset('/js/sb-admin-2.min.js') }}></script>

    <!-- Page level plugins -->
    <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('js/demo/datatables-demo.js') }}"></script>

    </body>

</html>
