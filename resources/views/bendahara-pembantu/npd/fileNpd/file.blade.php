<title>{{'NPD No. ' . $data->nomor_kop}}</title>
<style>
    html {
        margin: 40px;
    }

    * {
        box-sizing: border-box;
        margin: 0;
        padding: 0;
        font-family: calibri, Helvetica, sans-serif;
        font-size: 16px;
    }

    .table__header {
        padding: 15px;
        text-align: center;
        border: 1px solid black;
        position: relative;
    }

    .table__header h2 {
        font-weight: normal;
        font-size: 20px;
    }

    .table__sub-header {
        border: 1px solid black;
        padding: 15px;
    }

    .table__dana-tujuan {
        padding: 0 15px;
        border: 1px solid black;
    }

    .clearfix::after {
        content: "";
        display: table;
        clear: both;
    }

    .table__dana-tujuan-num {
        float: left;
        border-right: 1px solid black;
        width: 2%;
    }

    .table__dana-tujuan-text {
        float: left;
        padding-left: 20px;
    }

    .table__dana-tujuan-jumlah-dana {
        float: right;
        font-weight: bold;
    }

    .table__sub-heading {
        text-align: center;
        border: 1px solid black;
    }

    .table__content {
        width: 100%;
    }

    .table__content tr,
    .table__content th,
    .table__content td {
        border: 1px solid black;
    }

    .table__content {
        border: 2px solid black;
    }

    .table__content tr {
        height: 30px;
    }

    .table__content tr.data-bold {
        font-weight: bold;
    }

    .table__content tr.data-highlighted {
        background: lightgrey;
    }

    .table__content td {
        padding: 3px;
    }

    tr.table__content-bottom,
    .table__content-bottom td {
        border: 0;
    }

    .no {
        width: 2%;
    }

    .rek {
        width: 10%;
    }

    .kegiatan {
        width: 18%;
    }

    .perubahan {
        width: 10%;
        text-align: right;
    }

    .pergeseran {
        width: 10%;
        text-align: right;
    }

    .anggaran {
        width: 10%;
        text-align: right;
    }

    .akumulasi {
        width: 10%;
        text-align: right;
    }

    .pencairan {
        width: 10%;
        text-align: right;
    }

    .sisa {
        width: 10%;
        text-align: right;
    }

    .pptk {
        width: 20%;
        text-align: right;
    }

    .table__footer {
        padding-top: 30px;
        width: 100%;
    }

    .table__tanggal {
        width: 100%;
        text-align: center
    }

    .table__sign-container {
        text-align: center;
        clear: right;
        margin-top: 30px;
    }

    .table__sign-left {
        float: left;
        width: 50%;
    }

    .table__sign-right {
        float: right;
        width: 50%;
    }

    .table__sign {
        height: 80px;
    }

    .upper { text-transform: uppercase; }

</style>

<link rel="stylesheet" href="styles.css" />

<div class="table__container">
    <header class="table__header">
        <h2>NOTA PENCAIRAN DANA(NPD)</h2>
        <p>No. {{$data->nomor_kop}}</p>
    </header>
    <div class="table__sub-header">
        <p>BENDAHARA PENGELUARAN SETDA</p>
        <p>SKPD. SEKRETARIAT DAERAH KAB. PURWOREJO</p>
        <p>Supaya mencairkan dana kepada:</p>
    </div>
    <div class="table__dana-tujuan clearfix">
        <div class="table__dana-tujuan-num">
            <div>1</div>
            <div>2</div>
            <div>3</div>
            <div>4</div>
            <div>&nbsp;</div>
        </div>
        <div class="table__dana-tujuan-text">
            <div>Bendahara Pengeluaran Pembantu Bagian {{ $data->bidang }}</div>
            <div>
            Nomor DPA {{ $data->nomor_dpa }} Tanggal {{ \Carbon\Carbon::parse($data->tgl_dpa)->translatedFormat('j F Y') }}
            </div>
            <div>Tahun Anggaran {{ \Carbon\Carbon::parse($data->tgl_dpa)->translatedFormat('Y') }}</div>
            <div>
                Jumlah dana yang diminta<span
                    class="table__dana-tujuan-jumlah-dana"
                    >Rp. @uang($total['pencairan_saat_ini'])</span
                >
            </div>
        </div>
    </div>
    <div class="table__sub-heading">Pembebanan pada kode rekening</div>
    <table class="table__content" style="border-collapse: collapse">
        @if($repeatHeader)
        <thead>
        @endif
            <tr>
                <th>No</th>
                <th>Kode Rek</th>
                <th>Nama Kegiatan dan Nama Rincian Objek Belanja</th>
                <th>Anggaran Murni</th>
                <th>Pergeseran</th>
                <th>Perubahan</th>
                <th>Akumulasi pencairan sebelumnya</th>
                <th>Pencairan saat ini</th>
                <th>Sisa</th>
                <th>Nama PPTK</th>
            </tr>
        @if($repeatHeader)
        </thead>
        @endif
        @php ($lv0 = $lv1 = $lv2 = $lv3 = $lv4 = 0)
        @foreach ($file as $item)
        @php($induk0 = $item->rekening->induk->induk->induk->induk)
        @php($induk1 = $item->rekening->induk->induk->induk)
        @php($induk2 = $item->rekening->induk->induk)
        @php($induk3 = $item->rekening->induk)
        {{-- LEVEL 0 --}}
        @if ($induk0->id != $lv0)
        @php ($lv0 = $induk0->id)
        <tr class="data-bold">
            <td class="no"></td>
            <td class="rek">{{$induk0->kode_rekening}}</td>
            <td class="kegiatan">{{$induk0->nama_kegiatan}}</td>
            <td class="anggaran">@uang($subtotal['anggaran_murni'][$lv0])</td>
            <td class="pergeseran">@uang($subtotal['pergeseran'][$lv0])</td>
            <td class="perubahan">@uang($subtotal['perubahan'][$lv0])</td>
            <td class="akumulasi"></td>
            <td class="pencairan"></td>
            <td class="sisa"></td>
            <td class="pptk"></td>
        </tr>
        @endif
        {{-- LEVEL 1 --}}
        @if ($induk1->id != $lv1)
        @php ($lv1 = $induk1->id)
        <tr class="data-bold">
            <td class="no"></td>
            <td class="rek">{{$induk1->kode_rekening}}</td>
            <td class="kegiatan">{{$induk1->nama_kegiatan}}</td>
            <td class="anggaran">@uang($subtotal['anggaran_murni'][$lv1])</td>
            <td class="pergeseran">@uang($subtotal['pergeseran'][$lv1])</td>
            <td class="perubahan">@uang($subtotal['perubahan'][$lv1])</td>
            <td class="akumulasi"></td>
            <td class="pencairan"></td>
            <td class="sisa"></td>
            <td class="pptk"></td>
        </tr>
        @endif
        {{-- LEVEL 2 --}}
        @if ($induk2->id != $lv2)
        @php ($lv2 = $induk2->id)
        <tr class="data-bold">
            <td class="no"></td>
            <td class="rek">{{$induk2->kode_rekening}}</td>
            <td class="kegiatan">{{$induk2->nama_kegiatan}}</td>
            <td class="anggaran">@uang($subtotal['anggaran_murni'][$lv2])</td>
            <td class="pergeseran">@uang($subtotal['pergeseran'][$lv2])</td>
            <td class="perubahan">@uang($subtotal['perubahan'][$lv2])</td>
            <td class="akumulasi"></td>
            <td class="pencairan"></td>
            <td class="sisa"></td>
            <td class="pptk"></td>
        </tr>
        @endif
        {{-- LEVEL 3 --}}
        @if ($induk3->id != $lv3)
        @php ($lv3 = $induk3->id)
        <tr class="data-bold">
            <td class="no"></td>
            <td class="rek">{{$induk3->kode_rekening}}</td>
            <td class="kegiatan">{{$induk3->nama_kegiatan}}</td>
            <td class="anggaran">@uang($subtotal['anggaran_murni'][$lv3])</td>
            <td class="pergeseran">@uang($subtotal['pergeseran'][$lv3])</td>
            <td class="perubahan">@uang($subtotal['perubahan'][$lv3])</td>
            <td class="akumulasi"></td>
            <td class="pencairan"></td>
            <td class="sisa">@uang($subtotal['sisa'][$lv3])</td>
            <td class="pptk"></td>
        </tr>
        @endif
        {{-- LEVEL 4 --}}
        @if ($item->rekening->id != $lv4)
        @php ($lv4 = $item->rekening->id)
        <tr class="data-bold">
            <td class="no"></td>
            <td class="rek">{{$item->rekening->kode_rekening}}</td>
            <td class="kegiatan">{{$item->rekening->nama_kegiatan}}</td>
            <td class="anggaran">@uang($subtotal['anggaran_murni'][$lv4])</td>
            <td class="pergeseran">@uang($subtotal['pergeseran'][$lv4])</td>
            <td class="perubahan">@uang($subtotal['perubahan'][$lv4])</td>
            <td class="akumulasi">@uang($subtotal['pencairan_sebelumnya'][$lv4])</td>
            <td class="pencairan">@uang($subtotal['pencairan_saat_ini'][$lv4])</td>
            <td class="sisa">@uang($subtotal['sisa'][$lv4])</td>
            <td class="pptk"></td>
        </tr>
        @endif
        <tr class="">
            <td class="no"></td>
            <td class="rek">{{$item->subrekening->kode_rekening}}</td>
            <td class="kegiatan">{{$item->subrekening->nama_kegiatan}}</td>
            <td class="anggaran">@uang($item->anggaran_murni)</td>
            <td class="pergeseran">@uang($item->pergeseran)</td>
            <td class="perubahan">@uang($item->perubahan)</td>
            <td class="akumulasi">@uang($item->pencairan_sebelumnya)</td>
            <td class="pencairan">@uang($item->pencairan_saat_ini)</td>
            <td class="sisa">@uang($item->sisa)</td>
            <td class="pptk">{{$item->pptk}}</td>
        </tr>
        @endforeach

        <tr class="data-bold">
            <td class="no"></td>
            <td class="rek"></td>
            <td class="kegiatan"></td>
            <td class="anggaran">@uang($total['anggaran_murni'])</td>
            <td class="pergeseran">@uang($total['pergeseran'])</td>
            <td class="perubahan">@uang($total['perubahan'])</td>
            <td class="akumulasi"></td>
            <td class="pencairan"></td>
            <td class="sisa"></td>
            <td class="pptk"></td>
        </tr>

        <tr class="">
            <td class="" colspan="6" style="text-align: center;">JUMLAH</td>
            <td class="akumulasi">@uang($total['pencairan_sebelumnya'])</td>
            <td class="pencairan">@uang($total['pencairan_saat_ini'])</td>
            <td class="sisa"></td>
            <td class="pptk"></td>
        </tr>
        <tr class="">
            <td class="" colspan="10">JUMLAH</td>
        </tr>
        <tr class="">
            <td class="" colspan="10">Potongan-potongan:</td>
        </tr>
        <tr class="">
            <td class="" colspan="2">PPN</td>
            <td class="" >Rp.</td>
            <td class="" colspan="7"></td>
        </tr>
        <tr class="">
            <td class="" colspan="2">PPh 21/22/23</td>
            <td class="" >Rp.</td>
            <td class="" colspan="7"></td>
        </tr>
        <tr class="table__content-bottom">
            <td class="" colspan="2">Jumlah yang diminta</td>
            <td class="" >Rp.</td>
            <td class="" colspan="7"></td>
        </tr>
        <tr class="table__content-bottom">
            <td class="" colspan="2">Potongan-potongan</td>
            <td class="" >Rp.</td>
            <td class="" colspan="7"></td>
        </tr>
        <tr class="table__content-bottom">
            <td class="" colspan="2">Jumlah yang dibayar</td>
            <td class="" >Rp.</td>
            <td class="" colspan="7"></td>
        </tr>
    </table>
    <table class="table__footer">
        <tr class="table__tanggal">
            <td></td>
            <td>
                Purworejo, {{ Carbon\Carbon::now()->translatedFormat('d F Y') }}
            </td>
        </tr>
        <tr class="table__sign-container clearfix">
            <td class="table__sign-left">
                <h4 class="upper">KEPALA BAGIAN {{ strtoupper($data->bidang) }} SETDA PURWOREJO</h4>
                <h4 class="">SELAKU KUASA PENGGUNA ANGGARAN</h4>
                <div class="table__sign"></div>
                <!-- <p class="">{{ strtoupper($data->name) }}</p>
                <p class="">{{ strtoupper($data->nip) }}</p> -->
                <p class="">{{$ttd->nama_kepala}}</p>
                <p class="">NIP. {{$ttd->nip_kepala}}</p>
            </td>
            <td class="table__sign-right">
                <h4 class="">BENDAHARA PENGELUARAN PEMBANTU</h4>
                <h4 class="">BAGIAN {{ $data->bidang }} SETDA</h4>
                <div class="table__sign"></div>
                <!-- <p class="">{{ strtoupper($data->name) }}</p>
                <p class="">{{ strtoupper($data->nip) }}</p> -->
                <p class="">{{$ttd->nama_bendahara}}</p>
                <p class="">NIP. {{$ttd->nip_bendahara}}</p>
            </td>
        </tr>

    </table>
</div>
