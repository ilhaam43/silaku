@extends('layouts.bendahara_pembantu')

@section('bendahara-pembantu')
    <div class="container-fluid">
          <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-center">
                <h1 class="h3 mb-0 text-gray-800">Pengajuan Dana </h1>
            </div>
            <div class="d-sm-flex align-items-center justify-content-center mb-4">
                <h6 class="h6 mb-0 text-gray-800">No. {{ $data->nomor_kop}} </h6>
            </div>


        <!-- Tables-->
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            {{-- <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0"> --}}
            <div class="card-body">
                <div class="table-responsive">
                    <table
                        class="table table-bordered table-striped text-center"
                        id="dataTable"
                        width="100%"
                        cellspacing="0"
                    >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Uraian Kegiatan</th>
                                <th>Sub Kegiatan</th>
                                <th>Anggaran Murni</th>
                                <th>Anggaran Pergeseran</th>
                                <th>Anggaran Perubahan</th>
                                <th>Pencairan Sebelumnya</th>
                                <th>Pencairan Saat Ini</th>
                                <th>Sisa</th>
                                <th>PPTK</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($npd as $item)
                            <tr>
                                <td>{{ $loop-> iteration }}</td>
                                <td>{{ $item-> rekening->nama_kegiatan}}</td>
                                <td>{{ $item-> subrekening->nama_kegiatan}}</td>
                                <td>@uang($item-> anggaran_murni)</td>
                                @if (is_null($item->pergeseran))
                                <td></td>
                                @else
                                <td>@uang($item->pergeseran)</td>
                                @endif
                                @if (is_null($item->perubahan))
                                <td></td>
                                @else
                                <td>@uang($item->perubahan)</td>
                                @endif
                                <td>@uang($item-> pencairan_sebelumnya)</td>
                                <td>@uang($item-> pencairan_saat_ini)</td>
                                <td>@uang($item-> sisa)</td>
                                <td>{{ $item-> pptk }}</td>
                                <td>
                                    <div class="mb-2">
                                        <button class="btn btn-outline-danger user__delete-btn" data-url_delete="{{ route('npd.destroy', ['id' => $item->kopsurat->id, 'id2' => $item->id]) }}"><i class="fas fa-fw fa-trash"></i></button>
                                    </div>
                                    <div>
                                        <a href="{{ $item->id }}" class="btn btn-outline-info user__edit-btn"><i class="fas fa-fw fa-edit"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="card_body">
            <form action="{{ route('npd.update', ['id' => $data->id, 'id2' => $edit->id]) }}" method="post">
                @csrf
                <table border="0" align="center" id="dynamicTable">
                    <tr>
                        <div class="d-sm-flex align-items-center justify-content-center mb-4">
                            <h6 class="h6 mb-0 text-gray-800">Input Pengajuan Dana Kegiatan Baru </h6>
                        </div>
                    </tr>
                    <tr>
                        <input type="hidden" class="form-control" id="kop_id" name="kop_id" value={{ $kop_id }}>
                        {{-- @foreach ($npd as $item)
                            <input type="hidden" class="form-control" id="kop_id" name="kop_id" value={{ $item->kop_id }}>
                        @endforeach --}}
                        <td>
                            <label for="lname">Uraian Kegiatan</label>
                        </td>
                        <td>
                            <select class="form-control" name="rek_id" id="rek_id" disabled>
                                <option value="{{$edit->rekening->id}}" selected>{{$edit->rekening->nama_kegiatan}}</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lname">Sub Kegiatan</label>
                        </td>
                        <td>
                            <select class="form-control" name="sub_rek_id" id="sub_rek_id" disabled>
                                <option value="{{$edit->subrekening->id}}" selected>{{$edit->subrekening->nama_kegiatan}}</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lname">Anggaran Murni</label>
                        </td>
                        <td>
                            <input class="form-control" type="text" id="" name="anggaran_murni" value="{{$edit->anggaran_murni}}">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lname">Anggaran Pergeseran</label>
                        </td>
                        <td>
                            <input class="form-control" type="text" id="" name="pergeseran" value="{{$edit->pergeseran}}">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lname">Anggaran Perubahan</label>
                        </td>
                        <td>
                            <input class="form-control" type="text" id="" name="perubahan" value="{{$edit->perubahan}}">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lname">Akumulasi Pencairan Sebelumnya</label>
                        </td>
                        <td>
                            <input class="form-control" type="text" id="" name="pencairan_sebelumnya" value="{{$edit->pencairan_sebelumnya}}">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lname">Pencairan Saat ini</label>
                        </td>
                        <td>
                            <input class="form-control" type="text" id="" name="pencairan_saat_ini" value="{{$edit->pencairan_saat_ini}}">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lname">Sisa</label>
                        </td>
                        <td>
                            <input class="form-control" type="text" id="" name="sisa" value="{{$edit->sisa}}">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lname">Nama PPTK</label>
                        </td>
                        <td>
                            <input class="form-control" type="text" id="" name="pptk" value="{{$edit->pptk}}">
                        </td>
                    </tr>
                    {{-- <tr>
                        <td colspan="2" >
                            <button class="btn btn-primary float-right ml-2" type="submit">Simpan
                            <button class="btn btn-secondary float-right">Batal</button>
                        </td>
                    </tr> --}}
                </table>
                <div class="container mt-2">
                    <button class="btn btn-primary float-right ml-2" type="submit">Simpan</button>
                    <a class="btn btn-secondary float-right" href="{{ route('npd.input', ['id' => $edit->kopsurat->id]) }}">Batal</a>
                </div>
            </form>
        </div>
    </div>
    <script src="{{ asset('js/pengajuan.js') }}"></script>
@endsection
