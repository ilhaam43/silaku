@extends('layouts.bendahara_pembantu')

@section('bendahara-pembantu')
<div class="container-fluid">
    <div class="card-header">
        <input type="date"> - <input type="date"><button type="button" data-toggle="modal" data-target="#exampleModal">Cetak</button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Pilih Rentang Bulan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
                    <div class="modal-body">
                        <input type="date" name="" id=""> S.D <input type="date" name="" id="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary">Cetak</button>
                    </div>
                </div>
            </div>
        </div>
            <table class="table table-bordered" id="dataTable" >
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>12-02-2022</td>
                        <td>Diterima</td>
                        <td>
                            <button type="button" class="btn btn-primary"><i class="fas fa-download"></i></button>
                            <button type="button" class="btn btn-info"><i class="fas fa-file-alt"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection