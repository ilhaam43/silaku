<style>
    html {
        margin: 40px;
    }

    * {
        box-sizing: border-box;
        margin: 0;
        padding: 0;
        font-family: calibri, Helvetica, sans-serif;
    }

    .table__header {
        padding: 15px;
        display: flex;
        align-items: center;
        justify-content: center;
        border: 1px solid black;
        position: relative;
    }

    .table__logo {
        position: absolute;
        left: 15px;
    }

    .table__title {
        text-align: center;
    }

    .table__title * {
        margin-bottom: 10px;
    }

    .table__desc {
        padding: 15px;
        border: 1px solid black;
    }

    .table__desc-text tr td:nth-child(1) {
        width: 200px;
        font-weight: bold;
    }

    .table__desc-text tr td:nth-child(2) {
        width: 20px;
    }

    .table__desc-text tr td:nth-child(3) {
        width: 120px;
    }

    .table__content tr,
    .table__content th,
    .table__content td {
        border: 1px solid black;
    }

    .table__content {
        width: 100%;
        border: 2px solid black;
    }

    .table__content thead {
        border: 2px solid black;
    }

    .table__content th,
    .table__content td {
        padding: 5px;
    }

    .kode-rekening {
        text-align: left;
        width: 12%;
    }

    .uraian {
        text-align: left;
        width: 30%;
    }

    .uraian-1 span {
        padding-left: 0;
        font-weight: bold;
    }

    .uraian-2 span {
        padding-left: 30px;
        font-weight: bold;
    }

    .uraian-3 span {
        padding-left: 60px;
        font-weight: bold;
    }

    .uraian-4 span {
        padding-left: 90px;
    }

    .uraian-5 span {
        padding-left: 120px;
    }

    .anggaran,
    .realisasi,
    .bertambah-rp,
    .bertambah-persen {
        text-align: right;
    }

    .bertambah-rp,
    .bertambah-persen {
        width: 10%;
    }

    .anggaran,
    .realisasi {
        width: 12%;
    }

    .penjelasan {
        width: 23%;
        position: relative;
    }

    .penjelasan span, .jumlah-keseluruhan span {
        float: right;
    }

    .penjelasan .jumlah-penjelasan {
        font-weight: bold;
    }

    .table__jumlah {
        font-weight: bold;
    }

    .table__footer {
        padding-top: 30px;
        display: flex;
        justify-content: flex-end;
    }

    .table__sign-container {
        margin-right: 100px;
        text-align: center;
        float: right;
    }

    .table__sign {
        height: 80px;
    }

    .table__sign-nama {
        text-decoration: underline;
    }

</style>

<div class="table__container">
    <header class="table__header">
        <div class="table__logo">
            <img src="{{public_path('img/logo_bw.png')}}" alt="" width="80px" />
        </div>
        <div class="table__title">
            <h4>PEMERINTAH KABUPATEN PURWOREJO</h4>
            <h2>
                PENJABARAN LAPORAN REALISASI ANGGARAN PENDAPATAN DAN BELANJA
                DAERAH
            </h2>
            <p>TAHUN ANGGARAN {{$tahun}}</p>
        </div>
    </header>
    <div class="table__desc">
        <table class="table__desc-text">
            <tr>
                <td>Urusan Pemerintahan</td>
                <td>:</td>
                <td>4 . 01</td>
                <td>
                    Urusan Pemerintahan Fungsi Penunjang Administrasi
                    Pemerintahan
                </td>
            </tr>
            <tr>
                <td>Unit Organisasi</td>
                <td>:</td>
                <td>4 . 01 . 02</td>
                <td>BUPATI/WAKIL BUPATI</td>
            </tr>
        </table>
    </div>
    <table class="table__content" style="border-collapse: collapse">
        @if($repeatHeader)
        <thead>
        @endif
            <tr>
                <th rowspan="2">KODE REKENING</th>
                <th rowspan="2">URAIAN</th>
                <th colspan="2">JUMLAH (Rp)</th>
                <th colspan="2">BERTAMBAH / (BERKURANG)</th>
                <th rowspan="2">PENJELASAN</th>
            </tr>
            <tr>
                <th>ANGGGARAN SETELAH PERUBAHAN</th>
                <th>REALISASI</th>
                <th>(Rp)</th>
                <th>(%)</th>
            </tr>
            <tr>
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
                <th>6</th>
                <th>7</th>
            </tr>
        @if($repeatHeader)
        </thead>
        @endif
        @php
        $total_semua_detail = 0;
        $total_semua_anggaran = 0;
        $total_semua_realisasi = 0;
        @endphp
        <tr>
            <td class="kode-rekening">{{$default->kode_rekening}}</td>
            <td class="uraian uraian-1"><span>{{$default->nama_kegiatan}}</span></td>
            <td class="anggaran">@uang($total_anggaran)</td>
            <td class="realisasi">@uang($total_realisasi)</td>
            <td class="bertambah-rp">@uang($total_realisasi-$total_anggaran)</td>
            <td class="bertambah-persen">{{number_format($total_realisasi/$total_anggaran*100, 2, ',', '.')}}</td>
            <td class="penjelasan"></td>
        </tr>
        <tr>
            <td class="kode-rekening">{{$default->subs[0]->kode_rekening}}</td>
            <td class="uraian uraian-2"><span>{{$default->subs[0]->nama_kegiatan}}</span></td>
            <td class="anggaran">@uang($total_anggaran)</td>
            <td class="realisasi">@uang($total_realisasi)</td>
            <td class="bertambah-rp">@uang($total_realisasi-$total_anggaran)</td>
            <td class="bertambah-persen">{{number_format($total_realisasi/$total_anggaran*100, 2, ',', '.')}}</td>
            <td class="penjelasan"></td>
        </tr>
        <tr>
            <td class="kode-rekening">{{$default->subs[0]->subs[0]->kode_rekening}}</td>
            <td class="uraian uraian-3"><span>{{$default->subs[0]->subs[0]->nama_kegiatan}}</span></td>
            <td class="anggaran">@uang($total_anggaran)</td>
            <td class="realisasi">@uang($total_realisasi)</td>
            <td class="bertambah-rp">@uang($total_realisasi-$total_anggaran)</td>
            <td class="bertambah-persen">{{number_format($total_realisasi/$total_anggaran*100, 2, ',', '.')}}</td>
            <td class="penjelasan"></td>
        </tr>
        @foreach($spj as $rek_id => $items)
        <tr>
            <td class="kode-rekening">{{$items[0]->rekening->kode_rekening}}</td>
            <td class="uraian uraian-4">
                <span>{{$items[0]->rekening->nama_kegiatan}}</span>
            </td>
            <td class="anggaran">@uang($anggaran[$rek_id])</td>
            <td class="realisasi">@uang($realisasi[$rek_id])</td>
            <td class="bertambah-rp">@uang($realisasi[$rek_id]-$anggaran[$rek_id])</td>
            <td class="bertambah-persen">{{number_format($realisasi[$rek_id]/$anggaran[$rek_id]*100, 2, ',', '.')}}</td>
            <td class="penjelasan"></td>
            @php
            $total_semua_anggaran += $anggaran[$rek_id];
            $total_semua_realisasi += $realisasi[$rek_id];
            @endphp
        </tr>
        @foreach($items as $item)
        @php($totaldetail = 0)
        <tr>
            <td class="kode-rekening">{{$item->subrekening->kode_rekening}}</td>
            <td class="uraian uraian-5">
                <span>{{$item->subrekening->nama_kegiatan}}</span>
            </td>
            <td class="anggaran">@uang($item->npd->anggaran_murni)</td>
            <td class="realisasi">@uang($realisasi[$rek_id])</td>
            <td class="bertambah-rp">@uang($realisasi[$rek_id]-$anggaran[$rek_id])</td>
            <td class="bertambah-persen">{{number_format($realisasi[$rek_id]/$anggaran[$rek_id]*100, 2, ',', '.')}}</td>
            @foreach($item->spjdetail as $d)
            @foreach($d->spj_detail as $detail)
            <td class="penjelasan">{{$detail->penjelasan_barang_belanja}} {{$detail->volume}} x @uang($detail->harga_satuan)<span>@uang($detail->total)</span></td>
            @php($totaldetail += $detail->total)
        </tr>
        <tr>
            <td class="kode-rekening"></td>
            <td class="uraian uraian-5">
                <span></span>
            </td>
            <td class="anggaran"></td>
            <td class="realisasi"></td>
            <td class="bertambah-rp"></td>
            <td class="bertambah-persen"></td>
            @endforeach
            @endforeach
            <td class="penjelasan">
                Total<span class="jumlah-penjelasan">@uang($totaldetail)</span>
            </td>
        </tr>
        @php($total_semua_detail+=$totaldetail)
        @endforeach
        @endforeach
        <tr class="table__jumlah">
            <td class=""></td>
            <td class="">SURPLUS/(DEFISIT)</td>
            <td class="">@uang(0-$total_semua_anggaran)</td>
            <td class="">@uang(0-$total_semua_realisasi)</td>
            <td class="">@uang($total_semua_anggaran-$total_semua_realisasi)</td>
            <td class="">{{number_format($total_semua_realisasi/$total_semua_anggaran*100, 2, ',', '.')}}</td>
            <td class="jumlah-keseluruhan">
                <span class="">@uang($total_semua_detail)</span>
            </td>
        </tr>
    </table>
    <div class="table__footer">
        <div class="table__sign-container">
            <p class="table__sign-tanggal">Purworejo, 31 Desember {{$tahun}}</p>
            <h4 class="table__sign-jabatan">SEKRETARIAT DAERAH</h4>
            <div class="table__sign"></div>
            <p class="table__sign-nama">Drs. SAID ROMADHON</p>
            <p class="table__sign-nip">NIP. 19640122 198903 1 004</p>
        </div>
    </div>
</div>