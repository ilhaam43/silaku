@extends('layouts.admin')

@push('head')
    <!-- Custom styles for this page -->
    <link
      href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}"
      rel="stylesheet"
    />
@endpush

@section('admin')
<div class="container-fluid">
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-center mb-4">
    <h1 class="h3 mb-0 text-gray-800">Daftar Kode Rekening</h1>
  </div>

  <!-- Tables-->
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <button type="button" class="btn btn-primary user__add-btn">
        <i class="fas fa-fw fa-plus"></i> Tambah
      </button>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table
          class="table table-bordered table-striped text-center"
          id="dataTable"
          width="100%"
          cellspacing="0"
        >
          <thead>
            <tr>
              <th>Kode Rekening</th>
              <th>Jenis</th>
              <th>Uraian Kegiatan</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Kode Rekening</th>
              <th>Jenis</th>
              <th>Uraian Kegiatan</th>
              <th>Aksi</th>
            </tr>
          </tfoot>
          <tbody>
            @include('admin.partials.rekening', ['rekening' => $rekening])
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
</div>

@endsection

@push('scripts')
    <!-- Page level plugins -->
    <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('js/rekening.js')}}"></script>
@endpush
