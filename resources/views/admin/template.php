<h5>PEMERINTAH KABUPATEN PURWOREJO</h5>
<h5>BUKU KAS UMUM</h5>
<h5>BENDAHARA PENGELUARAN</h5>
<small>Periode {{Carbon\Carbon::createFromDate($bku->year, $bku->month, 1 , 'Asia/Jakarta')->format('d M Y')}} s/d
    {{Carbon\Carbon::createFromDate($bku->year, $bku->month, 30 , 'Asia/Jakarta')->format('d M Y')}}</small>


</div>
<div class="row">
    <div class="col">
        <table>

            <body>

                <tr>
                    <td width="300px">Urusan Pemerintah</td>

                    <td>{{'Urusan Wajib'}}</td>
                </tr>
                <tr>
                    <td>Bidang Pemerintah</td>

                    <td>{{'Otonomi Daerah, Pemerintahan Umum, Adm KeuDa, Perangkat Daerah, Kepegawaian'}}</td>
                </tr>
                <tr>
                    <td>Unit Organisasi</td>

                    <td>{{'Sekretariat Daerah'}}</td>
                </tr>
                <tr>
                    <td> Sub Unit Organisasi</td>

                    <td></td>
                </tr>
            </body>
        </table>

    </div>
</div>
<br>
<div class="ml-4">

    <table class="table table-bordered mb-0">
        <thead align="center">
            <tr class="table-active">
                <th scope="row" style="width : 50px">NO.</th>
                <th>TGL</th>
                <th style="width : 50px">No. <br>
                    Bukti </th>
                <th style="width : 300px">URAIAN </th>
                <th>KODE REK. </th>
                <th>PENERIMAAN</th>
                <th>PENGELUARAN</th>
                <th>SALDO</th>

            </tr>

        </thead>
        <tbody>
            <?php 
            $i=0;
            $pengeluaran = 0 ;
            $totalPengeluaran = 0;
            $totalPenerimaan = 0;
            ?>
            @foreach ($kop as $data)
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td> <strong>Pengambilan Uang</strong> </td>
                <td></td>
                <td>Rp. {{$data->pengambilanUang}}</td>
                <td></td>
                <td>Rp. {{$saldo[$i] = $data->pengambilanUang}}</td>
            </tr>
            <?php 
                $totalPenerimaan = $totalPenerimaan + $data->pengambilanUang ;
            ?>
            @foreach ($data->BKU as $BKU)
            <tr>
                <td></td>
                <td>{{$BKU->tanggal}}</td>
                <td></td>
                <td>{{$BKU->nama_kegiatan}}</td>
                <td>{{$BKU->kode_rekening}}</td>
                <td> {{'-'}}

                </td>
                <td>
                    @if ($BKU->level == 3 )
                    {{'-'}}
                    <?php 
                           $pengeluaran = 0;
                        ?>
                    @else
                    <?php
                  
                     $pengeluaran = $BKU->pengeluaran;
                     $totalPengeluaran=  $totalPengeluaran+$pengeluaran;
                    ?>
                    Rp. {{$pengeluaran}}
                    @endif


                </td>

                <td>Rp. {{  $saldo[$i] = $saldo[$i] - $pengeluaran }}</td>
            </tr>

            @foreach ($BKU->detail as $detail)
            <tr>
                <td></td>
                <td>{{$detail->tanggal}}</td>
                <td></td>
                <td>{{$detail->uraian}}</td>
                <td></td>
                <td>
                    <?php 
                     $totalPenerimaan = $totalPenerimaan +$detail->penerimaan;
                    ?>
                    {{$detail->penerimaan}} </td>
                <td> <?php
                  
                  $pengeluaran = $detail->pengeluaran;
                  $totalPengeluaran=  $totalPengeluaran+$pengeluaran;
                 ?>
                    Rp. {{$pengeluaran}}</td>
                <td>Rp. {{  $saldo[$i] = $saldo[$i] + $detail->penerimaan - $pengeluaran}}</td>
            </tr>
            @endforeach
            @endforeach

            <?php 
             $saldoAll =  $saldoAll +  $saldo[$i];
            $i++ ;
           
            ?>


            @endforeach


        </tbody>
    </table>
    <div style="page-break-after:avoid">

        <br>
        <table>
            <thead>
                <tr>
                    <th></th>
                    <th>Penerimaan</th>
                    <th>Pengeluaran</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="width :600px;">Jumlah periode ini</td>
                    <td style="width :200px;">{{ $totalPenerimaan}}</td>
                    <td style="width :200px;">{{$totalPengeluaran}}</td>
                </tr>
                <tr>
                    <td style="width :600px;">Jumlah periode lalu</td>
                    <td style="width :200px;">{{$penerimaanLalu}}</td>
                    <td style="width :200px;">{{$pengeluaranLalu}}</td>
                </tr>
                <tr>
                    <td style="width :600px;">Jumlah semua sampai periode ini</td>
                    <td style="width :200px;">{{$totalPenerimaan+$penerimaanLalu}}</td>
                    <td style="width :200px;">{{$totalPengeluaran+$pengeluaranLalu}}</td>
                </tr>
            </tbody>
        </table>
        <?php 
            $i = $i -1;
            ?>
        <p>Sisa Kas <br>
            Kas di Bendahara Pengeluaran <strong>Rp. {{$saldo[$i]}}</strong><br>
            <br>
            terdiri dari : <br>
            <ol type="a">
                <li>Tunai</li>
                <li>Saldo Bank</li>
                <li>Surat Berharga</li>
            </ol>

        </p>

    </div>
    <br>
    <div class="row" align="center">
        <div class="col">
            <p> Mengetahui,</p>
            <strong>KUASA PENGGUNA ANGGARAN</strong>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>

        </div>
        <div class="col">
            <p>Purworejo,
                ..../{{Carbon\Carbon::createFromDate($bku->year, $bku->month, 1 , 'Asia/Jakarta')->format('M/Y')}}</p>
            <strong>BENDAHARA PENGELUARAN PEMBANTU</strong>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>

        </div>
    </div>
    <div class="mt-3 mb-3 noPrint" align="center">
        <button type="button" class="btn btn-primary" onclick="window.print();return false;">Print</button>
    </div>

</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>
</body>

</html>
