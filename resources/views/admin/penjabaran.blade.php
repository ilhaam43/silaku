@extends('layouts.admin')

@push('head')
<!-- Custom styles for this page -->
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link rel="icon" href="{{asset("img/logo.png")}}">

@endpush

@section('admin')
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-center mb-4">
        <h1 class="h3 mb-0 text-gray-800">Daftar Penjabaran Realisasi</h1>
    </div>

    <!-- Tables-->
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-sm-3 col-md-2">
                    <input type="date" name="date-from" id="date-from" class="form-control">
                </div>
                <span style="padding-top: 0.4rem">-</span>
                <div class="col-sm-3 col-md-2">
                    <input type="date" name="date-from" id="date-from" class="form-control">
                </div>
                <button type="button" class="btn btn-primary user__add-btn">
                    Terapkan
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-center" id="dataTable" width="100%"
                    cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tahun</th>
                            <th>Urusan Pemerintahan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Tahun</th>
                            <th>Urusan Pemerintahan</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach($penjabaran as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->tahun}}</td>
                            <td>Urusan Pemerintahan</td>
                            <td>
                                <a href="{{route('admin.penjabaran.file', $item->tahun)}}"
                                    class="btn btn-outline-info user__edit-btn">
                                    <i class="fas fa-fw fa-download"></i>
                                </a>
                                <a href="{{route('admin.penjabaran.file', $item->tahun)}}"
                                    class="btn btn-outline-danger user__delete-btn">
                                    <i class="fas fa-fw fa-print"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>

@endsection

@push('scripts')
<!-- Page level plugins -->
<script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('js/penjabaran.js')}}"></script>
@endpush