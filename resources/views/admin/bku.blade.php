<style>
  html {
      margin: 40px;
  }

  * {
      box-sizing: border-box;
      margin: 0;
      padding: 0;
      font-family: calibri, Helvetica, sans-serif;
  }

  .bold {
      font-weight: bold;
  }

  .italic {
      font-style: italic;
  }

  .table__header {
      padding: 15px;
      display: flex;
      align-items: center;
      justify-content: center;
      position: relative;
  }

  .table__logo {
      position: absolute;
      left: 15px;
  }

  .table__title {
      text-align: center;
  }

  .table__title * {
      margin-bottom: 10px;
  }

  .table__desc {
      padding: 15px;
  }

  .table__desc-text tr td:nth-child(1) {
      width: 200px;
      font-weight: bold;
  }

  .table__desc-text tr td:nth-child(2) {
      width: 10px;
  }

  .table__desc-text tr td:nth-child(3) {
      width: 950px;
  }

  .table__content tr,
  .table__content th,
  .table__content td {
      border: 1px solid black;
  }

  .table__content {
      width: 100%;
  }

  .table__content thead {
      border: 2px solid black;
  }

  .table__content tbody {
      border: 2px solid black;
  }

  .table__content th,
  .table__content td {
      padding: 5px;
  }

  .tanggal {
      text-align: center;
  }

  .penerimaan,
  .pengeluaran,
  .saldo {
      text-align: right;
  }

  .no {
      width: 1%;
  }

  .tgl {
      width: 6%;
  }

  .bukti {
      width: 4%;
  }

  .uraian {
      width: 30%;
  }

  .rek,
  .penerimaan,
  .pengeluaran,
  .saldo {
      width: 10%;
  }

  .table__content tfoot tr,
  .table__content tfoot td {
      border: none;
      padding: 0;
  }

  .table__footer {
      padding-top: 2rem;
      width: 100%;
  }

  .table__sign-wrapper {
      display: inline-block;
      text-align: center;
      width: 49%;
  }

  .table__sign {
      height: 100px;
  }

</style>

<div class="table__container">
  <header class="table__header">
      <div class="table__logo">
            <img src="{{public_path('img/logo_bw.png')}}" alt="" width="80px" />
      </div>
      <div class="table__title">
          <h4>PEMERINTAH KABUPATEN PURWOREJO</h4>
          <h2>BUKU KAS UMUM</h2>
          <h3>BENDAHARA PENGELUARAN</h3>
          <p>Periode {{Carbon\Carbon::createFromDate($bku->year, $bku->month)->startOfMonth()->format('d M Y')}} s/d {{Carbon\Carbon::createFromDate($bku->year, $bku->month)->endOfMonth()->format('d M Y')}}</p>
       
      </div>
  </header>
  <div class="table__desc">
      <table class="table__desc-text">
          <tr>
              <td>Urusan Pemerintahan</td>
              <td>:</td>
              <td>Urusan Wajib</td>
          </tr>
          <tr>
              <td>Bidang Pemerintahan</td>
              <td>:</td>
              <td>
                  Otonomi Daerah, Pemerintahan Umum Administrasi Keuangan
                  Daerah, Perangkat Daerah, Kepegawaian, dan Persandian
              </td>
          </tr>
          <tr>
              <td>Unit Organisasi</td>
              <td>:</td>
              <td>SEKRETARIAT DAERAH</td>
          </tr>
          <tr>
              <td>Sub Unit Organisasi</td>
              <td>:</td>
              <td>{{$kop[0]->subBag}}</td>
          </tr>
      </table>
  </div>
  <table class="table__content" style="border-collapse: collapse">
      @if($repeatHeader)
      <thead>
          @endif
          <tr>
              <th>NO.</th>
              <th>TGL</th>
              <th>NO. BUKTI</th>
              <th>URAIAN</th>
              <th>KODE REK.</th>
              <th>PENERIMAAN</th>
              <th>PENGELUARAN</th>
              <th>SALDO</th>
          </tr>
          @if($repeatHeader)
      </thead>
      @endif
      <?php 
      $i = 0;
      $saldo = 0;
      $pengeluaran = 0 ;
      $totalPengeluaran = 0;
      $totalPenerimaan = 0;
      ?>
      @foreach ($kop as $data)
          <tr class="bold">
            <td class="no"></td>
            <td class="tgl"></td>
            <td class="bukti"></td>
            <td class="uraian">Pengambilan uang</td>
            <td class="rek"></td>
            @php($totalPenerimaan = $totalPenerimaan + $data->pengambilanUang)
            <td class="penerimaan">{{number_format($data->pengambilanUang,2,',','.');}} <?php 
            ?></td>
            <td class="pengeluaran"></td>
            <td class="saldo"> {{number_format($saldo += $data->pengambilanUang,2,',','.');}} </td>
        </tr>
             @foreach ($data->BKU as $BKU)
        
              @foreach ($BKU->detail as $detail)
                  
              <tr class="">
                <td class="no"></td>
                <td class="tgl">{{ Carbon\Carbon::parse($BKU->tanggal)->translatedFormat('d F Y') }}</td>
                <td class="bukti"></td>
                <td class="uraian">
                  {{$detail->nama_kegiatan}} - {{$detail->uraian}}
                </td>
                <td class="rek">{{$detail->kode_rekening}}</td>
                <td class="penerimaan"></td>
                <td class="pengeluaran">  
                 {{number_format($detail->panjar_spj,2,',','.');}}
                </td>
                <td class="saldo"> {{ number_format($saldo = $saldo - $detail->panjar_spj ,2,',','.'); }} </td>
            </tr>
             
              @foreach ($detail->pajaks as $pajak)

                  <tr class="">
                    <td class="no"></td>
                    <td class="tgl">{{ Carbon\Carbon::parse($pajak->tanggal)->translatedFormat('d F Y') }}</td>
                    <td class="bukti"></td>
                  <td class="uraian">{{$pajak->terima_pajak}}</td>
                    <td class="rek"></td>
                    <td class="penerimaan">  <?php 
                      $totalPenerimaan = $totalPenerimaan + $pajak->terima_nominal;
                     ?>
                   {{number_format($pajak->terima_nominal,2,',','.');}}</td>
                    <td class="pengeluaran">
                      <?php
                  
                      $pengeluaran = $pajak->bayar_nominal;
                      $totalPengeluaran=  $totalPengeluaran+$pengeluaran;
                     ?> 
                     {{number_format($pengeluaran,2,',','.');}}
                    </td>
                    <td class="saldo">{{number_format($saldo = $saldo + $pajak->terima_nominal - $pengeluaran,2,',','.');}}</td>
                </tr>
              @endforeach
              @endforeach
          @endforeach
          <tr class="">
            <td class="no"></td>
            <td class="tgl"></td>
            <td class="bukti"></td>
          <td class="uraian">Pengembalian sisa panjar ke Bendahara Setda</td>
            <td class="rek"></td>
            <td class="penerimaan"></td>
            <td class="pengeluaran">
            {{number_format($saldo = $saldo - $data->pengembalian,2,',','.');}}
            </td>
            <td class="saldo">{{number_format($data->pengembalian,2,',','.');}}</td>
        </tr>
      @endforeach
    
   
      <tfoot>
          <!-- space kosong -->
          <tr>
              <td colspan="8">&nbsp;</td>
          </tr>
          <!-- end space kosong -->
          <tr>
              <td colspan="5">Jumlah periode ini</td>
              <td class="penerimaan">{{number_format($totalPenerimaan,2,',','.');}}</td>
              <td class="pengeluaran">{{number_format($totalPengeluaran ,2,',','.');}}</td>
              <td class="saldo"></td>
          </tr>
          <tr>
              <td colspan="5">Jumlah sampai periode lalu</td>
              <td class="penerimaan">{{number_format($penerimaanLalu,2,',','.');}}</td>
              <td class="pengeluaran">{{number_format($pengeluaranLalu,2,',','.');}}</td>
              <td class="saldo"></td>
          </tr>
          <tr>
              <td colspan="5">Jumlah semua sampai perode ini</td>
              <td class="penerimaan">{{number_format($totalPenerimaan+$penerimaanLalu,2,',','.');}}</td>
              <td class="pengeluaran">{{number_format($totalPengeluaran+$pengeluaranLalu,2,',','.');}}</td>
              <td class="saldo"></td>
          </tr>
          <tr>
              <td colspan="3">Sisa Kas</td>
          </tr>
          <tr>
              <td colspan="3">Kas di Bendahara Pengeluaran</td>
              <td class="bold">Rp. 0</td>
          </tr>
          <tr>
              <td class="italic" colspan="3">(nol rupiah)</td>
          </tr>
          <tr>
              <td colspan="3">terdiri dari:</td>
          </tr>
          <tr>
              <td colspan="2">a. Tunai</td>
              <td>-</td>
          </tr>
          <tr>
              <td colspan="2">b. Saldo Bank</td>
              <td>-</td>
          </tr>
          <tr>
              <td colspan="2">c. Surat Berharga</td>
              <td>-</td>
          </tr>
      </tfoot>
  </table>

  <table class="table__footer">
      <tr>
          <td class="table__sign-wrapper">
              <p>Mengetahui,</p>
              <h4>KUASA PENGGUNA ANGGARAN</h4>
              <div class="table__sign"></div>
                <p class=""> {{$ttd->nama_kepala}}</p>
                <p class="">NIP. {{$ttd->nip_kepala}}</p>
          </td>
          <td class="table__sign-wrapper">
              <p>Purworejo, .... {{ Carbon\Carbon::now()->translatedFormat('F Y') }}</p>
              <h4>BENDAHARA PENGELUARAN PEMBANTU</h4>
              <div class="table__sign"></div>
                <p class=""> {{$ttd->nama_bendahara}}</p>
                <p class="">NIP. {{$ttd->nip_bendahara}}</p>
          </td>
      </tr>
  </table>
</div>