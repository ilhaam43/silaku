@foreach ($daftar as $df)
<tr>
    <td scope="row">{{$loop->iteration}}</td>
    <td>{{Carbon\Carbon::create()->day(1)->month($df->month)->format('M')}}</td>
    <td>{{$df->year}}</td>
    <td>{{$df->subBag}}</td>
    <td>
        @if ($df->form == "N")
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong{{$df->month}}">
            Input
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModalLong{{$df->month}}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Input Utusan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="POST">
                        @csrf
                        <div class="modal-body">

                            <input type="hidden" name="idFungsional" value="{{$df->id}}">

                            <div class="form-group">
                                <label for="">Urusan Pemerintahan</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="kodeUrusan" id=""
                                        aria-describedby="helpId" placeholder="Kode Urusan">
                                    <input type="text" class="form-control" name="namaUrusan" id=""
                                        aria-describedby="helpId" placeholder="Nama Urusan">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Bidang Pemerintahan</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="kodeBidang" id=""
                                        aria-describedby="helpId" placeholder="Kode Bidang">
                                    <input type="text" class="form-control" name="namaBidang" id=""
                                        aria-describedby="helpId" placeholder="Nama Bidang">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Unit Organisasi</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="kodeUnit" id=""
                                        aria-describedby="helpId" placeholder="Kode Unit">
                                    <input type="text" class="form-control" name="namaUnit" id=""
                                        aria-describedby="helpId" placeholder="Nama Unit">
                                </div>
                                
                            </div>

                            <div class="form-group">
                                <label for="">Sub Unit Organisasi</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="kodeSubUnit" id=""
                                        aria-describedby="helpId" placeholder="Kode Sub Unit">
                                    <input type="text" class="form-control" name="namaSubUnit" id=""
                                        aria-describedby="helpId" placeholder="Nama Sub Unit">
                                </div>
                              
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        @endif

        <a name="" id="" class="btn btn-primary" href="/cetak/{{$df->id}}" role="button">Print</a>
    </td>
</tr>

@endforeach