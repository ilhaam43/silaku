@foreach($rekening as $rek)
<tr>
    <td>{{$rek->kode_rekening}}</td>
    <td>{{$rek->level == 0 ? 'Default 0' : ($rek->level == 1 ? 'Default 1' : ($rek->level == 2 ? 'Program' : ($rek->level == 3 ? 'Kegiatan' : ($rek->level == 4 ? 'Sub Kegiatan' : 'Belanja'))))}}</td>
    <td>{{$rek->nama_kegiatan}}</td>
    <td>
    <button
        type="button"
        class="btn btn-outline-danger user__delete-btn" onclick="destroy({{$rek->id}})"
    >
        <i class="fas fa-fw fa-trash"></i>
    </button>
    <button
        type="button"
        class="btn btn-outline-info user__edit-btn" onclick="update({{$rek->id}})"
    >
        <i class="fas fa-fw fa-edit"></i>
    </button>
    </td>
</tr>
@include('admin.partials.rekening', ['rekening' => $rek->subs])
@endforeach
