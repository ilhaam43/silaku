@php $i=0 @endphp
@foreach ($BKU as $df)
<tr>
    <td scope="row">{{$loop->iteration}}</td>
    <td>{{Carbon\Carbon::create()->day(1)->month($df->month)->format('M')}}</td>
    <td>{{$df->year}}</td>
    <td>{{$df->subBag}}</td>
    <td>
        <form action="/create" method="post">
            <a name="" id="" class="btn btn-primary" href="/cetakbku/{{$df->id}}" role="button">Print</a>
            @csrf
        
            <input type="hidden" name="id" value="{{$df->id}}">
            <button type="submit" class="btn btn-primary">Generate</button>
      
        </form>
    </td>
</tr>
@endforeach
