@extends('layouts.admin')
@push('head')
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
@endpush
@section('admin')
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-center mb-4">
        <h1 class="h3 mb-0 text-gray-800">Daftar Pengguna Sistem</h1>
    </div>
    <!-- Tables-->
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <button type="button" class="btn btn-primary user__add-btn">
                <i class="fas fa-fw fa-plus"></i> Tambah
            </button>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-center" id="dataTable" width="100%"
                    cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama User</th>
                            <th>Jabatan User</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Nama User</th>
                            <th>Jabatan User</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->role == 'Benpem' ? 'Bendahara Pembantu (' . $user->level . ')': ($user->role == 'Bentama' ? 'Bendahara Setda' : $user->role) }}
                            </td>
                            <td>
                                <button type="button" class="btn btn-outline-danger user__delete-btn"
                                    data-id={{ $user->id }}>
                                    <i class="fas fa-fw fa-trash"></i>
                                </button>
                                <button type="button" class="btn btn-outline-info user__edit-btn"
                                    data-id={{ $user->id }}>
                                    <i class="fas fa-fw fa-edit"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
@push('scripts')
<script src="{{asset('js/user.js')}}"></script>
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
@endpush
