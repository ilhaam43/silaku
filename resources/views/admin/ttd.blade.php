@extends('layouts.admin')
@push('head')
    <link rel="icon" href="{{asset("img/logo.png")}}">
@endpush

@section('admin')
<div class="container-fluid">
    <div class="card-header">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-center mb-4">
            <h1 class="h4 mb-0 text-gray-800">Daftar TTD Laporan</h1>
        </div>

        <!-- Modal Tambah TTD -->
        <div class="modal fade" id="tambahModal" tabindex="4" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="{{ route('ttd.store') }}" method="post" class="form-horizontal">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header align-items-center justify-content-center">
                            <h4 class="modal-title" id="exampleModalLabel">Tambah Data TTD</h4>
                        </div>
                        <div class="form-group text-left mx-4 px-4">
                            <label for="nama_kepala">Masukan nama Kepala Bidang</label>
                            <input class="form-control" type="text" name="nama_kepala" id="" required>
                        </div>
                        <div class="form-group text-left mx-4 px-4">
                            <label for="nip_kepala">Masukan NIP Kepala Bidang</label>
                            <input class="form-control" type="text" name="nip_kepala" id="" required>
                        </div>
                        <div class="form-group text-left mx-4 px-4">
                            <label for="nama_bendahara">Masukan nama Bendahara</label>
                            <input class="form-control" type="text" name="nama_bendahara" id="" required>
                        </div>
                        <div class="form-group text-left mx-4 px-4">
                            <label for="nip_bendahara">Masukan NIP Bendahara</label>
                            <input class="form-control" type="text" name="nip_bendahara" id="" required>
                        </div>
                        <div class="form-group text-left mx-4 px-4">
                            <label for="bidang">Pilih bidang</label>
                            <select name="bidang" class="form-control">
                                <option value="">- Pilih Bidang -</option>
                                <option value="Umum">Umum</option>
                                <option value="Organisasi">Organisasi</option>
                                <option value="Prokopim">Prokopim</option>
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <!-- Tables Kop Surat-->
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <button type="button" class="btn btn-primary user__add-btn" data-toggle="modal"
                    data-target="#tambahModal">
                    <i class="fas fa-plus"></i> Tambah Data TTD
                </button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIP Kepala</th>
                                <th>Nama Kepala</th>
                                <th>NIP Bendahara</th>
                                <th>Nama Bendahara</th>
                                <th>Bidang</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $ttd)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $ttd->nip_kepala }}</td>
                                <td>{{ $ttd->nama_kepala }}</td>
                                <td>{{ $ttd->nip_bendahara }}</td>
                                <td>{{ $ttd->nama_bendahara }}</td>
                                <td>{{ $ttd->bidang }}</td>
                                <td>
                                    <div class="mb-2">
                                        <form action="{{ route('ttd.destroy',$ttd->id) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="sumbit"
                                                onclick="return confirm('Apakah Yakin Ingin Menghapus Data?')"
                                                href="{{ url('admin/ttd/'.$ttd->id) }}"
                                                class="btn btn-outline-danger"><i
                                                    class="fas fa-trash-alt"></i></i></button>
                                        </form>
                                    </div>
                                    <div class="mb-2">
                                        <button class="btn btn-outline-primary" data-toggle="modal"
                                            data-target="#UbahModal{{ $ttd->id }}"><i
                                                class="fas fa-fw fa-edit"></i></button>
                                    </div>
                                </td>
                            </tr>

                            <!-- Modal Ubah TTD -->
                            <div class="modal fade" id="UbahModal{{ $ttd->id }}" tabindex="4" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <form action="{{ route('ttd.update', ['id' => $ttd->id])}}" method="post"
                                        class="form-horizontal">
                                        @csrf
                                        <div class="modal-content">
                                            <div class="modal-header align-items-center justify-content-center">
                                                <h4 class="modal-title" id="exampleModalLabel">ubah Data TTD</h4>
                                            </div>
                                            <div class="form-group text-left mx-4 px-4">
                                                <label for="nama_kepala">Masukan nama Kepala Bidang</label>
                                                <input class="form-control" type="text" name="nama_kepala" id=""
                                                    value="{{ $ttd->nama_kepala }}" required>
                                            </div>
                                            <div class="form-group text-left mx-4 px-4">
                                                <label for="nip_kepala">Masukan NIP Kepala Bidang</label>
                                                <input class="form-control" type="text" name="nip_kepala" id=""
                                                    value="{{ $ttd->nip_kepala }}" required>
                                            </div>
                                            <div class="form-group text-left mx-4 px-4">
                                                <label for="nama_bendahara">Masukan nama Bendahara</label>
                                                <input class="form-control" type="text" name="nama_bendahara" id=""
                                                    value="{{ $ttd->nama_bendahara }}" required>
                                            </div>
                                            <div class="form-group text-left mx-4 px-4">
                                                <label for="nip_bendahara">Masukan NIP Bendahara</label>
                                                <input class="form-control" type="text" name="nip_bendahara" id=""
                                                    value="{{ $ttd->nip_bendahara }}" required>
                                            </div>
                                            <div class="form-group text-left mx-4 px-4">
                                                <label for="bidang">Pilih bidang</label>
                                                <select name="bidang" class="form-control">
                                                    <option value="{{ $ttd->bidang }}">{{ $ttd->bidang }}</option>
                                                    <option value="Umum">Umum</option>
                                                    <option value="Organisasi">Organisasi</option>
                                                    <option value="Prokopim">Prokopim</option>
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
</div>
@endsection