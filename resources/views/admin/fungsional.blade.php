<style>
    html {
        margin: 40px;
    }

    * {
        box-sizing: border-box;
        margin: 0;
        padding: 0;
        font-family: calibri, Helvetica, sans-serif;
        font-size: 16px;
    }

    .table__header {
        padding: 15px;
        text-align: center;
        position: relative;
    }

    .table__header h2 {
        font-size: 20px;
    }

    .table__desc-text {
        width: 100%;
    }

    .table__desc-text tr td:nth-child(1) {
        font-weight: bold;
        width: 12%;
    }

    .table__desc-text tr td:nth-child(2) {
        width: 15%;
    }

    .table__desc-text tr td:nth-child(3) {
        width: 73%;
    }

    .table__content {
        width: 100%;
    }

    .table__content thead {
        border: 2px solid black;
    }

    .table__content tbody {
        border: 2px solid black;
    }

    .table__content .jumlah-wrapper {
        border: 2px solid black;
    }

    .table__content tfoot {
        border-top: 2px solid black;
    }

    .table__content tr,
    .table__content td,
    .table__content th {
        border: 1px solid black;
    }

    .table__content tr {
        height: 30px;
    }

    .table__content th,
    .table__content td {
        padding: 5px;
    }

    .rek {
        width: 15%;
    }

    .uraian {
        width: 45%;
    }

    .anggaran,
    .pergeseran,
    .perubahan,
    .sd-bulan-lalu,
    .bulan-ini,
    .sd-bulan-ini,
    .jumlah-spj,
    .sisa {
        width: calc(40% / 8);
        text-align: right;
    }

    .jumlah {
        text-align: center;
    }

    .table__content tfoot tr,
    .table__content tfoot td {
        border: none;
        padding: 0;
        height: auto;
    }

    .table__content tfoot tr td:nth-child(2) {
        text-align: right;
    }

    .table__footer {
        padding-top: 30px;
        width: 100%;
    }

    .table__tanggal {
        width: 100%;
        text-align: center;
    }

    .table__sign-container {
        text-align: center;
        clear: right;
        margin-top: 30px;
    }

    .table__sign-left {
        float: left;
        width: 50%;
    }

    .table__sign-right {
        float: right;
        width: 50%;
    }

    .table__sign {
        height: 80px;
    }
</style>

<div class="table__container">
    <header class="table__header">
        <h2>LAPORAN PERTANGGUNG JAWABAN PENGELUARAN</h2>
        <h2>(SPJ BELANJA - FUNGSIONAL)</h2>
    </header>
    <div class="table__desc">
        <table class="table__desc-text">
            <tr>
                <td>Urusan Pemerintahan</td>
                <td>{{$dataFungsional->kodeUrusan}}</td>
                <td>{{$dataFungsional->namaUrusan}}</td>
            </tr>
            <tr>
                <td>Bidang Pemerintahan</td>
                <td>{{$dataFungsional->kodeBidang}}</td>
                <td>
                    {{$dataFungsional->namaBidang}}
                </td>
            </tr>
            <tr>
                <td>Unit Organisasi</td>
                <td>{{$dataFungsional->kodeUnit}}</td>
                <td>{{$dataFungsional->namaUnit}}</td>
            </tr>
            <tr>
                <td>Sub Unit Organisasi</td>
                <td>{{$dataFungsional->kodeSubUnit}}</td>
                <td>{{$dataFungsional->namaSubUnit}}</td>
            </tr>
            <tr>
                <td colspan="2">Pengguna Anggaran/Kuasa Pengguna Anggaran</td>
                <td>{{$ttd->nama_kepala}}</td>
            </tr>
            <tr>
                <td colspan="2">Bendahara Pengeluaran</td>
                <td>{{$ttd->nama_bendahara}}</td>
            </tr>
            <tr>
                <td colspan="2">Bulan</td>
                <td>{{Carbon\Carbon::parse($kop[0]->tgl_dpa)->format('M-y')}}</td>
            </tr>
        </table>
    </div>
    <table class="table__content" style="border-collapse: collapse">
        <!-- @if($repeatHeader) -->
        <thead>
            <!-- @endif -->
            <tr>
                <th rowspan="3">KODE REKENING</th>
                <th rowspan="3">URAIAN</th>
                <th rowspan="3">JUMLAH ANGGARAN</th>
                <th rowspan="3">JUMLAH ANGGARAN PERGESERAN</th>
                <th rowspan="3">JUMLAH ANGGARAN PERUBAHAN</th>
                <th>s.d. BULAN LALU</th>
                <th>BULAN INI</th>
                <th>s.d. BULAN INI</th>
                <th rowspan="3">JUMLAH SPJ (LS+UP/GU/TU) s.d. BULAN INI</th>
                <th rowspan="3">SIS PAGU ANGGARAN</th>
            </tr>
            <tr>
                <th colspan="3">SPJ - LS Barang & Jasa</th>
            </tr>
            <tr>
                <th colspan="3">SPJ - UP/GU/TU</th>
            </tr>
            <!-- @if($repeatHeader) -->
        </thead>
        <!-- @endif -->
        <tbody>
            <?php 
          $i=1;
         
        $j=0;
        $totalMurni = 0;
        $totalPergeseran = 0;
        $totalPerubahan = 0;
        $totalSpjSampaiLalu = 0;
        $totalPanjar = 0;
        $totaljmlSpjSampaiLalu = 0;
        $totalPagu = 0;
        $totalspjPanjar = 0;
        $npd= 0;
        $spj=0;
        $sisa=0;
        $sisaSpj=0;
        // dd($kop);
          ?>
            @foreach ($kop as $item)
            @php ($sisaSpj+=$item->pengembalian)
            @foreach ($item->lvl2 as $level2)
            <tr>
                <td class="rek">{{$level2->kode_rekening}}</td>
                <td class="uraian">
                    {{$level2->nama_kegiatan}}
                </td>
                <td class="anggaran">{{number_format($level2->anggaran_murni,2,',','.');}}</td>
                <td class="pergeseran"></td>
                <td class="perubahan"></td>
                <td class="sd-bulan-lalu"></td>
                <td class="bulan-ini"></td>
                <td class="sd-bulan-ini"></td>
                <td class="jumlah-spj"></td>
                <td class="sisa"></td>
            </tr>

            @foreach ($level2->lvl3 as $level3)

            @if ($level3->anggaran_murni == 0)
                
            @else
            <tr>
                <td class="rek">{{$level3->kode_rekening}}</td>
                <td class="uraian">
                    {{$level3->nama_kegiatan}}
                </td>
                <td class="anggaran">{{number_format($level3->anggaran_murni,2,',','.');}}</td>
                <td class="pergeseran">{{number_format($level3->pergeseran,2,',','.');}}</td>
                <td class="perubahan">{{number_format($level3->perubahan,2,',','.');}}</td>
                <td class="sd-bulan-lalu">{{number_format($level3->spjSampaiBulanLalu,2,',','.');}}</td>
                <td class="bulan-ini">{{number_format($level3->pengeluaran,2,',','.');}}</td>
                <td class="sd-bulan-ini">{{number_format($spjPanjar3 = $level3->spjSampaiBulanLalu + $level3->pengeluaran,2,',','.');}}</td>
                <td class="jumlah-spj">{{number_format($jmlSpj3 = $level3->jmlSpjSampaiBulanLalu + $level3->penerimaan,2,',','.');}}</td>
                <td class="sisa">{{number_format($tPagu = $level3->anggaran_murni,2,',','.')}}</td>
                {{-- <td class="sisa">{{number_format($pagu = $level3->anggaran_murni - $jmlSpj3,2,',','.');}}</td> --}}
            </tr>
                
            @endif
           
            @foreach ($level3->lvl4 as $level4)
            @foreach ($level4->NPDBKU as $n)
            <tr>
                <td class="rek">{{$n->kode_rekening}}</td>
                <td class="uraian">{{$n->nama_kegiatan}}</td>
                <td class="anggaran">{{number_format($n->anggaran_murni,2,',','.');}}</td>
                <td class="pergeseran">{{number_format($n->pergeseran,2,',','.');}}</td>
                <td class="perubahan">{{number_format($n->perubahan,2,',','.');}}</td>
                <td class="sd-bulan-lalu">{{number_format($n->spjSampaiBulanLalu,2,',','.');}}</td>
                <td class="bulan-ini"> {{number_format($n->pengeluaran,2,',','.');}}</td>
                <td class="sd-bulan-ini">{{number_format($spjPanjar = $n->spjSampaiBulanLalu + $n->pengeluaran,2,',','.');}}</td>
                {{-- <td class="jumlah-spj">{{$spjPanjar}}</td> --}}
                <td class="jumlah-spj">{{number_format($jmlSpj = $n->jmlSpjSampaiBulanLalu + $n->penerimaan,2,',','.');}}</td>
                <td class="sisa"> - </td>
            </tr>
            <?php 
                          if($n->level == 5){
                            $totalMurni =  $totalMurni + $n->anggaran_murni;
                            $totalPergeseran = $totalPergeseran + $n->pergeseran;
                            $totalPerubahan = $totalPerubahan + $n->perubahan;
                           
                           
                            $npd = $npd + $n->pencairan_saat_ini;
                            $sisa = $sisa + $n->sisa;

                          }
                          $i++; 
                          $j++;?>

                        @foreach ($n->SPJdetail as $SPJdetail)     
                        <tr>
                            <td class="rek">{{$SPJdetail->kode_rekening}}</td>
                            <td class="uraian">{{$SPJdetail->nama_kegiatan }} - {{$SPJdetail->uraian}}</td>
                            <td class="anggaran">-</td>
                            <td class="pergeseran">-</td>
                            <td class="perubahan">-</td>
                            <td class="sd-bulan-lalu">{{number_format($SPJdetail->panjar_jmlLalu,2,',','.');}}</td>
                            <td class="bulan-ini"> {{number_format($SPJdetail->panjar_spj,2,',','.');}}</td>
                            <td class="sd-bulan-ini">{{number_format($spjPanjarD = $SPJdetail->panjar_jmlLalu + $SPJdetail->panjar_spj,2,',','.');}}</td>    
                            <td class="jumlah-spj">{{number_format($jmlSpjD = $SPJdetail->panjar_jmlLalu + $SPJdetail->panjar_jml,2,',','.');}}</td>
                           <?php 
                           $pagu = $n->anggaran_murni - $jmlSpjD;

                           $tPagu = $tPagu- $jmlSpjD;

                           $totalPagu = $totalPagu +  $tPagu;
                           ?>
                            <td class="sisa">{{number_format($tPagu,2,',','.');}}</td>
                            
                            
                            <?php 
                           
                            $totalPanjar = $totalPanjar + $SPJdetail->panjar_spj;
                            $totalSpjSampaiLalu = $totalSpjSampaiLalu + $SPJdetail->panjar_jmlLalu;
                            $totalspjPanjar = $totalspjPanjar + $spjPanjarD;
                            $totaljmlSpjSampaiLalu = $totaljmlSpjSampaiLalu +  $jmlSpjD ;
                            ?>
                        </tr>
                        @endforeach
                          
            @endforeach
            @endforeach
            @endforeach

            @endforeach
            @endforeach




            <tr class="jumlah-wrapper">
                <td class="jumlah" colspan="2">JUMLAH</td>
                <td class="anggaran">{{number_format( $totalMurni,2,',','.');  }}</td>
                <td class="pergeseran">{{ number_format($totalPergeseran,2,',','.');}}</td>
                <td class="perubahan">{{number_format($totalPerubahan,2,',','.'); }}</td>
                <td class="sd-bulan-lalu">{{number_format($totalSpjSampaiLalu,2,',','.');}}</td>
                <td class="bulan-ini">{{number_format($totalPanjar,2,',','.');}}</td>
                <td class="sd-bulan-ini">{{number_format($totalspjPanjar,2,',','.'); }}</td>
                <td class="jumlah-spj">{{number_format($totaljmlSpjSampaiLalu,2,',','.');}}</td>
                <td class="sisa">{{number_format($totalPagu ,2,',','.'); }}</td>
            </tr>
        </tbody>
        tr
        <tfoot>
            <tr>
                <td>Penerimaan</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>- NPD(SP2D/UMK/LS)</td>
                <td>{{number_format($NPD2D,2,',','.');}}</td>
            </tr>
            <tr>
                <td>- Pajak</td>
                <td>{{number_format($penerimaanAll['pajak'],2,',','.');}}</td>
            </tr>
            <tr>
                <td>- Lain-lain/panjar</td>
                <td> <?php $lainIn = 0;
                  $lainIn = $lainIn;
              ?>
                    {{number_format($lainIn,2,',','.');}}</td>
            </tr>
            <tr>
                <td>Jumlah Penerimaan</td>
                <td>{{number_format($NPD2D + $penerimaanAll['pajak'] + $lainIn ,2,',','.');}}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Pengeluaran</td>
            </tr>
            <tr>
                <td>SPJ (LS+UP/GU/TU)</td>
                <td>{{number_format( $tSPJLSUP =  $SPJLSUP - $sisaSpj,2,',','.');}}</td>
            </tr>
            <tr>
                <td>Pajak</td>
                <td>{{number_format($pengeluaranAll['pajak'],2,',','.');}}</td>
            </tr>
            <tr>
                <td>Lain-lain/panjar/setor</td>
                <td>{{number_format($sisaSpj,2,',','.');}}</td>
            </tr>
            <tr>
                <td>Jumlah Pengeluaran</td>
                <td>{{//$totaljmlSpjSampaiLalu 
              number_format($tSPJLSUP+ $pengeluaranAll['pajak'] + $sisaSpj,2,',','.');  }}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Saldo Kas</td>
            </tr>
        </tfoot>
    </table>
    <table class="table__footer">
        <tr class="table__sign-container clearfix">
            <td class="table__sign-left">
                <p>&nbsp;</p>
                <h4 class="">KUASA PENGGUNA ANGGARAN</h4>
                <div class="table__sign"></div>
                <p class=""> {{$ttd->nama_kepala}}</p>
                <p class="">NIP. {{$ttd->nip_kepala}} </p>
            </td>
            <td class="table__sign-right">
                <p>Purworejo, 07 Maret 2022</p>
                <h4 class="">Bendahara Pengeluaran Pembantu</h4>
                <div class="table__sign"></div>
                <p class=""> {{$ttd->nama_bendahara}}</p>
                <p class="">NIP. {{$ttd->nip_bendahara}}</p>
            </td>
        </tr>
    </table>
</div>