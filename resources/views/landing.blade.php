@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h4 class="m-0">Login sebagai</h4></div>

                <div class="card-body d-flex justify-content-center">
                    <a href="/login/admin" class="btn btn-outline-primary p-4 m-2" style="width: 150px"><i class="fa-solid fa-user-gear landing__btn fs-1 mb-2"></i><div class="lh-sm">Admin</div></a>
                    <a href="/login/bentama" class="btn btn-outline-primary p-4 m-2" style="width: 150px"><i class="fa-solid fa-sack-dollar landing__btn fs-1 mb-2"></i><div class="lh-sm">Bendahara Setda</div></a>
                    <a href="/login/benpem" class="btn btn-outline-primary p-4 m-2" style="width: 150px"><i class="fa-solid fa-user landing__btn fs-1 mb-2"></i><div class="lh-sm">Bendahara Pembantu</div></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
