<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Email/password yang Anda masukkan salah',
    'password' => 'Password yang Anda masukkan salah.',
    'throttle' => 'Terlalu banyak percobaan login gagal. Coba lagi dalam :seconds detik.',

];
